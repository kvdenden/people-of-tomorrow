<?php

$whitelist = array('localhost', '127.0.0.1');
// Database gegevens

if (in_array($_SERVER['HTTP_HOST'], $whitelist)) {
	$db_host = "localhost";
	$db_naam = "tomorrowland";
	$db_gebruiker = "root";
	$db_ww = "";
} else {
	$db_host = "localhost";
	$db_naam = "sculpture"; 
	$db_gebruiker = "sculpture";
	$db_ww = "secret";
}

// MYSQL tabellen

$tbl_orders = "banking_orders";

// Algemene instellingen

if (in_array($_SERVER['HTTP_HOST'], $whitelist)) {
	$rootdir = "c:/wamp/www/tomorrowland";
	$domein = "http://localhost/tomorrowland";
	
} else {
	$rootdir = "/var/www/vhosts/mac21.be/subdomains/pot/httpdocs";
	$domein = "http://pot.mac21.be/banking";
}

// Email instellingen
$email_van = "diego@onetouchbvba.be";
$email_naar = "diego@onetouchbvba.be";

$headers = "From: $email_van \r\n";
$headers .= "Reply-To:  $email_van \r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

// Ogone instellingen

$toegelaten_velden = array("ACCEPTANCE","AMOUNT","BRAND","CARDNO","CN","CURRENCY","ED","IP","NCERROR","ORDERID","PAYID","PM","SHASIGN","STATUS","TRXDATE");

$pspid = "relaxybe";
$passphrase = "tomorrowlandistop1";
$passphrase_shaout = "tomorrowlandistop2";
$ogone_url = "https://secure.ogone.com/ncol/test/orderstandard.asp";

$accept_url = "$domein/feedback.php";
$cancel_url = "$domein/feedback.php";
$decline_url = "$domein/feedback.php";
$exception_url = "$domein/feedback.php";
$back_url = $domein;
$home_url = $domein;
$template_url = "$domein/template.php";

$templates	= array (
	'TML'	=> 'https://socialmingle.net/pot-test/tml.html',
	'POT'	=> 'https://socialmingle.net/pot-test/pot.html',
);

// Productie

$currency = "EUR";
$language = "en_US";

?>