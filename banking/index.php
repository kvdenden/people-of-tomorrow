<?php 
ob_start();
?>
<html>
<head>

</head>
<title></title>
<body>




<?php

/* To do

- Velden verder aanvullen in formulier
- Server side controle of velden zijn ingevuld
- Query verder aanvullen

*/

include 'config/config.php';
include 'inc/functions.php';

//var_dump($_POST);

if (!isset ($_POST['send'])) {
	
	// Formulier weergeven waarbij alle informatie moet worden opgegeven
	
	echo "<form method='POST' id='order_form'>
<input type='text' name='first_name' placeholder='First name' class='required'><br>
<input type='radio' name='payment_method' value='MasterCard'checked> MasterCard <br>
<input type='radio' name='payment_method' value='VISA'> VISA<br>
<input type='radio' name='payment_method' value='Maestro'> Maestro<br>
<input type='radio' name='payment_method' value=''> Choose on Ogone
<input type='submit' name='send' value='TML'>
<input type='submit' name='send' value='POT'>
</form>";

} else {
	
	// Formulier is verzonden
	
	include 'config/open_db.php';
	
	$first_name = sanitize ( $_POST ['first_name'] );
	$last_name = sanitize ( $_POST ['last_name'] );
	$email = sanitize ( $_POST ['email'] );
	$owner_address = sanitize ( $_POST ['owner_address'] );
	$owner_zip = sanitize ( $_POST ['owner_zip'] );
	$owner_town = sanitize ( $_POST ['owner_town'] );
	$owner_country = sanitize ( $_POST ['owner_country'] );
	$owner_telno = sanitize ( $_POST ['owner_telno'] );
	$payment_method = "CreditCard";
	$brand = sanitize ( $_POST ['payment_method'] );
	$amount = 15 * 100;
	
	$last_name = "Llera";
	$email = "diego@onetouchbvba.be";
	$owner_address = "Sint lambertusstraat 7";
	$owner_zip = "2600";
	$owner_town = "Berchem";
	$owner_country = "Belgium";
	$owner_telno = "0496683967";
	
	// Controle of alle velden zijn ingevuld
	
	if ($first_name != "") {
		
		// Variabelen encrypten
		
		$first_name_encrypt = aes_encrypt ( $first_name );
		$amount_encrypt = aes_encrypt ( $amount );
		
		// Order ID aanmaken & controleren
		
		if ($_SERVER ['HTTP_X_FORWARDED_FOR'] == "") {
			$ip = $_SERVER ['REMOTE_ADDR'];
		} else {
			$ip = $_SERVER ['HTTP_X_FORWARDED_FOR'];
		}
		
		$i = 0;
		
		while ($i < 1000) {
			
			$random = rand ( 1, 99999 );
			$ip = str_pad ( substr ( preg_replace ( "/[^0-9]/", "", $ip ) * $random, 3, 6 ), 6, '0', STR_PAD_LEFT );
			$stamp = strtotime ( "now" );
			$order_id = "TML-$stamp-$ip";
			
			$order_id_encrypt = aes_encrypt($order_id);
			
			$query = "SELECT order_id FROM $tbl_orders WHERE order_id='$order_id_encrypt' LIMIT 1";
			$result = mysql_query($query);
			$num = mysql_num_rows($result);
			
			if ($num == 0) {
				$i = 1000;
			}
			
			$i++;
		}
		
		// Order in database steken
		
		$query = "INSERT INTO $tbl_orders (order_id,first_name,amount) VALUES ('$order_id_encrypt','$first_name','$amount')";
		
		// Query voltooid? We gaan verder
		
		if (mysql_query ( $query )) {
			
			// Signature aanmaken
			
			$fields = array (
				"ACCEPTURL" => $accept_url,
				"AMOUNT" => $amount,
				"BACKURL" => $back_url,	
				"BRAND" => $brand,	
				"CANCELURL" => "$cancel_url",
				"CN" => $first_name . " " . $last_name,
				"CURRENCY" => $currency,
				"DECLINEURL" => $decline_url,
				"EMAIL" => $email,
				"EXCEPTIONURL" => $exception_url,
				"HOMEURL" => $home_url,		
				"LANGUAGE" => $language,
			    "ORDERID" => $order_id,
			    "OWNERADDRESS" => $owner_address,
				"OWNERCTY" => $owner_country, // Country
				"OWNERTELNO" => $owner_telno,
				"OWNERTOWN" => $owner_town,
				"OWNERZIP" => $owner_zip,
				"PM" => $payment_method,
				"PSPID" => $pspid,		
				"TP" => $templates[$_POST['send']],						
			);	
			
			$ogone_sha = generateShasign($fields,$passphrase);
			
			$fields['SHASIGN'] = $ogone_sha;

			// Link klaar maken om naar de checkout te gaan
			
			// Prepare query string
			$query_string = http_build_query($fields);
			
			header('Location: '. $ogone_url.'?' . $query_string);
		
		} else {
			
			// Query kon niet worden uitgevoerd.. foutmelding weergeven
			echo "query error";
			
		}
	
	} else {
		
		// Alle velden invullen
		
		echo "Fill in all fields.";
		
	}
	
	include 'config/sluit_db.php';

}
echo"<br><br>";
include 'footer.php';
?>


<script src="<?php echo"$domein"?>/js/jquery.min.js"></script>
	<script src="<?php echo"$domein"?>/js/jquery.validate.min.js"></script>
	<script src="<?php echo"$domein"?>/js/main.js"></script>
</body>
</html>