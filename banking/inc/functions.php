<?php 

function sanitize($data) {
	$data = trim ( $data );
	$data = htmlspecialchars ( $data, ENT_QUOTES );
	$data = mysql_real_escape_string ( $data );
	return $data;
}

function sanitize_text($data) {
	$data = trim ( $data );
	$data = mysql_real_escape_string ( $data );
	return $data;
}

function mysql_aes_key($key) {
	$new_key = str_repeat ( chr ( 0 ), 16 );
	for($i = 0, $len = strlen ( $key ); $i < $len; $i ++) {
		$new_key [$i % 16] = $new_key [$i % 16] ^ $key [$i];
	}
	return $new_key;
}

// Functie om data te encrypten

function aes_encrypt($val) {
	$key = mysql_aes_key ( 'MpDsw*8cQM&fez*7eBoZB^W*kP652NoK' );
	$pad_value = 16 - (strlen ( $val ) % 16);
	$val = str_pad ( $val, (16 * (floor ( strlen ( $val ) / 16 ) + 1)), chr ( $pad_value ) );
	return addslashes ( mcrypt_encrypt ( MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB ), MCRYPT_DEV_URANDOM ) ) );
}

// Functie om data te decrypten

function aes_decrypt($val) {
	$key = mysql_aes_key ( 'MpDsw*8cQM&fez*7eBoZB^W*kP652NoK' );
	$val = stripslashes ( mcrypt_decrypt ( MCRYPT_RIJNDAEL_128, $key, $val, MCRYPT_MODE_ECB, mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB ), MCRYPT_DEV_URANDOM ) ) );
	return rtrim ( $val, "\0..\x10" );
}

function datumformat_sql($datum) {

	$datum = str_replace ( "/", "-", $datum );

	if (strtotime ( $datum ) != "") {

		$datum = strftime ( "%Y-%m-%d", strtotime ( str_replace ( '/', '-', $datum ) ) );
	} else {
		$datum = "";
	}
	return $datum;
}

function generateShasign($fields,$passphrase) {
	unset($fields['SHASIGN']);
	//ksort($fields);
	$phrase = '';
	foreach($fields as $key => $field){
		if(empty($field) && $field !== '0') continue;
		$phrase .= strtoupper($key) . '=' . $field . $passphrase;
	}
	//echo $phrase;
	return strtoupper(sha1($phrase));
}

?>