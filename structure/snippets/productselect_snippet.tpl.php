
<div class="container productselectv2 hidden-xs hidden-sm">
	<div class="row">
		<div class="col-xs-12">
			<h1><?php echo $content_xml->buy->productselect_v2->title; ?></h1>
		</div>
	</div>
</div>

<div class="container productselectv2 visible-xs">
	<div class="row">
		<div class="col-xs-12">
			<div class="alert alert-warning"><?php echo $content_xml->buy->productselect_v2->mobile_xs; ?></div>
		</div>
	</div>
</div>

<div class="container productselectv2 visible-sm vspace-top-half">
	<div class="row">
		<div class="col-xs-12">
			<div class="alert alert-warning"><?php echo $content_xml->buy->productselect_v2->mobile_sm; ?></div>
		</div>
	</div>
</div>

<div class="container productselectv2 selection gift">
	<div class="row">
		<div class="col-xs-12">
			<div class="productTable">
				<!-- small square -->
				<div class="productCol small_square" data-value="small_square" data-price="<?php echo $content_xml->buy->productselect_v2->small_square->price; ?>">
					<div class="title">
						<?php echo $content_xml->buy->productselect_v2->small_square->title; ?>
						<div class="glyphicon glyphicon-chevron-down"></div>
					</div>
					<div class="content">
						<div class="content_bg">
							<div class="icon">
								<img src="/content/_global/images/product_small_square.png"/>
							</div>
							<div class="c01">
								<?php echo $content_xml->buy->productselect_v2->small_square->content01; ?>
							</div>
							<div class="size">
								<?php echo $content_xml->buy->productselect_v2->small_square->size; ?>
							</div>
							<div class="c02">
								<?php //echo $content_xml->buy->productselect_v2->small_square->content02; ?>
							</div>
							<div class="price">
								€ <?php echo $content_xml->buy->productselect_v2->small_square->price; ?>
							</div>
						</div>
					</div>
				</div>

				<!-- small rectangle -->
				<div class="productCol small_rectangle" data-value="small_rectangle" data-price="<?php echo $content_xml->buy->productselect_v2->small_rectangle->price; ?>">
					<div class="title">
						<?php echo $content_xml->buy->productselect_v2->small_rectangle->title; ?>
						<div class="glyphicon glyphicon-chevron-down"></div>
					</div>
					<div class="content">
						<div class="content_bg">
							<div class="icon">
								<img src="/content/_global/images/product_small_rectangle.png"/>
							</div>
							<div class="c01">
								<?php echo $content_xml->buy->productselect_v2->small_rectangle->content01; ?>
							</div>
							<div class="size">
								<?php echo $content_xml->buy->productselect_v2->small_rectangle->size; ?>
							</div>
							<div class="c02">
								<?php //echo $content_xml->buy->productselect_v2->small_rectangle->content02; ?>
							</div>
							<div class="price">
								€ <?php echo $content_xml->buy->productselect_v2->small_rectangle->price; ?>
							</div>
						</div>
					</div>
				</div>

				<!-- large -->
				<div class="productCol large" data-value="large" data-price="<?php echo $content_xml->buy->productselect_v2->large->price; ?>">
					<div class="title">
						<?php echo $content_xml->buy->productselect_v2->large->title; ?>
						<div class="glyphicon glyphicon-chevron-down"></div>
					</div>
					<div class="content">
						<div class="content_bg">
							<div class="icon">
								<img src="/content/_global/images/product_large.png"/>
							</div>
							<div class="c01">
								<?php echo $content_xml->buy->productselect_v2->large->content01; ?>
							</div>
							<div class="size">
								<?php echo $content_xml->buy->productselect_v2->large->size; ?>
							</div>
							<div class="c02">
								<?php //echo $content_xml->buy->productselect_v2->large->content02; ?>
							</div>
							<div class="price">
								€ <?php echo $content_xml->buy->productselect_v2->large->price; ?>
							</div>
						</div>
					</div>
				</div>

				<!-- large custom -->
				<div class="productCol large_custom" data-value="large_custom" data-price="<?php echo $content_xml->buy->productselect_v2->large_custom->price; ?>">
					<div class="title">
						<?php echo $content_xml->buy->productselect_v2->large_custom->title; ?>
						<div class="glyphicon glyphicon-chevron-down"></div>
					</div>
					<div class="content">
						<div class="content_bg">
							<div class="icon">
								<img src="/content/_global/images/product_creative.png"/>
							</div>
							<div class="c01">
								<?php echo $content_xml->buy->productselect_v2->large_custom->content01; ?>
							</div>
							<div class="size">
								<?php echo $content_xml->buy->productselect_v2->large_custom->size; ?>
							</div>
							<div class="c02">
								<?php //echo $content_xml->buy->productselect_v2->large_custom->content02; ?>
							</div>
							<div class="price">
								€ <?php echo $content_xml->buy->productselect_v2->large_custom->price; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>