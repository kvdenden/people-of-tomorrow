<!-- language switch -->
<div id="languageSwitch">
	<?php
		$path_en = getMenuLinkByID(intval($activmenuId[0]), $structure_en_xml);
		$path_nl = getMenuLinkByID(intval($activmenuId[0]), $structure_nl_xml);
	?>
	<a href="<?php echo $base_path . 'en/'. $path_en; ?>" class="en <?php echo ($language === 'en')? 'active' : ''; ?>">en</a> -
	<a href="<?php echo $base_path . 'nl/'. $path_nl; ?>" class="nl <?php echo ($language === 'nl')? 'active' : ''; ?>">nl</a>
</div>
