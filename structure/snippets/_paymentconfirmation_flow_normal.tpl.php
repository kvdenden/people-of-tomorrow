<script>
	function shareToFacebook(page_link){
		FB.ui({
			method: 'feed',
			name: 'People of Tomorrow - User Message Preview',
			caption: 'User message preview for the People of Tomorrow Bridge',
			description: (
				'Let\'s build a bridge with YOUR MESSAGE ' +
				'by Tomorrowland, Arne Quinze & the Province of Antwerp'
			),
				link: page_link,
				picture: "http://www.peopleoftomorrow.com/images/facebook_thumb.jpg"
			},
			function(response) {
				if(response && response.post_id){
					//alert('Post was published.');
					ga('send', 'social', 'facebook', 'share', page_link);
				}else{
					//alert('Post was not published.');
				}
			}
		);
	}


</script>
<?php 
	$design_type_tmp =  $_SESSION["pot_basket"]["design"]["design_type"];
	$label_type = "";

	if(($design_type_tmp == "small square") || (($design_type_tmp == "small rectangle"))){
		$label_type = ($design_type_tmp == "small square")? 'square' : 'rectangle';
	}else if(($design_type_tmp == "large") || (($design_type_tmp == "large custom"))){
		$label_type = ($design_type_tmp == "large")? 'large' : 'custom';
	}

	$file_link = $_SESSION['pot_basket']['design']['file'];
	$file_link = str_replace(".png", "", $file_link);
?>


<div class="container fullscreen-container orderconfirmation_v2 preview">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2><?php echo $content_xml->buy->confirmation_v2->normal->preview_title ;?></h2>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<div class="preview_wrapper <?php echo $label_type; ?>">
						<img src="<?php echo $base_path . '/preview/' . $_SESSION['pot_basket']['design']['file'] ;?>"/>
					</div>
					<a class="btn btn-primary social-share" onclick="shareToFacebook('<?php echo $base_path . $language . '/preview?c=' . $file_link ;?>');"></a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container fullscreen-container orderconfirmation_v2 blue">
	<div class="row info normal-info">
		<div class="col-xs-12">
			<div class="line1">
				<?php echo $content_xml->buy->confirmation_v2->normal->line1;?> <span class="dark"><?php echo $_SESSION['pot_basket']['user']['email'];?></span>
			</div>

			<div class="line2">
				<?php echo $content_xml->buy->confirmation_v2->normal->line2; ?>
			</div>
		</div>
	</div>

<?php
	$critsend = new MxmConnect();
	$tmp_base_path = ($base_path == 'http://pot.local/') ? 'http://pot.mac21.be/' : $base_path; 

	sendConfirmationEmail($critsend, $tmp_base_path, $language, $global_xml, $debug);
	/**
	 *
	 *
	 */
	function sendConfirmationEmail($critsend ,$tmp_base_path, $language, $global_xml, $debug){
		$email_body = file_get_contents($tmp_base_path . "mails/" . $language . "/orderconfirmation.html");
		$content = array('subject'=> 'Order Confirmation | People of Tomorrow', 'html'=> $email_body, 'text'=>$email_body);
		$param = array(	
			'tag'=>array('gift_payment_confirmation'), 
			'mailfrom'=> $global_xml->emailconfig->mail_from, 
			'mailfrom_friendly'=> $global_xml->emailconfig->mail_from_name, 
			'replyto'=> $global_xml->emailconfig->mail_replyto, 
			'replyto_filtered'=> 'true');

		$emails[0] = array(
			'email' => $_SESSION['pot_basket']['user']['email'], //email sender
			'field1'=> str_replace("@", "<span>@</span>", $_SESSION['pot_basket']['user']['email']) //email buyer
			);

		if($debug === true){
			explore($content);
			explore($emails);
		}

		try {
			$critsend->sendCampaign($content, $param, $emails);
		} catch (MxmException $e) {
			echo $e->getMessage();
		}
	}
?>