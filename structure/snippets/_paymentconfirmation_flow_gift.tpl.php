<?php
	// send normal email
	// send gift email
	// receiver link: <a href="" style='color: #002F40; text-transform:uppercase; text-decoration:none; font-weight:bold;'>link</a>
?>

<div class="container fullscreen-container orderconfirmation_v2 preview">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="gift"><?php echo $content_xml->buy->confirmation_v2->normal->preview_title ;?></h2>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container fullscreen-container orderconfirmation_v2 blue">
	<div class="row info gift-info">
		<div class="col-xs-12">
			<div class="line1">
				<?php echo $content_xml->buy->confirmation_v2->gift->line1a;?> <span class="dark"><?php echo $_SESSION['pot_basket']['user']['email'];?></span> <?php echo $content_xml->buy->confirmation_v2->gift->line1b;?>
			</div>

			<div class="line2">
				<?php echo $content_xml->buy->confirmation_v2->gift->line2;?> <span class="dark"><?php echo $_SESSION['pot_basket']['user']['email_sender'];?></span>
			</div>


			<div class="line3">
				<?php echo $content_xml->buy->confirmation_v2->normal->line2; ?>
			</div>
		</div>
	</div>

<?php
	$critsend = new MxmConnect();
	$tmp_base_path = ($base_path == 'http://pot.local/') ? 'http://pot.mac21.be/' : $base_path; 
	$link_suffix = "";
	if($_SESSION['pot_basket']['product'] == "small_square"){
		$link_suffix = "square";
	}else if($_SESSION['pot_basket']['product'] == "small_rectangle"){
		$link_suffix = "rectangle";
	}else if($_SESSION['pot_basket']['product'] == "large"){
		$link_suffix = "large";
	}else if($_SESSION['pot_basket']['product'] == "custom"){
		$link_suffix = "upload";
	}
	
	$link = $base_path . $language . "/" . getMenuLinkByID(51, $structure_xml) . "#/" . $link_suffix;
	$link_url = "<a href='". $link ."' target='_blank' style='color: #002F40; text-transform:uppercase; text-decoration:none; font-weight:bold;'>link</a>";
	
	sendGiftSenderEmail($critsend, $tmp_base_path, $language, $global_xml, $debug);
	sendGiftReceiverEmail($critsend, $tmp_base_path, $language, $global_xml, $debug, $token, $link_url);
	/**
	 *
	 *
	 */
	function sendGiftSenderEmail($critsend ,$tmp_base_path, $language, $global_xml, $debug){
		$email_body = file_get_contents($tmp_base_path . "mails/" . $language . "/gift-sender-confirmation.html");
		$content = array('subject'=> 'Gift Order Confirmation | People of Tomorrow', 'html'=> $email_body, 'text'=>$email_body);
		$param = array(	
			'tag'=>array('gift_payment_confirmation'), 
			'mailfrom'=> $global_xml->emailconfig->mail_from, 
			'mailfrom_friendly'=> $global_xml->emailconfig->mail_from_name, 
			'replyto'=> $global_xml->emailconfig->mail_replyto, 
			'replyto_filtered'=> 'true');

		$emails[0] = array(
			'email' => $_SESSION['pot_basket']['user']['email_sender'], //email sender
			'field1'=> str_replace("@", "<span>@</span>", $_SESSION['pot_basket']['user']['email_sender']), //email sender
			'field2'=> str_replace("@", "<span>@</span>", $_SESSION['pot_basket']['user']['email']) //email receiver
			);

		if($debug === true){
			//explore($content);
			//explore($emails);
		}

		try {
			$critsend->sendCampaign($content, $param, $emails);
		} catch (MxmException $e) {
			echo $e->getMessage();
		}
	}

	/**
	 * 
	 *
	 *
	 */
	function sendGiftReceiverEmail($critsend ,$tmp_base_path, $language, $global_xml, $debug, $token, $link_url){
		$email_body = file_get_contents($tmp_base_path . "mails/" . $language . "/gift-receiver-confirmation.html");
		$subject = "A gift from ". $_SESSION['pot_basket']['user']['email_sender'] .", Tomorrowland & Arne Quinze.";
		$content = array('subject'=> $subject, 'html'=> $email_body, 'text'=>$email_body);

		$param = array(	
			'tag'=>array('gift_payment_confirmation'), 
			'mailfrom'=> $global_xml->emailconfig->mail_from, 
			'mailfrom_friendly'=> $global_xml->emailconfig->mail_from_name, 
			'replyto'=> $global_xml->emailconfig->mail_replyto, 
			'replyto_filtered'=> 'true');

		$emails[0] = array(
			'email' => $_SESSION['pot_basket']['user']['email'], //email receiver
			'field1'=> str_replace("@", "<span>@</span>", $_SESSION['pot_basket']['user']['email']), //email sender
			'field2'=> str_replace("@", "<span>@</span>", $_SESSION['pot_basket']['user']['email_sender']), //email receiver
			'field3'=> $_SESSION['pot_basket']['note'], //note
			'field4'=> $token, //code
			'field5'=> $link_url //link
			);

		if($debug === true){
			//explore($content);
			//explore($emails);
		}

		try {
			$critsend->sendCampaign($content, $param, $emails);
		} catch (MxmException $e) {
			echo $e->getMessage();
		}
	}
?>