<nav id='main-navigation' class="navbar navbar-default" role="navigation">
   <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <?php
        $active_menu_item = _recursiveStructureByPath($structure_xml, $path_info['call_parts'][count($path_info['call_parts']) -1] );
        $active_id = $active_menu_item['id'];
        $active_link = $active_menu_item['path'];

        foreach($structure_xml as $menuitem){
          $set = true;
          if( isset($menuitem['nomenu']) ){
            $nomenu_arr = explode(' ', $menuitem['nomenu']);
            foreach ($nomenu_arr as $nomenu_item){
              if( ($nomenu_item == $active_id) || ($nomenu_item == "all") )
                $set = false;
            }
          }

          if( $menuitem['id'] > 0 ){
            $url = $menuitem['path'];

            /*
            if($menuitem['id'] == 1 ){
              // is project -> redirect to project/who
              $url .= "/" . $menuitem->navitem[0]['link'];
            }
            */

            if( $set ){
              if($menuitem['id'] == 5){ //create message menu item
                echo "<li class='blue'>";
                echo "<a href='". $language .'/'. $url ."'><div class='centerme'>". trim(strtoupper($menuitem['title'])) ."</div><div class='glyphicon glyphicon-gift'></div></a>";
              }else{
                if( str_replace(" ", "", $active_link) == str_replace(" ", "", $menuitem['link']) ){
                  echo "<li class='active'>";
                }else{
                  echo "<li>";
                }
                echo "<a href='". $language .'/'. $url ."'>". strtoupper($menuitem['title']) ." /</a>";
              }
              
              echo "</li>";
            }
          }
        }
      ?>
    </ul>
    <div class="social social-mobile">
      <a href="http://www.facebook.com/peopleoftomorrowofficial" target="_blank">
        <div class="fa fa-facebook-square"></div>
      </a>

      <a href="http://www.twitter.com/POTomorrow" target="_blank">
        <div class="fa fa-twitter-square"></div>
      </a>
      <!--
      <a href="http://www.instagram.com/peopleoftomorrow" target="_blank">
        <i class="fa fa-instagram"></i>
      </a>
      -->
    </div>
  </div><!-- /.navbar-collapse -->
</nav>