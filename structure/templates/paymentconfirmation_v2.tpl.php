<?php require_once realpath(dirname(__FILE__) . '/..') . '/_private/Critsend.php'; ?>

<?php
	$test = isset ( $_GET['test'] ) ?  $_GET['test'] : false;
	$pay_status = isset ( $_GET['status'] ) ?  $_GET['status'] : "";
	$flow = isset ( $_GET['flow'] ) ?  $_GET['flow'] : "";

	if ($test == true){
		$debug = true;
		session_destroy();
		$pay_id = "1";
		$transaction_date = "";
		$order_id = "POT-1389555657-046338";
		$cardholder = "Test please delete";

		$design_id = 0;
		$shasign = 1;
		$sha_controle = 1;

		if ($flow == "b"){
			$_SESSION['pot_basket']['design']["id"] = "21";
			$_SESSION['pot_basket']['design']["type"] = "product_1";
			$_SESSION["pot_basket"]['design']['file'] = "b1790a55a67906c18bd9a046e17c5935.png";
			$_SESSION["pot_basket"]["design"]["design_type"] = "small square";
		}if($flow == "gift"){
			$order_db_id = '666';
			$_SESSION['pot_basket']['user']['email_sender'] = "ben@thinc.be";
			$_SESSION['pot_basket']['note'] = "Hi lucky, this is a testing note woohoo!";
			$_SESSION['pot_basket']['product'] = "small_square";  // small_square, small_rectangle, large, large_custom
		}

		$_SESSION["pot_basket"]["flow"] = $flow;
		$_SESSION['pot_basket']['items']['product_1']['qty'] = 1;
		$_SESSION['pot_basket']['items']['product_1']['price'] = 10;
		//$_SESSION['pot_basket']['items']['product_3']['qty'] = 3;
		//$_SESSION['pot_basket']['items']['product_3']['price'] = 40;
		$_SESSION["pot_basket"] ["user"] ["email"] = "ben@thinc.be";
		$_SESSION['pot_basket']['total'] = 10;
		$_SESSION['pot_basket']['ogone_language'] = "en_US";
		$_SESSION['pot_basket']['currency'] = "EUR";
	}

	if (isset ( $_REQUEST ['SHASIGN'] ) && isset ( $_SESSION ['pot_basket'] ) || $test == true) {
		if ($test == false) {
			foreach ( $_REQUEST as $key => $value ) {

				$key = strtoupper ( $key );

				if (in_array ( $key, $ogone_allowed_fields )) {
					$fields [$key] = $value;
				}
			}
			ksort ( $fields );

			if ($debug == true) {
				//explore ( $fields );
				//explore ( $_SESSION );
			}

			$shasign = $fields ['SHASIGN'];
			$sha_controle = generateShasign ( $fields, $passphrase_shaout );

			if ($debug == true) {
				echo "$shasign<br>";
				echo "$sha_controle<br>";
			}
		}

		if ($shasign == $sha_controle) {
			if($test == false){
				$pay_status = $fields ['STATUS'];
				$pay_id = $fields ['PAYID'];
				$transaction_date = datumformat_sql ( $fields ['TRXDATE'] );
				$order_id = $fields ['ORDERID'];
				$cardholder = (isset($fields['CN'])) ? $fields['CN'] : "";
				//$mail_to = $email = $_SESSION ['pot_basket'] ['user'] ['email'];
				$design_id = 0;
			}

			require_once realpath ( dirname ( __FILE__ ) . '/..' ) . "/_private/db.php";
			require_once realpath ( dirname ( __FILE__ ) . '/..' ) . "/_private/edit-orderstatus.php";
			require_once realpath ( dirname ( __FILE__ ) . '/..' ) . "/_private/generate-credit.php";


			if ($pay_status == 5 or $pay_status == 9 or $pay_status == 51 or $pay_status == 91) {
				// PAYMENT ACCEPTED
				if($_SESSION["pot_basket"]["flow"] == "gift"){
					$token = 0;
					$type = "";

					foreach ($_SESSION ["pot_basket"]["items"] as $key => $product_type){
						if($test == false){
							$order_db_id = retrieveOrderDBid($db,$order_id);
						}

						$type = $key;
						$group_id = $design_id = 0;
						$token = generateCredit($db,$order_id,$order_db_id,$type,$group_id,$design_id,$token);
					}

					require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_paymentconfirmation_flow_gift.tpl.php';

				}else if($_SESSION["pot_basket"]["flow"] == "b"){
					$design_id = $_SESSION ["pot_basket"]["design"]["id"];
					$type = $_SESSION["pot_basket"]["design"]["type"];
					$order_db_id = retrieveOrderDBid($db,$order_id);
					$group_id = 0;
					$token = generateCredit($db,$order_id,$order_db_id,$type,$group_id,$design_id,$token);
					require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_paymentconfirmation_flow_normal.tpl.php';
				}else{
					// if none of both, please leave ;) // redirect to product selection page
					header ( sprintf ( "Location: %s", $base_path . $language. "/" . getMenuLinkByID(4, $structure_xml) ) );
				}
?>
				<div class="row social-action">
					<div class="col-xs-12">
						<div class="first_line"> <?php echo $content_xml->buy->confirmation_v2->social->line1; ?> </div>
						<div class="second_line"> <?php echo $content_xml->buy->confirmation_v2->social->line2; ?> </div>
						<div class="button">
							<a href="http://www.facebook.com/peopleoftomorrowofficial" target='blank'>
								<?php echo $content_xml->buy->confirmation_v2->social->button; ?>
								<span class="l3"><img src="/images/facebook_name.png" /></span>
							</a>
						</div>
					</div>
				</div>
			</div><!-- end fullscreen blue -->
<?php
			}elseif ($pay_status == 1 or $pay_status == 2 or $pay_status == 52 or $pay_status == 92){
				// SOMETHING IS WRONG
				if ($pay_status == 1){
					// Payment cancelled
					$error_message .= $content_xml->buy->confirmation->error->cancel;
					$alert_class = "alert-info";
				} elseif ($pay_status == 2){
					// Payment declined
					$error_message .= $content_xml->buy->confirmation->error->declined;
					$alert_class = "alert-danger";
				} elseif ($pay_status == 52 or $pay_status == 92){
					// Payment exception
					$error_message .= $content_xml->buy->confirmation->error->exception;
					$alert_class = "alert-warning";
				}
			}else{
				// No status received - error message
				$error_message .= $content_xml->buy->confirmation->error->standard;
				$alert_class = "alert-danger";
			}
		}else{
			// Sha sign stemt niet overeen - fraude - Error afhandeling
			$error_message .= $content_xml->buy->confirmation->error->sha;
			$alert_class = "alert-danger";
		}
	}else{
		// Error in parameters
		$error_message .= $content_xml->buy->confirmation->error->parameters;
		$alert_class = "alert-danger";
	}
?>
<?php
if ($debug == true && $test == true) {
	explore ( $_SESSION );
}
?>


<script type='text/javascript'>
	var order_id = '<?php echo $order_id; ?>';
	var flow = ('<?php echo $flow; ?>' == "b")? 'normal' : 'gift';
	var design_type = '<?php echo $_SESSION["pot_basket"]["design"]["design_type"]; ?>';
	var price = '<?php echo $_SESSION["pot_basket"]["items"][$_SESSION["pot_basket"]["design"]["type"]]["price"]; ?>';

	ga('ecommerce:addTransaction', {
		'id': order_id,
		'affiliation': "People of Tomrrow",
		'revenue': price,
		'currency': 'EUR'  // local currency code.
	});

	ga('ecommerce:addItem', {
		'id': order_id,
		'name': design_type,
		'category': flow,
		'price': price,
		'quantity': '1',
		'currency': 'EUR' // local currency code.
	});

	ga('ecommerce:send');
</script>
