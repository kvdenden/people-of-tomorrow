<?php 
	if (isset($_SESSION['pot_basket']['design'])) {
		session_destroy();
		//header ( sprintf ( "Location: %s", $base_path . $language . "/" . getMenuLinkByID(41, $structure_xml) ) );
	}

	$isChange = false;
	if( isset($_GET['c']) ){
		if( $_GET['c'] == 1){
			$isChange = true;
		}

		$normal_amount = ( isset($_SESSION['pot_basket']["items"]["product_1"]['qty']) )? $_SESSION['pot_basket']["items"]["product_1"]['qty'] : 0;  
		$large_amount = ( isset($_SESSION['pot_basket']["items"]["product_3"]['qty']) )? $_SESSION['pot_basket']["items"]["product_3"]['qty'] : 0;  
		$group_amount = ( isset($_SESSION['pot_basket']["items"]["product_2"]['qty']) )? $_SESSION['pot_basket']["items"]["product_2"]['qty'] : 0;  
	}
?>

<div class="fullscreen-container top-border-container"></div>
<form method='POST' action='<?php echo "$language/" . getMenuLinkByID(41, $structure_xml);?>' role="form">
	<div class="container productselect vspace-top-full">
		<div class='row steps'>
			<div class='col-xs-12'>
				<?php echo createSubmenu(4, $structure_xml, $language, $base_path, false, true); ?>
			</div>
		</div>
	</div>
	
	<div class="container vspace-bottom-half vspace-top-half productselect">
		<div class="row vspace-bottom-half">
			<div class='col-xs-12 text-center top-highlight'>
				<p class="blue" style='font-size:1rem;'><span class="glyphicon glyphicon-gift" style="font-size:1.3rem;"></span>  <strong>An Exciting and Unique, last minute New Year's gift!</strong></p>
				<p style="margin:0 auto;"><strong>Write a message for your nearest and dearest!<br/>  
Or, gift a blank plank to your loved one giving them all the creative freedom<br/>
in the world to produce a message that will make a lasting impression until the ends of time!</strong></p>
			</div>
		</div>
		
		<div class="row">
			<!-- INDIVIDUAL SMALL -->
			<div class="col-sm-4 first">
				<?php 
					if($isChange && ($normal_amount != 0) ){
						echo "<div class='wrapper light-gray selected'>";
					}else{
						echo "<div class='wrapper light-gray'>";
					}
				?>
				
					<div class='productImage'>
						<img src="content/_global/images/01-individualsmall.jpeg"/>
					</div>
					<h3><span class="dash">-</span> <?php echo $content_xml->buy->productselect->product_single->title; ?> <span class="dash">-</span></h3>
					<p><?php echo $content_xml->buy->productselect->product_single->description; ?></p>
				</div>
				<div class='example'>
					<img src='content/_global/images/prsel_small.jpg'>
				</div>
				<div class="text-center vspace-half">
					<select name='product_1' id='product_1'>
						<?php 
							if( $isChange ){
								display_quantity ("product_1");
							}
							for($i=0; $i <= $global_xml->payment->max_batch_size_single_slates ; $i++){
								echo "<option =value='". $i . "'>" . $i . "</value>";
							}
						?>
					</select>

					<label for="product_1">X <?php echo $products["product_1"]["price"][$activeCurrency] . " " .  $currency[$activeCurrency] ;?></label>
				</div>
				<div class="details">
					<?php echo $content_xml->buy->productselect->product_single->details; ?>
				</div>
			</div>

			<!-- INDIVIDUAL LARGE -->
			<div class="col-sm-4 second">
				<?php 
					if($isChange && ($large_amount != 0) ){
						echo "<div class='wrapper light-gray selected'>";
					}else{
						echo "<div class='wrapper light-gray'>";
					}
				?>
					<div class='productImage'>
						<img src="content/_global/images/01-individuallarge.jpeg"/>
					</div>
					<h3><span class="dash">-</span> <?php echo $content_xml->buy->productselect->product_single_large->title; ?> <span class="dash">-</span></h3>
					<p><?php echo $content_xml->buy->productselect->product_single_large->description; ?></p>
				</div>
				<div class='example'>
					<img src='content/_global/images/prsel_large.jpg'>
				</div>
				<div class="text-center vspace-half">
					<select name='product_3' id='product_3'>
						<?php 
							if( $isChange ){
								display_quantity ("product_3");
							}

							for($i=0; $i <= $global_xml->payment->max_batch_size_single_slates ; $i++){
								echo "<option =value='". $i . "'>" . $i . "</value>";
							}
						?>
					</select>

					<label for="product_3">X <?php echo $products["product_3"]["price"][$activeCurrency] . " " .  $currency[$activeCurrency] ;?></label>
				</div>
				<div class="details">
					<?php echo $content_xml->buy->productselect->product_single_large->details; ?>
				</div>
			</div>
			
			<!-- GROUP MESSAGE-->
			<div class="col-sm-4">
				<?php 
					if($isChange && ($group_amount != 0) ){
						echo "<div class='wrapper light-gray selected'>";
					}else{
						echo "<div class='wrapper light-gray'>";
					}
				?>
					<div class='productImage'>
						<img src="content/_global/images/03_groupmessage.jpeg"/>
					</div>
					<h3><span class="dash">-</span> <?php echo $content_xml->buy->productselect->product_group->title; ?> <span class="dash">-</span></h3>
					<p><?php echo $content_xml->buy->productselect->product_group->description; ?></p>
				</div>
				<div class='example'>
					<img src='content/_global/images/prsel_group.jpg'>
				</div>
				<div class="text-center vspace-half">
					<select name='product_2' id='product_2'>
						<?php 
							if( $isChange ){
								display_quantity ("product_2");
							}

							for($i=0; $i <= $global_xml->payment->max_batch_size_group_slates ; $i++){
								echo "<option =value='" . $i . "'>" . $i . "</value>";
							}
						?>
					</select>
					<label for="product_2">X <?php echo $products["product_2"]["price"][$activeCurrency] . " " .  $currency[$activeCurrency] ;?></label>
				</div>
				<div class="details">
					<?php echo $content_xml->buy->productselect->product_group->details; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="container productselect vspace-bottom-half">
		<div class="row">
			<div class="col-xs-12 bottom-highlight">
				<p><strong><span class="blue">Buy credits</span> first according to the number of messages you want to create<br/>
					<span class="blue">Credits will be sent immediately by e-mail</span> after purchase, to your personal e-mail address (also please check your SPAM folder for this email)<br/>
					In the e-mail you'll receive, <span class="blue">you'll find a direct link to the message editor</span>, where you can create your message</strong>
				</p>
			</div>
		</div>
	</div>

	<div class="container productselect vspace-bottom-full">
		<div class="row">
			<div class="col-xs-12 center-button">
				<button type='submit' name='productselect' class="btn btn-primary">
					<?php echo $content_xml->buy->productselect->submit_label; ?>
				</button>
			</div>
		</div>
	</div>

	<div class="fullscreen-container productselect light-gray">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 first">
					<div class="center_hrw">
						<div class="left">
							<h3><?php echo $content_xml->buy->productselect->messages->message01->title; ?></h3>
							<p><?php echo $content_xml->buy->productselect->messages->message01->body; ?></p>
						</div class="right">
							<img src="content/_global/images/hrw.png" />
						</div>
					</div>
				</div>
				<!--
				<div class="col-xs-12 col-sm-8">
					<div class="row">
						
						<div class="col-xs-12 col-sm- col-md-8">
							
							
							<div class="emphasize-block">
								<h3><span class="glyphicon glyphicon-gift"></span> <?php //echo $content_xml->buy->productselect->messages->message02->title; ?></h3>
								<p><?php //echo $content_xml->buy->productselect->messages->message02->body; ?>
									<span class='smaller'>
										<?php //echo $content_xml->buy->productselect->messages->message02->extra;?>
									</span>
								</p>
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-3 col-md-4">
							<button type='submit' name='productselect' class="btn btn-primary">
								<?php //echo $content_xml->buy->productselect->submit_label; ?>
							</button>
						</div>
						
					-->
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<!--
<div class="fullscreen-container productselect media-container vspace-full buy">
	<div class="container-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-8">
					<div class="overlay-centered dark">
						<h2 class="text-right"><?php //echo $content_xml->buy->productselect->overlay01->title; ?></h2>
						<p class="text-right"><?php //echo $content_xml->buy->productselect->overlay01->author; ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<img class="alignRight" src="content/_global/images/participate_step03.jpg">
</div>
-->
<!--
<div class="container vspace-full">
	<div class="row">
		<div class="col-xs-12">
			<img src="content/_global/images/collage_ambassador.jpg"/>
		</div>
	</div>
</div>
-->
<?php 
	if ($debug == true) {
		//session_destroy();
		if (isset($_SESSION)) {
			explore($_SESSION);
		}
	}
	
?>