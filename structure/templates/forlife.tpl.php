<div id="ng-app" ng-app="tfl">
	<div id="app" class="nobg tomorrowlandforlife">
    <div ng-view=""></div>
  </div>
</div>

<div id="snippets" class="hidden">
	<div id="community">
		<?php
			require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_sidebar_community.tpl.php';
			getSidebarCommunityText($content_xml);

			require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_socialButton.tpl.php';
		?>
	</div>
	<div id="facebook">
		<a class="btn btn-primary btn-large facebook-icon" href="https://www.facebook.com/arnequinze" target='_blank'><?php echo $content_xml->projects->trailer->cta;?></a>
	</div>
</div>
