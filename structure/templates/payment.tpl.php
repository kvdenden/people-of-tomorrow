<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<meta name="application-name" content="Tomorrowland Sculpture"/>
	<meta name="msapplication-TileColor" content="#333233"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
	<meta http-equiv="cleartype" content="on">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php $base_url = "https://pot.r.worldssl.net";?>
   	<link rel="icon" href="<?php echo $base_url?>/favicon.ico" type="image/x-icon"/> 
	<link rel="stylesheet" type="text/css" href="<?php echo $base_url?>/css/main.css"/>
	
    <title>Tomorrowland Sculpture</title>
<style>
.ncoltxtl,.ncoltxtl2 {
width: 10% !important;
min-width: 150px;
}
.ncoltxtl2  {
	min-width: 200px !important;
}
.ncolline1,.ncolline2,.ncolh1,.ncoltxtl,.ncoltable3 td center,.ncollogoc,.ncoltxtc {
	text-align: left;
}
.ncoltable2 td {
	width: 0px;
	padding-bottom: 10px;
	text-align:left;	
}
.ncoltable2 td + td {
	width: 35%;
	min-width: 300px;
}
.ncoltable2 td + td + td {
	width: 65%;
	min-width: 300px;
}
.ncoltable3 td {
	width: 25%;
	min-width: 55px;
}
.ncoltable3 td + td {
	width: 50%;
	min-width: 500px;
}
.ncoltable3 td + td + td {
	width: 25%;
	min-width: 150px;
}
#NCOLPP {
	padding-right: 5px;
}
.ncolinput {
	width: 90%;
	min-width: 300px;
}


</style>    
</head>
<body>
	 <!--[if lt IE 8]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

<header class='container'>
		<div class='row'>
			<div class='col-md-2 col-sm-3 col-xs-12'>
				<div id='logo'>
					<a href='en/home'>
						<img src='<?php echo $base_url?>/images/pot_logo.svg' width="130" style="margin: 10px 0;"/>
					</a>
				</div>
			</div>
		</div>
</header>
<div id='main' role='content'>

	<div class="fullscreen-container top-border-container"></div>
	<div class="container basket">
		<div class="row">
			<div class="col-md-12" style='margin-top:40px;'>
				<h1 class="blue">Pay</h1>
				<p class="description" style='display:none'> 
				$$$PAYMENT ZONE$$$
				</p>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="floatright">
					<a href="http://www.peopleoftomorrow.com/en/contact" target="_blank">contact</a>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-45895636-1', 'peopleoftomorrow.com');
		ga('send', 'pageview');
		ga('require', 'ecommerce', 'ecommerce.js');   // Load the ecommerce plug-in.
	</script>

</body>
</html>