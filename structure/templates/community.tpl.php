<div class="fullscreen-container media-container">
	<div class='container-overlay'>
		<div class='container vspace-top-full'>
			<div class="row">
				<div class="col-xs-12 community">
					<div class="top">
						<?php echo $content_xml->community->header_message->top; ?>
					</div>
					<div class="bottom">
						<?php echo $content_xml->community->header_message->bottom; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<img src="content/_global/images/whoarewe_header.jpg">
</div>

<form role="form" class="form" id="community_form" method="POST">
	<div class="container vspace-full">
		<div class="row">
			<div class="col-xs-8">
				<?php
					if (isset($_POST["form_name"])) {
						$community_name = isset ( $_POST['form_name'] ) ? $_POST['form_name']  : "";
						$community_email = isset ( $_POST['form_email'] ) ? $_POST['form_email']  : "";
						
						if ($community_name == "" or $community_email == "") {
							echo $content_xml->community->messages->fields;
						} else {
							require_once realpath(dirname(__FILE__) . '/..') . "/_private/insert-community.php";
							
							if ($success){
								echo $content_xml->community->messages->thanks;
							}else{
								echo $content_xml->community->messages->error;
							}	
						}
					}else{ 
				?>

					<div class="form-group col-md-7">
						<label class="sr-only control-label" for="form_name"><?php echo $content_xml->community->form->name;?><span class="required">*</span></label>
						<div col-md-8>
							<input type="text" class="form-control" id="form_name" name="form_name" required placeholder="<?php echo $content_xml->community->form->name;?>*">
		  				</div>
					</div>
					<br>
					<div class="form-group col-md-7">
						<label class="sr-only control-label" for="form_email"><?php echo $content_xml->community->form->email;?><span class="required">*</span></label>
						<div>
							<input 	type="email" class="form-control" id="form_email" name="form_email" required placeholder="<?php echo $content_xml->community->form->email;?>*">
		  				</div>
					</div>
					
					<div class="form-actions col-md-7">
						<button type="submit" class="btn btn-primary no-arrow"><?php echo $content_xml->community->form->subscribe; ?></button>
					</div>

				<?php } ?>	
			</div>
			<div class="col-md-4 sidebar">
				<?php 
				require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_sidebar_community.tpl.php';
				getSidebarCommunityText($content_xml); 
				?>
			</div>
		</div>
	</div>
</form>

<div class="container vspace-full">
	<div class="row">
		<div class="col-xs-8">
		<?php echo $content_xml->community->share; ?>
		</div>
	</div>
</div>