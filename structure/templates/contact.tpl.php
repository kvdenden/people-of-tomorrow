<div class="fullscreen-container media-container">
	<img src="content/_global/images/whoarewe_header.jpg">
</div>

<div class='container vspace-full'>
	<div class='row'>
		<div class='col-xs-12'>
			<h2> <?php echo $content_xml->contact->title; ?> </h2>
			<p>
				<?php echo $content_xml->contact->general; ?>
				<br>
				<a class='contact-email' href="mailto:contact@peopleoftomorrow.com">contact@peopleoftomorrow.com</a>
			</p>
			<p>
				<?php echo $content_xml->contact->press; ?>
				<br>
				<a class='contact-email' href="mailto:press@peopleoftomorrow.com">press@peopleoftomorrow.com</a>
			</p>
		</div>
	</div>
</div>