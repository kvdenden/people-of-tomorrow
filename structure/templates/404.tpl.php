<script type='text/javascript'>
	var hi404 = true;
</script>

<div class="fullscreen-container top-border-container"></div>

<div class="container notfound vspace-full">
	<div class="row">
		<div class="col-xs-12">
			<h1 class="err"><span class='ultra'><?php echo $content_xml->notfound->title; ?></span></h1>
		</div>
		<div class="col-xs-12">
			<p class="emphasise">
				<?php echo $content_xml->notfound->body; ?>
				<br>
				<a class="btn btn-primary" href="<?php echo $language . '/' . getMenuLinkByID(999, $structure_xml); ?>"><?php echo $content_xml->notfound->cta; ?></a>
			</p>
		</div>

		<div class="col-xs-12">
			<?php echo $content_xml->notfound->links_description; ?>
			<ul>
				<li><a href="<?php echo $language . '/' . getMenuLinkByID(13, $structure_xml); ?>"><?php echo $content_xml->notfound->links->moreinfo; ?></a></li>
				<li><a href="<?php echo $language . '/' . getMenuLinkByID(4, $structure_xml); ?>"><?php echo $content_xml->notfound->links->startcreating; ?></a></li>
			</ul>
		</div>
</div>