<div class="fullscreen-container container homeopen">
	<div class="container productselectv2">
		<div class="row">
			<div class="col-xs-12">
				<ul id="slider" class='rslides'>
					<li><img src="/images/slideshow/vb_aoki.jpeg"/></li>
					<li><img src="/images/slideshow/vb_carlcox.jpeg"/></li>
					<li><img src="/images/slideshow/vb_julien.jpeg"/></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container homeopen content">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
				<h3>Be part of the Tomorrowland bridge and discover your message at Tomorrowland 2014.</h3>
				<p>The "One World" bridge will represent the People of Tomorrow, a community of positive minds from all over the world.</p>
				<p><strong>Create Your FREE Message Now!</strong></p>
			</div>
		</div>
		<br><br><br>
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<h2>Use the online editor</h2>
				<div class="col-xs-12 col-sm-6">
					<a class="cta" style="display:block; margin: 0 0 100px;" href="<?php echo $base_path . $language . "/participate/create/#/tickets/square " ?>">Square message</a>
				</div>
				<div class="col-xs-12 col-sm-6">
					<a class="cta" style="display:block; margin: 0 0 100px;" href="<?php echo $base_path . $language . "/participate/create/#/tickets/rectangle" ?>">Rectangular message</a>
				</div>
				<!--
				<div class="col-xs-12 col-sm-4">
					<a class="cta" style="display:block; margin: 0 0 100px;" href="<?php //echo $base_path . $language . "/participate/create/#/tickets/large" ?>">Large message</a>
				</div>-->
			</div>
			<div class="col-xs-12 col-sm-4">
				<h2>Upload your own design</h2>
				<div class="col-xs-12">
					<a class="cta" style="display:block; margin: 0 0 100px;" href="<?php echo $base_path . $language . "/participate/create#/tickets/upload" ?>">Upload your own</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="photogrid">
					<div class="photo-item"> <img src='content/_global/images/homepage/people/01.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/04.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/09.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/08.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/07.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/02.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/art/01.jpg'></div>
					<div class="photo-item"> <img src='content/_global/images/homepage/people/03.jpg'></div>
				</div>
			</div>
		</div>
	</div>
</div>
