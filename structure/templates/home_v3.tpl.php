<?php
	// 1. Random pick type
	// 2. Check if type weight is used up
	// 3a. if not take random picture
	// 4a. check if picture is used from that type
	// 5. if not use picture

	// 3b. if it is goto 1
	// 4b. if it is goto 3a

	$people_arr = array();
	$people_arr['name'] = "people";
	$people_arr['library_count'] = 16;
	$people_arr['weight'] = 4;
	$people_arr['used'] = array();

	$art_arr = array();
	$art_arr['name'] = "art";
	$art_arr['library_count'] = 5;
	$art_arr['weight'] = 2;
	$art_arr['used'] = array();

	$message_arr = array();
	$message_arr['name'] = "message";
	$message_arr['library_count'] = 11;
	$message_arr['weight'] = 4;
	$message_arr['used'] = array();

	$image_lib = array($people_arr, $art_arr, $message_arr);

	function getRndImg(){
		global $image_lib;

		// select type
		$random_type = rand(1, count($image_lib)) - 1;
		$type_limit_reached = (count($image_lib[$random_type]['used']) >= $image_lib[$random_type]['weight']) ? 1 : 0;

		// if limit is reached for that type -> get new one that isnt used up
		while( $type_limit_reached == 1 ){
			//echo "TYPE LIMIT <br>";
			$random_type = rand(1, count($image_lib)) - 1;
			$type_limit_reached = (count($image_lib[$random_type]['used']) >= $image_lib[$random_type]['weight']) ? 1 : 0;
		}

		// check if specific image in type is used
		$random_picture_in_type = rand(1, $image_lib[$random_type]['library_count']);

		//echo $random_type . " " . $image_lib[$random_type]['name'] . "(". $image_lib[$random_type]['library_count'] .") ==> " . $random_picture_in_type . '<br>';
		//return '';
		//if( count($image_lib[$random_type]['used']) > 0){
			$random_picture_in_type_limit_reached = 0;

			foreach($image_lib[$random_type]['used'] as $item){
				if($random_picture_in_type == $item){
					$random_picture_in_type_limit_reached = 1;
				}
			}

			while($random_picture_in_type_limit_reached == 1){
				$random_picture_in_type_limit_reached = 0;
				$random_picture_in_type = rand(1, $image_lib[$random_type]['library_count']);

				foreach($image_lib[$random_type]['used'] as $item){
					if($random_picture_in_type == $item){
						$random_picture_in_type_limit_reached = 1;
					}
				}
			}
		//}

		array_push($image_lib[$random_type]['used'], $random_picture_in_type);

		$random_picture_in_type = ($random_picture_in_type < 10 )? "0". $random_picture_in_type : $random_picture_in_type;
		$random_picture_in_type .= ".jpg";

		$img_url = "/content/_global/images/homepage/". $image_lib[$random_type]['name'] . "/" . $random_picture_in_type;

		return $img_url;
	}
?>

<div class="container homepage homepage_v3 vspace-bottom-half">
	<div class="row imagegrid">
		<div class="resizer">
		<!-- row 1-->
		<!--  1 --><div class="griditem gridimg col-xs-6 col-sm-4 col-md-3 hidden-xs"><img src="<?php echo getRndImg(); ?>"/></div>
		<!--  2 --><div class="griditem gridimg col-xs-6 col-sm-4 col-md-3 hidden-xs"><img src="<?php echo getRndImg(); ?>"/></div>
		<!--  3 --><div class="griditem 		col-xs-12 col-sm-4 col-md-3 cta cta_top">
						<div class="cta_content">
							<h2><?php echo $content_xml->home_v3->grid->title;?></h2>
							<p><span class="bigger">“</span> <?php echo $content_xml->home_v3->grid->body->l1;?> <span class="bigger">”</span><br/><b><?php echo $content_xml->home_v3->grid->body->l2;?></b></p>
						</div>
					</div>
		<!--  4 --><div class="griditem gridimg col-sm-4 col-md-3 hidden-xs hidden-sm"><img src="<?php echo getRndImg(); ?>"/></div>

		<!-- row 2 -->
		<!--  5 --><div class="griditem gridimg col-sm-4 col-md-3 hidden-xs"><img src="<?php echo getRndImg(); ?>"/></div>
		<!--  6 --><div class="griditem gridimg col-sm-4 col-md-3 hidden-xs"><img src="<?php echo getRndImg(); ?>"/></div>
		<!--  7 --><div class="griditem 		col-xs-12 col-sm-4 col-md-3 cta cta_bottom">
						<div class="cta_content">
							<a href="<?php echo $language . '/' . getMenuLinkByID(4, $structure_xml); ?>">
								<div class='cta_button'>
									<!--<img src="/images/homev3_cta_btn.png" />-->
									<span class="l1"><?php echo $content_xml->home_v3->grid->button->l1;?></span>
									<span class="l2"><?php echo $content_xml->home_v3->grid->button->l2;?></span>
									<span class="l3"><?php echo $content_xml->home_v3->grid->button->l3;?></span>
								</div>
							</a>
						</div>
					</div>
		<!--  8 --><div class="griditem gridimg col-sm-4 col-md-3 hidden-xs hidden-sm"><img src="<?php echo getRndImg(); ?>"/></div>

		<!-- row 3 -->
		<!--  9 --><div class="griditem gridimg col-xs-6 col-sm-4 col-md-3"><img src="<?php echo getRndImg(); ?>"/></div>
		<!-- 10 --><div class="griditem gridimg col-xs-6 col-sm-4 col-md-3"><img src="<?php echo getRndImg(); ?>"/></div>
		<!-- 11 --><div class="griditem gridimg col-xs-6 col-sm-4 col-md-3"><img src="<?php echo getRndImg(); ?>"/></div>
		<!-- 12 --><div class="griditem gridimg col-xs-6 col-sm-4 col-md-3 hidden-sm"><img src="<?php echo getRndImg(); ?>"/></div>
		</div>
	</div>
</div>

<div class="container homepage homepage_v3 vspace-bottom-half">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-6 hrw">
			<p>
				<img src="content/_global/images/humanrightswatch.png">
				<?php echo $content_xml->home_v3->hrw; ?>
			</p>
		</div>
	</div>
</div>

<div class="container homepage homepage_v3 vspace-bottom-full">
	<div class="row">
		<div class="col-xs-12 video">
			<h2><?php echo $content_xml->home_v3->trailer; ?></h2>
			<iframe src="//player.vimeo.com/video/80568584?title=0&amp;byline=0&amp;portrait=0" frameborder="0" id="full_trailer"></iframe>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 visible-xs">
			<div class="social-mobile">
				<a href="http://www.facebook.com/peopleoftomorrowofficial" target="_blank" class="facebook">
					<div class="fa"></div>
				</a>

				<a href="http://www.twitter.com/POTomorrow" target="_blank" class="twitter">
					<div class="fa"></div>
				</a>

				<a href="#" target="_blank" class="instagram">
					<div class="fa"></div>
				</a>
			</div>
		</div>
	</div>
</div>

