<!-- start -->
<div class="container container-large homepage homepage-top homepage_v2">
	<div class="row">
		<img id='vimeo_firstfiew_preview' src="content/_global/images/home_top_2.jpg " alt="">
		<!--<iframe id="vimeo_firstview" class='vimeo-video' src='http://player.vimeo.com/video/80622288?a?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1&amp;loop=1&mp;api=1&amp;player_id=vimeo_firstview' frameborder='0' ></iframe>				
		-->
		<div class="container-overlay">
			<div class='oneworld'>
				<img src="content/_global/images/oneworld.png"/>
			</div>
			<div class='oneworld_slogan'>
				<h2>
					<?php echo $content_xml->homeV2->top->oneworld_slogan; ?>
				</h2>
			</div>

			<div class='button-row'>
				<!-- link 1 -->
				<div class="homebtn black btn1"> 
					<a class="scroll-link" href="#slider-arne">
						<div class="link">
							<div class='left'>
								<div class="heading"><?php echo $content_xml->homeV2->top->buttons->btn1->title; ?></div>
								<span><?php echo $content_xml->homeV2->top->buttons->btn1->subtitle; ?></span>
							</div>
							<div class="arrow"><img src="/images/down-arrow-grey.png"></div> 
						</div>
					</a> 
				</div>
				<!-- /link 1 --> 

				<!-- link 2 -->
				<div class="homebtn white btn2"> 
					<a href="<?php echo $language . '/' . getMenuLinkByID(4, $structure_xml); ?>">
						<div class="link">
							<div class="left">
								<div class="heading"><?php echo $content_xml->homeV2->top->buttons->btn2->title; ?></div>
								<span><?php echo $content_xml->homeV2->top->buttons->btn2->subtitle; ?></span>
							</div>
							<div class="arrow"><img src="/images/right-arrow-blue.png"></div>
						</div>
					</a> 
				</div>
				<!-- link 2 --> 

				<!-- link 3 -->
				<div class="homebtn white btn3"> 
					<a href="<?php echo $language . '/' . getMenuLinkByID(51, $structure_xml); ?>">
						<div class="link">
							<div class="left">
								<div class="heading"><?php echo $content_xml->homeV2->top->buttons->btn3->title; ?></div>
								<span><?php echo $content_xml->homeV2->top->buttons->btn3->subtitle; ?></span>
							</div>
							<div class="arrow"><img src="/images/right-arrow-blue.png"></div>
						</div>
					</a> 
				</div>
				<!-- link 3 --> 

				<!-- link 4 -->
				<div class="homebtn white btn4"> 
					<a href="<?php echo $language . '/' . getMenuLinkByID(51, $structure_xml); ?>#/tryout">
						<div class="link">
							<div class="left">
								<div class="heading"><?php echo $content_xml->homeV2->top->buttons->btn4->title; ?></div>
								<span><?php echo $content_xml->homeV2->top->buttons->btn4->subtitle; ?></span>
							</div>
							<div class="arrow"><img src="/images/right-arrow-blue.png"></div>
						</div>
					</a> 
				</div>
				<!-- link 4 --> 
			</div>
		</div>
	</div>
</div>

<!-- arne -->

<div class="container container-large homepage homepage-arne homepage_v2">
	<div class="row">
		<div class="rslides_container">
			<ul id="slider-arne" class='rslides'>
				<li class="slides first" > 
					<img src="content/_global/images/Slide_ProfielArneQuinze_FINAL.jpg" alt="">
					
					<div class="content-block content1">
						<div class='content-position'>
							<h2><?php echo $content_xml->homeV2->arne->title; ?></h2>
							<div class="author"><?php echo $content_xml->homeV2->arne->author; ?></div>
							<blockquote> <?php echo $content_xml->homeV2->arne->quote; ?> </blockquote>
							<a href="#slider-tomorrowland" class="scroll-down scroll-link">
								<div class='arrow'><img src="/images/down-arrow-blue.png"></div>
								<div class='txt'><?php echo $content_xml->homeV2->arne->button_scroll; ?></div>
							</a>
							<a class="learn-more learn-arne next rslides_nav rslides1_nav">
								<div class='txt'><?php echo $content_xml->homeV2->arne->button_seemore; ?></div>
								<div class="arrow"><img src="/images/right-arrow-blue.png"></div>
							</a>
						</div> 
					</div>
					
				</li>

				<li class="slides second"> 
					<img src="content/_global/images/Sildes_Overview_Arne.jpeg" alt="">
				</li>

				<li class="slides third"> 
					<img src="content/_global/images/arne3.jpeg" alt="">
					<div class="content-block">
						<div class='content-position content2'>
							<a class="btn btn-primary social-icons" href="https://www.facebook.com/arnequinze" target="_blank">
								<div class="text"><?php echo $content_xml->homeV2->arne->social;?></div>
								<img src="content/_global/images/social/facebook_icon.png" class="facebook">
							</a>
							<a href="#slider-tomorrowland" class="scroll-down scroll-link">
								<div class='arrow'><img src="/images/down-arrow-blue.png"></div>
								<div class='txt'><?php echo $content_xml->homeV2->arne->button_scroll; ?></div>
							</a> 
							<a class="learn-more learn-arne next rslides_nav rslides1_nav">
								<div class='txt'><?php echo $content_xml->homeV2->arne->button_seemore; ?></div>
								<div class="arrow"><img src="/images/right-arrow-blue.png"></div>
							</a>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>

<!-- Human rights watch -->
<div class="container container-large homepage humanrightswatch homepage_v2">
	<div class="row">
		<h2><?php echo $content_xml->homeV2->human->title; ?></h2>
		<p>
			<?php echo $content_xml->homeV2->human->body; ?>
		</p>

		<div class="hrw_logo">
			<img src="content/_global/images/humanrightswatch.png">
		</div>
	</div>
</div>

<!-- tomorrowland -->
<div class="container container-large homepage homepage-tomorrowland homepage_v2">
	<div class="row">
		<div class="rslides_container">
			<ul id="slider-tomorrowland" class='rslides'>
				<li class="slides first"> 
					<img src="content/_global/images/legendary.jpg" alt="">
					
					<div class="content-wrapper">
						<div class='texts'>
							<h2><?php echo $content_xml->homeV2->tomorrowland->title; ?></h2>
							<p class='first'><?php echo $content_xml->homeV2->tomorrowland->body1;?></p>
							<p><?php echo $content_xml->homeV2->tomorrowland->body2; ?></p>
						</div>		
						<a href="#slider-trailer" class="scroll-down scroll-link">
							<div class='arrow'><img src="/images/down-arrow-white.png"></div>
							<div class='txt'><?php echo $content_xml->homeV2->arne->button_scroll; ?></div>
						</a> 
						
						<a class="learn-more rslides_nav rslides2_nav next">
							<div class="arrow"><img src="/images/right-arrow-white.png"></div>
							<div class='txt'><?php echo $content_xml->homeV2->tomorrowland->button_seemore; ?></div>
							
						</a>
					</div>
					
				</li>

				<li class="slides"> 
					<img src="content/_global/images/legendary2.jpg" alt="">
				</li>
				<li class="slides"> 
					<img src="content/_global/images/legendary3.jpg" alt="">
				</li>
				<li class="slides"> 
					<img src="content/_global/images/legendary4.jpg" alt="">
				</li>
				<li class="slides"> 
					<img src="content/_global/images/legendary5.jpg" alt="">
				</li>
			</ul>
		</div>
	</div>
</div>

<!-- trailer -->
<div class="container container-large homepage homepage-trailer homepage_v2">
	<div class="row">
		<div class="rslides_container">
			<ul id="slider-trailer" class='rslides'>
				<li class="slider first">
					<img src="content/_global/images/home_video.jpg" alt="">
					<div class="content-wrapper">
						<h2><?php echo $content_xml->homeV2->trailer->title;?></h2>
						<a class="links watch_video rslides_nav rslides3_nav next"><?php echo $content_xml->homeV2->trailer->watch;?></a>
					</div>
				</li>
				<li class="slider video">
					<iframe src="//player.vimeo.com/video/80568584?title=0&amp;byline=0&amp;portrait=0" frameborder="0" id="full_trailer"></iframe>
				</li>
			</ul>
		</div>
	</div>
</div>



<!-- making your mark -->
<div class="container container-large homepage homepage-mark homepage_v2">
	<div class="row">
		<img src="content/_global/images/harmony.jpg" alt="">
		<div class='content-wrapper'>
			<section class="texts">
				<h2><?php echo $content_xml->homeV2->mark->title;?></h2>
				<p><?php echo $content_xml->homeV2->mark->p1;?></p>
				<h3><?php echo $content_xml->homeV2->mark->subtitle1;?></h3>
				<p><?php echo $content_xml->homeV2->mark->p2;?></p>
				<h3><?php echo $content_xml->homeV2->mark->subtitle2;?></h3>
				<p><?php echo $content_xml->homeV2->mark->p3;?></p>
			</section>

			<a href="<?php echo $language . '/' . getMenuLinkByID(4, $structure_xml); ?>" class="create-msg reset-basket">
				<div class='text'><?php echo $content_xml->homeV2->mark->button->title;?>
					<span class="small"><?php echo $content_xml->homeV2->mark->button->small;?></span>
				</div>
			</a> 
		</div>
	</div>
</div>



<!-- TMP MESSAGE -->
<div class='container'>
	<div class='row'>
		<div class='col-xs-12' style='text-align:center; font-size:0.9rem; letter-spacing:0;'>
			<small >De Nederlandse versie van de website is momenteel in opbouw.</small>
		</div>
	</div>
</div>