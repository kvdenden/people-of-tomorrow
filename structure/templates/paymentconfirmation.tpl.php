<?php require_once realpath(dirname(__FILE__) . '/..') . '/_private/Critsend.php'; ?>

<div class="fullscreen-container top-border-container"></div>
<div class="container vspace-full">
	<div class='row steps'>
		<div class='col-xs-12'>
			<?php echo createSubmenu(4, $structure_xml, $language, $base_path, false, true); ?>
		</div>
	</div>
</div>


<?php

$test = isset ( $_GET['test'] ) ?  $_GET['test'] : false;
$pay_status = isset ( $_GET['status'] ) ?  $_GET['status'] : "";
$flow = isset ( $_GET['flow'] ) ?  $_GET['flow'] : "";
// Remove these are test stuff

if ($test == true) {
	$debug = true;
	session_destroy();
	$pay_id = "1";
	$transaction_date = "";
	$order_id = "POT-1385399340-822163";
	$cardholder = "Testing";
	//$email = $mail_to = "ben.eelen@mac21.com"; //"diego@onetouchbvba.be";
	
	$design_id = 0;
	$shasign = 1;
	$sha_controle = 1;
	
	if ($flow == "b") {
		$_SESSION['pot_basket']['design']["id"] = "21";
		$_SESSION['pot_basket']['design']["type"] = "product_1";
		$_SESSION["pot_basket"]['design']['file'] = "cb920b4a4393a8e4d78bf43668c6d081.png";
	}else if($flow == "gift"){
		$_SESSION['pot_basket']['user']['email_sender'] = "ben.eelen@telenet.be";
	}

	$_SESSION['pot_basket']['items']['product_1']['qty'] = 1;
	$_SESSION['pot_basket']['items']['product_1']['price'] = 10;
	//$_SESSION['pot_basket']['items']['product_3']['qty'] = 3;
	//$_SESSION['pot_basket']['items']['product_3']['price'] = 40;
	//$_SESSION['pot_basket']['items']['product_2']['qty'] = 1;
	//$_SESSION['pot_basket']['items']['product_2']['price'] = 35;
	$_SESSION['pot_basket']['user']['email'] = "ben@thinc.be";
	$_SESSION['pot_basket']['total'] = 10;
	$_SESSION['pot_basket']['ogone_language'] = "en_US";
	$_SESSION['pot_basket']['currency'] = "EUR";
}

$email = $mail_to = $_SESSION['pot_basket']['user']['email'];
// Define some variabels
$error_message = "";
$group_id = 0;
$email_credits = "";
$total_price = 0;
// Destroy old credits session

unset ( $_SESSION ['credits'] );

// Check if there is a request & a session (prevent multiple creation of tokens)

if (isset ( $_REQUEST ['SHASIGN'] ) && isset ( $_SESSION ['pot_basket'] ) || $test == true) {

	if ($test == false) {
		foreach ( $_REQUEST as $key => $value ) {
		
			$key = strtoupper ( $key );
		
			if (in_array ( $key, $ogone_allowed_fields )) {
				$fields [$key] = $value;
			}
		}
		ksort ( $fields );
		
		if ($debug == true) {
			//explore ( $fields );
			//explore ( $_SESSION );
		}
		
		$shasign = $fields ['SHASIGN'];
		$sha_controle = generateShasign ( $fields, $passphrase_shaout );
		
		if ($debug == true) {
			echo "$shasign<br>";
			echo "$sha_controle<br>";
		}
	}
	
	if ($shasign == $sha_controle) {
		
		if ($test == false) {
			
			$pay_status = $fields ['STATUS'];
			$pay_id = $fields ['PAYID'];
			$transaction_date = datumformat_sql ( $fields ['TRXDATE'] );
			$order_id = $fields ['ORDERID'];
			$cardholder = $fields ['CN'];
			//$mail_to = $email = $_SESSION ['pot_basket'] ['user'] ['email'];
			$design_id = 0;
		}

		require_once realpath ( dirname ( __FILE__ ) . '/..' ) . "/_private/db.php";
		require_once realpath ( dirname ( __FILE__ ) . '/..' ) . "/_private/edit-orderstatus.php";
		require_once realpath ( dirname ( __FILE__ ) . '/..' ) . "/_private/generate-credit.php";
		
		// Payment accepted
		
		if ($pay_status == 5 or $pay_status == 9 or $pay_status == 51 or $pay_status == 91) {
			// Check whether a design cookie exists
			
			if (isset ( $_SESSION ["pot_basket"] ["design"]["id"] )) {
				$design_id = $_SESSION ["pot_basket"] ["design"]["id"];
				$type = $_SESSION["pot_basket"]["design"]["type"];
				
				// credit bought directly from editor
				
				$order_db_id = retrieveOrderDBid ($db,$order_id);
				generateCredit($db,$order_id,$order_db_id,$type,$group_id,$design_id,$token);
				
				// Thank you flow 2
				?>
				
				<div class="container paymentconfirmation">
					<div class="row thankyou">
						<div class="col-xs-12 col-md-8">
							<h2>
								<b> <?php echo $content_xml->buy->confirmation->flow02->title; ?> </b>
							</h2>
						</div>
					</div>
					<?php 
					if (isset($_SESSION["pot_basket"]['design']['file'])) {
					?>
					<div class="row preview">
						<div class="col-xs-12 col-md-8">
							<div id="preview" class="container">
								<div id="image-wrapper">
							  		<img src='<?php echo $base_path . "preview/" . $_SESSION["pot_basket"]['design']['file'];?>'>
							 	</div>
							</div>
						</div>
					</div>		
					<?php 
					}
					?>			
				</div>

				<div class="fullscreen-container light-gray vspace-full paymentconfirmation goto">
					<div class='container'>
						<div class="row">
							<div class="col-sm-8">
								<p class="text-center">
									<?php echo $content_xml->buy->confirmation->community->share; ?>
								</p>
							</div>
							<div class="col-sm-4">
								<a class="btn btn-primary btn-large facebook-icon" href="https://www.facebook.com/arnequinze" target='_blank'><?php echo $content_xml->buy->confirmation->community->cta;?></a>
							</div>
						</div>
					</div>
				</div>
				
				<?php 
			
			} else {
				
				// Credit bought through store
				// Check how many different products are present
				
				$credit_details = "";
				
				// Prepare top of the mail
				/*
				$email_credits .= "<table width='590' border='0' align='center' cellpadding='0' cellspacing='0'>
				<tr>
					<td><table width='400' border='0' align='right' cellpadding='0' cellspacing='0'>
						<tr>
							<td style='line-height: 30px; font-size: 30px;'>&nbsp;</td>
						</tr>
						<tr>
							<td><table width='400' border='0' align='right' cellpadding='0' cellspacing='0'>
								<tr>
									<td valign='top' style='border-bottom: 2px solid #676767; font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;'>".$content_xml->buy->email_flow1->details->order_information."</td>
								</tr>
							</table></td>
						</tr>
						<tr>
							<td><table width='400' border='0' align='right' cellpadding='0' cellspacing='0'>
								<tr>
									<td width='200' height='50' valign='middle' style='font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;'>".$content_xml->buy->email_flow1->details->unit."</td>
									<td width='100' valign='middle' style='font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;'>".$content_xml->buy->email_flow1->details->subtotal."</td>
									<td width='100' valign='middle' style='font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;'>".$content_xml->buy->email_flow1->details->creditcode."</td>
								</tr>
							</table></td>
						</tr>
						<tr>
							<td><table width='400' border='0' align='right' cellpadding='0' cellspacing='0'>";
				*/
				// Count number of different products
				if ($test == false) {
					$number_products = count($_SESSION ["pot_basket"] ["items"]);
				} else {
					$number_products = 3;
					//explore($products);
				}
				
				$credit_details .= "<h2 class='credit_overview_title'>" . $content_xml->buy->confirmation->product->title . "</h2>";
				$credit_details .= "<div class='credit-overview light-gray'>";
					foreach ( $_SESSION ["pot_basket"] ["items"] as $key => $product_type ) {	
						// Loop quantity per product type
						$type = $products[$key]["type"];
						$group = $products[$key]["group"];
						$price = $products[$key]["price"][$activeCurrency];
						
						if ($key == "product_2") {
							$credit_details .= "<h3>" . $content_xml->buy->confirmation->product->$key . "</h3>" . " " . $content_xml->buy->confirmation->product->product_2_info;
							
						} else {
							$credit_details .= "<h3>" . $content_xml->buy->confirmation->product->$key . "</h3>";
							
						}
						
							$credit_details .=  "<div class='credit-type-group'>";
								//$credit_details .= "<ul>";
									$o = 0;
									$creditGroupCounter = 1;
									$oldCreditGroupCounter = 0;
									if($key !== "product_2"){
										$credit_details .= "<ul>";
									}
										
									if($test == false){					
										$order_db_id = retrieveOrderDBid ($db,$order_id);
									}else{
										$order_db_id = "666";
									}

									while($o < $product_type ["qty"]){
										// Check number of credits per product type
										$z = 0;
										
										if ($group == true) {
											$group_id = $order_id . "-" .($o+1);
										}
										if ($key == "product_2") {
											$email_product_type = $content_xml->buy->confirmation->product->$key . "-" . ($o+1);
										} else {
											$email_product_type = $content_xml->buy->confirmation->product->$key;
										}

										while ( $z < $products [$key] ["credits"] ){
											generateCredit($db,$order_id,$order_db_id,$type,$group_id,$design_id,$token);
											
											// Only store the token in a session when there is only 1 product type & quantity = 1

											if($oldCreditGroupCounter != $creditGroupCounter){
												if($creditGroupCounter > 1){
													if($key == "product_2"){
														$credit_details .= '</div>';
														$credit_details .= "</ul>";
													}
												}
												
												if($key == "product_2"){
													$credit_details .= "<div class='group-wrapper'>";
													$credit_details .= "<div class='group_title'>" . $content_xml->buy->confirmation->product->group_title . " ". ($o+1) ."</div>";
													$credit_details .= "<ul>";
												}										
											}

											if ($number_products == 1 && $product_type ["qty"] == 1) {
												$_SESSION ['credits']['token'] = $token;
												$_SESSION ['credits']['type'] = $type;
												
											}
											if ($number_products == 1) {
												if ($type == "normal") {
													$editor_type = "small";
												} else {
													$editor_type = $type;
												}
												$editor_link = getMenuLinkByID(51, $structure_xml) . "#/$editor_type";
											} else {
												$editor_link = getMenuLinkByID(51, $structure_xml);
											}
											
											if($key === "product_2"){
												$credit_details .= "<li>Credit ". ($z+1) .": <b>$token</b></li>"; //  -- groupid: $group_id<br>
											}else{
												$credit_details .= "<li>Credit ". ($o+1) .": <b>$token</b></li>"; //  -- groupid: $group_id<br>
											}
											
											
											if($key === "product_2"){
												// if group mesage
												if($z===0){
													$email_credits.="<tr style='border-top: 1px solid #AAA;'>";
													
												}else{
													$email_credits.="<tr>";
												}

												$email_credits.="<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>";
													if($z===0){
														$email_credits .= $email_product_type;
													}else{
														$email_credits .= "&nbsp;";
													}
												$email_credits .= "</td>";
												
												$email_credits.="<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>";
													
													if($z === 0){
														$email_credits.= $price . " " .$activeCurrency;
													}else{
														$email_credits.= "&nbsp;";
													}

												$email_credits.= "</td>";
													
													$email_credits.= "<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><a style='color: #30c0fc; font-weight: bold;' href='http://www.peopleoftomorrow.com/$language/$editor_link'>$token</a></td>
												</tr>";
											}else{
												/*
												$email_credits.="<tr>
												<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>$email_product_type</p></td>
												<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>$price $activeCurrency</td>
												<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><a style='color: #30c0fc; font-weight: bold;' href='http://www.peopleoftomorrow.com/$language/$editor_link'>$token</a></td>
												</tr>";
												*/

												$email_credits.="<tr>
												<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>$email_product_type</p></td>
												<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>$price $activeCurrency</td>
												<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>$token</td>
												</tr>";
											}


											$oldCreditGroupCounter = $creditGroupCounter;
											
											if($key !== "product_2"){
												$total_price = $price + $total_price;
											}else{
												if($z==0){
													$total_price = $price + $total_price;
												}
											}

											$z++;
										}
										
										
										
										$creditGroupCounter++;
										$o ++;
									}
								$credit_details .= "</ul>";
							$credit_details .=  "</div>";
					}
				$credit_details .=  "</div>";
				
				$email_credits .= "<!-- for empty space -->
								<tr>
									<td width='200' valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>&nbsp;</p></td>
									<td width='100' valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>&nbsp;</td>
									<td width='100' valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>&nbsp;</td>
								</tr>
								<!-- /for empty space -->
							<!--</table>-->";
			/*
				$email_credits.= "</td>
						</tr>
						<tr>
							<td><table width='400' border='0' align='right' cellpadding='0' cellspacing='0'>
								<tr>
									<td width='200' valign='top' style='border-top: 2px solid #676767; font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;'>".$content_xml->buy->email_flow1->details->total."</td>
									<td width='200' valign='top' style='border-top: 2px solid #676767; font-family: helvetica; font-weight: bold; color: #676767; font-size: 10px; line-height: 18px;'>$total_price $activeCurrency</td>
								</tr>
							</table></td>
						</tr>
						<tr>
							<td style='line-height: 30px; font-size: 30px;'>&nbsp;</td>
						</tr>
					</table></td>
					<td width='75'>&nbsp;</td>
				</tr>
			</table>";
			*/
				// Thank you message
				
				?>
				<div class="container paymentconfirmation">
					<div class="row thankyou">
						<div class="col-xs-12 col-md-8">
							<h2>
								<b> <?php echo $content_xml->buy->confirmation->flow01->title; ?> </b>
							</h2>
							<p>
								<?php echo $content_xml->buy->confirmation->flow01->body01; ?>
								<span class="blue"> <?php echo $mail_to; ?></span><br>
								<?php echo $content_xml->buy->confirmation->flow01->body02; ?>
							</p>
						</div>
					</div>
				
					<div class="row expiration">
						<div class="col-xs-12 col-md-8">
							<h3><?php echo $content_xml->buy->confirmation->flow01->expiration_info->title; ?></h3>
							<p><?php echo $content_xml->buy->confirmation->flow01->expiration_info->description; ?></p>
						</div>
					</div>
				</div>


				<div class="container paymentconfirmation">
					<div class="row">
						<div class="col-xs-12">
							<?php 
								// Output tokens
								echo $credit_details;
							?>
						</div>
					</div>
				</div>

				<div class="fullscreen-container light-gray vspace-full paymentconfirmation goto">
					<div class='container'>
						<div class="row">
							<div class="col-sm-4">
								<div class="highlight">
									<?php echo $content_xml->buy->confirmation->flow01->goto->title;?>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="cta">
									<a class="btn btn-primary" href="<?php echo $language . '/' . "$editor_link"?>"><?php echo $content_xml->buy->confirmation->flow01->goto->cta;?></a>
								</div>
							</div>
						</div>
					</div>
				</div>

			<?php
				/*
				CRITSEND FIELDS 

				field1: buyer name
				field2: order summary (html blob)
				field3: total price
				
				field4: personal link to editor before <a style="color: #30c0fc; font-weight: bold;" href="http://www.peopleoftomorrow.com">create your message</a>
				field5: personal link to editor after <a style="color: #30c0fc; font-weight: bold;" href="http://www.peopleoftomorrow.com">START CREATING YOUR MESSAGE HERE</a>
				*/

				// Prepare email
				$mail_from = $global_xml->emailconfig->mail_from;
				$mail_from_name = $global_xml->emailconfig->mail_from_name;
				$mail_replyTo = $global_xml->emailconfig->mail_replyto;
				$mail_replyTo_name = $global_xml->emailconfig->mail_replyto_name;
					

				//explore($global_xml->emailconfig);
				//require_once realpath ( dirname ( __FILE__ ) . '/..' ) .  '/classes/PHPMailer/PHPMailerAutoload.php';
				//if($debug === false){
					$mail_to = $email = $_SESSION ["pot_basket"] ["user"] ["email"];
				//}



				$critsend = new MxmConnect();

				$link_before = "<a style='color: #30c0fc; font-weight: bold;' href='" . $base_path . $language . "/" . $editor_link . "'>" . $content_xml->buy->email_flow1->top->link_editor . "</a>";
				$link_after = "<a style='color: #30c0fc; font-weight: bold;' href='" . $base_path . $language . "/" . $editor_link . "'>" . $content_xml->buy->email_flow1->bottom->start_link . "</a>";
				$tmp_base_path = ($base_path === 'http://pot.local/') ? 'http://pot.mac21.be' : $base_path; 
				$email_body = file_get_contents($tmp_base_path . "/mails/" . $language . "/orderconfirmation.html");
				//$email_body = str_replace("\\n",'', $email_body);
				//$email_body = str_replace("\\t",'', $email_body);

				$content = array('subject'=> 'Order Confirmation | People of Tomorrow', 'html'=> $email_body, 'text'=>$email_body);
				
				$param = array(	'tag'=>array('payment_confirmation'), 
								'mailfrom'=> $mail_from, 
								'mailfrom_friendly'=> $mail_from_name, 
								'replyto'=>$mail_replyTo, 
								'replyto_filtered'=> 'true');
				
				$emails[0] = array('email' => $mail_to,
								'field1'=> $cardholder,
								'field2'=> $email_credits,
								'field3'=> $total_price . " " . $activeCurrency,
								'field4'=> $link_before,
								'field5'=> $link_after
								);
				
				if($debug === true){
					explore($content);
					explore($emails);
				}

				try {
					//if($debug ===true){
					//	print_r( $critsend->sendCampaign($content, $param, $emails) );
					//}else{
						$critsend->sendCampaign($content, $param, $emails);
					//}
				} catch (MxmException $e) {
					echo $e->getMessage();
				}
			}
			
			// Destroy session basket			
			unset ( $_SESSION ['pot_basket'] );
			unset ( $_SESSION ['pot_basket_created'] );
			
			?>
<?php
		
		} elseif ($pay_status == 1 or $pay_status == 2 or $pay_status == 52 or $pay_status == 92) {
			
			// Create link
				
			//$ogone_paymenturl = createOgoneUrl ($order_id,"session",$base_path,$language,$structure_xml,$pspid,$passphrase,$ogone_url);
				
			if ($pay_status == 1) {
				
				// Payment cancelled
				
				$error_message .= $content_xml->buy->confirmation->error->cancel;
				$alert_class = "alert-info";
			
			} elseif ($pay_status == 2) {
				
				// Payment declined
				$error_message .= $content_xml->buy->confirmation->error->declined;
				$alert_class = "alert-danger";
			
			} elseif ($pay_status == 52 or $pay_status == 92) {
				
				// Payment exception
				$error_message .= $content_xml->buy->confirmation->error->exception;
				$alert_class = "alert-warning";
			
			}
			
			//$error_message .= " <a href='$ogone_paymenturl'>". $content_xml->buy->confirmation->again ."</a>";
		
		} else {
			// No status received - error message
			$error_message .= $content_xml->buy->confirmation->error->standard;
			$alert_class = "alert-danger";
		}
	
	} else {
		
		// Sha sign stemt niet overeen - fraude - Error afhandeling
		$error_message .= $content_xml->buy->confirmation->error->sha;
		$alert_class = "alert-danger";
	
	}

} else {
	$error_message .= $content_xml->buy->confirmation->error->parameters;
	$alert_class = "alert-danger";
}
 
if ($error_message != "") {
?>	
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="alert <?php echo "$alert_class";?>">
			<?php 
				echo $error_message;
			?>
			</div>
		</div>
	</div>
</div>
<?php 
}
if ($debug == true && $test == true) {
	explore ( $_SESSION );
}
?>