<?php
	// do crazy stuff :)

	// 1. Create order for user
	// 2. Create credit for user
	// 3. Save order and credit in session to be picked up later
	// 4. Save flow in session
	$test = FALSE;

	if( isset($_GET['x']) ){
		$test = ($_GET['x'] == 1 )? TRUE : FALSE;
	}

	if (isset($_SESSION['pot_basket']['flow']) && $_SESSION['pot_basket']['flow'] == 'o') {
		$token = $_SESSION ["pot_basket"]['open']["token"];
	}else{
		$flow = $_SESSION["pot_basket"]["flow"] = 'o';
		$email = ""; //$_SESSION['pot_basket']['user']['email'] = $fts_email;
		$_SESSION['pot_basket']['total'] = 40;

		$currency = $_SESSION["pot_basket"]["currency"] = "EUR";
		$ogone_language = $_SESSION["pot_basket"]["ogone_language"] = "en_US";
		$_SESSION['pot_basket_created'] = time();

		$_SESSION ["pot_basket"]["items"]['product_3'] ='';
		$_SESSION ["pot_basket"]["items"]['product_3']['qty'] = 1;
		$_SESSION ["pot_basket"]["items"]['product_3']['price'] = 40;

		$amount = $_SESSION['pot_basket']['total'] * 100;
		// Generate order id
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-orderid.php";
		// Insert order + order_items in database
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/save-order-autopay.php";
		// generate credit
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-credit.php";
		$order_db_id = retrieveOrderDBid($db, $order_id);
		$token = '';
		generateCredit($db,$order_id,$order_db_id, "large", 0, '', $token);
		$_SESSION ["pot_basket"]['open']["token"] = $token;
	}

?>

<div class="fullscreen-container container homeopen">
	<div class="container productselectv2">
		<div class="row">
			<div class="col-xs-12">
				<ul id="slider" class='rslides'>
					<li><img src="content/_global/images/bannerGA_1.jpeg"/></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container homeopen content">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
				<?php
					if($test == TRUE){
						explore($order_id);
						explore($order_db_id);
						explore($token);
						explore($_SESSION);
					}
				?>

				<p>Graffiti Art magazine offers you to be part of a wonderful and huge human and art project. One World by contemporary artist Arne Quinze.</p>
				<p>You are among the first one hundred people coming here on this page, giving you the possibility to send a message and see it included in this On World bridge. Graffiti Art Magazine offers you the opportunity to engrave your personal message into the wood of this installation. Becoming an essential building block of the project. Your message will be here for ever and will be the symbol of the unity of all the people of the world. </p>


				<a class="cta" href="<?php echo $base_path . $language . "/participate/create/#large" ?>"><?php echo $content_xml->homeopen->cta; ?></a>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="photogrid">
					<div class="photo-item"> <img src='content/_global/images/homepage/people/01.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/04.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/09.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/08.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/07.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/02.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/art/01.jpg'></div>
					<div class="photo-item"> <img src='content/_global/images/homepage/people/03.jpg'></div>
				</div>
			</div>
		</div>
	</div>
</div>
