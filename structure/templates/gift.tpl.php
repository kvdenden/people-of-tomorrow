<?php
	// alweays clear session before start
	//session_start();
	if (isset ( $_SESSION ["pot_basket"] )) {
		unset ( $_SESSION ["pot_basket"] );
		unset ( $_SESSION ["pot_basket_created"] );
	}
?>

<div class="container productselectv2">
	<div class="row">
		<div class="col-xs-12">
			<ul id="slider" class='rslides'>
				<li><img src="/images/slideshow/vb_aoki.jpeg"/></li>
				<li><img src="/images/slideshow/vb_carlcox.jpeg"/></li>
				<li><img src="/images/slideshow/vb_julien.jpeg"/></li>
			</ul>
		</div>
	</div>
</div>


<form role="form" id='gift_form' class="form-horizontal" action='<?php echo $language . "/" . getMenuLinkByID(42, $structure_xml);?>' method='POST'>

<?php require_once 'structure/snippets/productselect_snippet.tpl.php' ;?>

	<div class="container fullscreen-container productselectv2 blue hidden-xs hidden-sm submit-pane gift">
		<div class="row">
			<div class="container vspace-top-full">
				<!-- sender -->
				<div class="row">
					<div class="col-sm-5 col-sm-offset-4 title">
						<h2><?php echo $content_xml->buy->productselect_v2->gift->gift_sender_label; ?></h2>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<label for='email' class="col-sm-2 control-label col-sm-offset-3"><?php echo $content_xml->buy->productselect_v2->gift->email_label; ?></label>
						<div class="col-xs-3 col-input">
							<input type='text' name='email' id='email' class='form-control required email' x-autocompletetype="email" 
								value=''
								data-validation-required-message="<?php echo $content_xml->buy->basket->error->email; ?>">
								<br>
						</div>
					</div>
					<div class="form-group">
						<label for='confirm_email' class="col-sm-2 control-label col-sm-offset-3"><?php echo $content_xml->buy->productselect_v2->gift->email_confirm_label; ?></label>
						<div class="col-xs-3 col-input">
							<input type='text' name='confirm_email' id='confirm_email' class='form-control required email'
								value=''
								data-validation-required-message="<?php echo $content_xml->buy->basket->error->email; ?>"
								data-validation-matches-message="<?php echo $content_xml->buy->basket->error->email_confirm; ?>">
								<br>
						</div>
					</div>
				</div>


				<!-- recipient -->
				<div class="row">
					<div class="col-sm-5 col-sm-offset-4 title">
						<h2><?php echo $content_xml->buy->productselect_v2->gift->gift_recipient_label; ?></h2>
					</div>

					<div class="form-group">
						<label for='email_rec' class="col-sm-2 control-label col-sm-offset-3"><?php echo $content_xml->buy->productselect_v2->gift->email_label; ?></label>
						<div class="col-xs-3 col-input">
							<input type='text' name='email_rec' id='email_rec' class='form-control required email' x-autocompletetype="email" 
								value=''
								data-validation-required-message="<?php echo $content_xml->buy->basket->error->email; ?>">
								<br>
						</div>
					</div>

					<div class="form-group">
						<label for='confirm_email_rec' class="col-sm-2 control-label col-sm-offset-3"><?php echo $content_xml->buy->productselect_v2->gift->email_confirm_label; ?></label>
						<div class="col-xs-3 col-input">
							<input type='text' name='confirm_email_rec' id='confirm_email_rec' class='form-control required email'
								value=''
								data-validation-required-message="<?php echo $content_xml->buy->basket->error->email; ?>"
								data-validation-matches-message="<?php echo $content_xml->buy->basket->error->email_confirm; ?>">
								<br>
						</div>
					</div>
				</div>

				<!-- terms -->
				<div class="row">
					<div class="col-sm-5 col-sm-offset-4 title">
						<h2><?php echo $content_xml->buy->productselect_v2->gift->note_label; ?></h2>
					</div>
				</div>

				<div class="row">
					<div class="form-group">
						<div class="col-sm-2 col-sm-offset-3"></div>
						<div class="col-xs-3 col-textarea">
							<textarea class="form-control" rows="6" name="note" id="note"></textarea>
						</div>
					</div>
				</div>

				<!-- note -->
				<div class="row">
					<div class="form-group">
						<div class="col-sm-2 col-sm-offset-3"></div>
						<div class="col-xs-6 col-checkbox payment_agree">
							<div class="input-group checkbox">
								<input type='checkbox' id='terms' name='terms' 
									data-validation-required-message="<?php echo $content_xml->buy->basket->error->terms; ?>"> <label for='terms' class="control-label checkbox"><?php echo $content_xml->buy->basket->terms; ?></label>
								</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group">
						<div class="col-sm-2 col-sm-offset-3"></div>
						<div class="col-xs-3 col-textarea col-product">
							<input type="hidden" name="product" id="product" value="" data-validation-required-message="<?php echo $content_xml->buy->productselect_v2->gift->error->productselect;?>" >
							<input type="hidden" name="product_price" id="product_price" value="">
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-2 col-sm-offset-3"></div>
					<div class="col-xs-3 col-checkbox payment_agree">
						<div class="cta">
							<button class='btn btn-primary btn-submit' name='basket' type='submit'>
								<?php echo $content_xml->buy->productselect_v2->gift->gift_button; ?>
							</button>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</form>


<?php 
// Define language
	if (in_array("$language", $ogone_language_array) == true) {
		$ogone_language = $ogone_language_array["$ogone_language"];
	} else {
		// Default language = English
		$ogone_language = "en_US";
	}

	$_SESSION ["pot_basket"] ["currency"] = $activeCurrency;
	$_SESSION ["pot_basket"] ["ogone_language"] = $ogone_language;
	$_SESSION ["pot_basket"] ["flow"] = "gift";
	$_SESSION ['pot_basket_created'] = time();
?>
