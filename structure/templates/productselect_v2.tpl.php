<?php
	// alweays clear session before start
	if( isset($_SESSION["pot_basket"]["items"]) ){
		unset($_SESSION["pot_basket"]["items"]);
	}
?>
<div class="container productselectv2">
	<div class="row">
		<div class="col-xs-12">
			<ul id="slider" class='rslides'>
				<li><img src="/images/slideshow/vb_aoki.jpeg"/></li>
				<li><img src="/images/slideshow/vb_carlcox.jpeg"/></li>
				<li><img src="/images/slideshow/vb_julien.jpeg"/></li>
			</ul>
		</div>
	</div>
</div>

<?php require_once 'structure/snippets/productselect_snippet_v2.tpl.php';?>

<div class="container fullscreen-container productselectv2 blue hidden-xs hidden-sm normal-flow">
	<div class="row">
		<div class="col-xs-12">
			<div class="warning-block"> <div class="glyphicon glyphicon-exclamation-sign"> </div> <?php echo $content_xml->buy->productselect_v2->warning_message; ?></div>
			<div class="thankyou-block"> <div class="glyphicon glyphicon-ok-sign"> </div> <?php echo $content_xml->buy->productselect_v2->thankyou_message; ?> </div>
		</div>
	</div>
</div>

