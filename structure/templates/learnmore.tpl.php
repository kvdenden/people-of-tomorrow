<div class="container learnmore vspace-top-full vspace-bottom-half">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-md-offset-3">
			<h2><?php echo $content_xml->learnmore->project->title; ?></h2>
			<?php echo $content_xml->learnmore->project->body; ?>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-3 sidebar-images">
			<img src='images/learnmore01.jpg'>
			<img src='images/learnmore02.jpg'>
			<img src='images/learnmore03.jpg'>
			<img src='images/learnmore04.jpg'>
		</div>
	</div>
</div>

<div class="container learnmore vspace-top-half vspace-bottom-half">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-3 hrw vspace-top-half vspace-bottom-half">
			<p>
				<img src="content/_global/images/humanrightswatch.png">
				<?php echo $content_xml->learnmore->hrw; ?>
			</p>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-6">
			<h2><?php echo $content_xml->learnmore->who->title; ?></h2>
			<?php echo $content_xml->learnmore->who->body; ?>
		</div>
	</div>
</div>

<div class="container learnmore vspace-top-half vspace-bottom-half">
	<div class="row">
		<div class="col-xs-12 col-sm-9 col-md-6 col-md-offset-3">
			<h2><?php echo $content_xml->learnmore->founders->title; ?></h2>
			<?php echo $content_xml->learnmore->founders->body; ?>
		</div>
	</div>
</div>