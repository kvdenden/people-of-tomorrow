<?php
	// TESTING SLATES
	// Large: 035ef260d1f801e900126b22b11b84f4
	// Small Square: b1790a55a67906c18bd9a046e17c5935
	// Small Rectangle: f4acd1d49fcad00ffa264143156579c4
	$content = (isset($_GET['c']))? $_GET['c'] : "";
	if($content != ""){
		$file_url = "http://www.peopleoftomorrow.com/preview/" . $content . ".png";
		$file_dimensions = getimagesize($file_url);
		$isSquare = false;
		if( ($file_dimensions[0]/$file_dimensions[1]) == 1 ){
			$isSquare = true;
		}
	}
?>

<div class="fullscreen-container container share_preview wood">
	<div class='row'>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 title">
					<?php echo $content_xml->share_preview->title; ?>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="preview_wrapper <?php echo ($isSquare)? 'square' : 'rectangle';?>">
						<div class="preview_img">
							<img src="<?php echo $file_url;?>"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="fullscreen-container container share_preview blue">
	<div class='row'>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2><?php echo $content_xml->share_preview->title_below; ?></h2>
					<p><?php echo $content_xml->share_preview->body_below; ?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<a href="http://www.peopleoftomorrow.com" class="editor-link" target='_blank'>
						<div class="cta_button">
							<span class="l1"><?php echo $content_xml->share_preview->button->l1; ?></span>
							<span class="l2"><?php echo $content_xml->share_preview->button->l2; ?></span>
							<span class="l3"><?php echo $content_xml->share_preview->button->l3; ?></span>
							
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 preview_social">
					<?php echo $content_xml->share_preview->social; ?>
				</div>
			</div>
		</div>
	</div>
</div>

