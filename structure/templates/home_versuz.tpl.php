<?php
	// do crazy stuff :) 

	// 1. Create order for user
	// 2. Create credit for user
	// 3. Save order and credit in session to be picked up later
	// 4. Save flow in session 
	$test = FALSE;

	if( isset($_GET['x']) ){
		$test = ($_GET['x'] == 1 )? TRUE : FALSE;
	}

	if (isset($_SESSION['pot_basket']['flow']) && $_SESSION['pot_basket']['flow'] == 'v') {
		$token = $_SESSION ["pot_basket"]['versuz']["token"];
	}else{
		$flow = $_SESSION["pot_basket"]["flow"] = 'v';
		$email = ""; //$_SESSION['pot_basket']['user']['email'] = $fts_email;
		$_SESSION['pot_basket']['total'] = 40;
		
		$currency = $_SESSION["pot_basket"]["currency"] = "EUR";
		$ogone_language = $_SESSION["pot_basket"]["ogone_language"] = "en_US";
		$_SESSION['pot_basket_created'] = time();

		$_SESSION ["pot_basket"]["items"]['product_3'] ='';
		$_SESSION ["pot_basket"]["items"]['product_3']['qty'] = 1;
		$_SESSION ["pot_basket"]["items"]['product_3']['price'] = 40;

		$amount = $_SESSION['pot_basket']['total'] * 100;
		// Generate order id
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-orderid.php";
		// Insert order + order_items in database
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/save-order-autopay.php";
		// generate credit
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-credit.php";
		$order_db_id = retrieveOrderDBid($db, $order_id);
		$token = '';
		generateCredit($db,$order_id,$order_db_id, "large", 0, '', $token);
		$_SESSION ["pot_basket"]['versuz']["token"] = $token;
	}
?>

<div class="fullscreen-container container versuz">
	<div class="shade">
		<div class="row">	
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<img class="tml_logo responsize-img" src='images/versuz/tml_10yr_logo.png'>
					</div>
				</div>
			</div>
			<div class="container versuz_content">
				<div class="row">
					<div class="col-xs-12">
						<img class="pot_logo" src="images/pot_logo.svg">
						<img class="versuz_logo" src="images/versuz/versuz_logo.png">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<?php echo $content_xml->versuz->body; ?>
						<p><a class="cta" href="<?php echo $base_path . $language . "/participate/create/#large" ?>"><?php echo $content_xml->versuz->cta; ?></a></p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<img class="responsive-img" src="images/versuz/faces.jpg">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>