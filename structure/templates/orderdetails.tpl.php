<?php
	// debug aan of uit?

	//$debug_url = (isset($_GET['debug'])? $_GET['debug'] : "0";
	//$debug = ($debug_url == "1") ? true : false;
	//explore($debug_url);
	$debug = false;
	if ($debug == true){
		explore($_SESSION);
		explore($_POST);
	}

	if($_SESSION["pot_basket"]["flow"] == "b"){
		// normal flow
		$email = isset($_POST['email']) ? $_POST['email'] : "";
		$confirm_email = isset($_POST['confirm_email']) ? $_POST['confirm_email'] : "";
		$flow = $_SESSION ["pot_basket"] ["flow"];
		$ogone_language = $_SESSION ["pot_basket"]["ogone_language"];
		$amount = $_SESSION['pot_basket']['total'] * 100;
		$currency = $_SESSION["pot_basket"]["currency"];
		if ($email != "" && $email == $confirm_email) {
			$_SESSION['pot_basket']['user']['email'] = $email;
			// Generate order id
			require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-orderid.php";
			// Insert order + order_items in database
			require_once realpath(dirname(__FILE__) . '/..') . "/_private/save-order.php";

			$ogone_paymenturl = createOgoneUrl ($order_id,"session",$base_path,$language,$structure_xml,$pspid,$passphrase,$ogone_url);
			header('Location: '. $ogone_paymenturl);

			if($debug == true){
				echo "<br><br>$ogone_paymenturl";
			}
		}else{
			header ( sprintf("Location: %s", $base_path . $language . "/" . getMenuLinkByID(4, $structure_xml)));
			exit();
		}
	}else if($_SESSION["pot_basket"]["flow"] == "gift"){
		// GIFT
		$email_sender = isset($_POST['email']) ? $_POST['email'] : "";
		$email_sender_confirm = isset($_POST['confirm_email']) ? $_POST['confirm_email'] : "";
		$email_receiver = isset($_POST['email_rec']) ? $_POST['email_rec'] : "";
		$email_receiver_confirm = isset($_POST['confirm_email_rec']) ? $_POST['confirm_email_rec'] : "";
		$product = isset($_POST['product']) ? $_POST['product'] : "";
		$price = isset($_POST['product_price']) ? $_POST['product_price'] : "";
		$note = isset($_POST['note']) ? $_POST['note'] : "";

		if( ($email_sender == $email_sender_confirm) && ($email_receiver == $email_receiver_confirm) && ($email_sender != "") && ($email_receiver != "")){
			$_SESSION['pot_basket']['user']['email'] = $email_receiver;
			$_SESSION['pot_basket']['user']['email_sender'] = $email_sender;
			$_SESSION['pot_basket']['total']  =  $price;
			$_SESSION['pot_basket']['product'] = $product;
			$_SESSION['pot_basket']['note'] = $note;

			$_SESSION ["pot_basket"]["items"] = '';

			if(($product == "small_rectangle") || ($product == "small_square")){ // small individual
				$_SESSION ["pot_basket"]["items"]['product_1'] ='';
				$_SESSION ["pot_basket"]["items"]['product_1']['qty'] = 1;
				$_SESSION ["pot_basket"]["items"]['product_1']['price'] = $price;
			}else if(($product == "large") || ($product == "large_custom")){ // large
				$_SESSION ["pot_basket"]["items"]['product_3'] = '';
				$_SESSION ["pot_basket"]["items"]['product_3']['qty'] = 1;
				$_SESSION ["pot_basket"]["items"]['product_3']['price'] = $price;
			}
			$amount = $_SESSION['pot_basket']['total'] * 100;
			$currency = $_SESSION["pot_basket"]["currency"];
			$ogone_language = $_SESSION["pot_basket"]["ogone_language"];
			$flow = $_SESSION["pot_basket"]["flow"];
			$email = $email_sender;
			// Generate order id
			require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-orderid.php";
			// Insert order + order_items in database
			require_once realpath(dirname(__FILE__) . '/..') . "/_private/save-order.php";
			// Create gateway url
			$ogone_paymenturl = createOgoneUrl ($order_id,"session",$base_path,$language,$structure_xml,$pspid,$passphrase,$ogone_url);
			//header('Location: '. $ogone_paymenturl);
			if($debug == true){
				echo "<br><br>$ogone_paymenturl";
			}else{
				header('Location: '. $ogone_paymenturl);
			}
		}
	}else{
		// No session found, redirect to product selection page
		header ( sprintf ( "Location: %s", $base_path . $language . "/" . getMenuLinkByID(4, $structure_xml) ) );
		exit();
	}
?>
