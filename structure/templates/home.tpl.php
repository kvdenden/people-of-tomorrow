<div class="fullscreen-container media-container">
	<div class='container-overlay'>
		<div class='container vspace-top-full homepage-overlay'>
			<div class="row">
				<div class="col-xs-12">
					<div class="oneworld-logo">
						<?php require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_header_title.tpl.php'; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class='oneworld-slogan'><?php echo $content_xml->home->title_subtext; ?></div>
				</div>
			</div>
			<div class="row vspace-top-half">
				<div class="col-md-4 col-sm-3 col-xs-12">
					<div class='cta'>
						<a class='btn btn-primary btn-lg' href="<?php echo $language . "/" . getMenuLinkByID(5, $structure_xml); ?>">
							<?php echo $content_xml->home->title_cta; ?>
						</a>
					</div>
				</div>
				<div class="col-md-8 col-sm-9 col-xs-12">
					<div class='oneworld-quote'>
						<span class="dash">-</span> <?php echo $content_xml->home->title_quote; ?> <span class="dash">-</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php echo createHtml5Video($global_xml->video->homepage); ?>
</div>

<div class='container vspace-full'>
	<div class="row vspace-bottom-half">
		<div class='col-md-8 col-sm-10 col-xs-12'>
			<h2><?php echo $content_xml->home->title; ?></h2>
			<p><?php echo $content_xml->home->body; ?></p>
		</div>
	</div>
	
	<div class="row">
		<div class='col-xs-12'>
			<img src='content/_global/images/home_quote.jpg' width='100%' height='auto' />
		</div>
	</div>
</div>