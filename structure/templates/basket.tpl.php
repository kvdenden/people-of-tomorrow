<?php 
	// Delete old basket
	if (isset ( $_SESSION ["pot_basket"] ) && !isset($_SESSION ["pot_basket"]["design"]["id"])) {
		unset ( $_SESSION ["pot_basket"] );
		unset ( $_SESSION ["pot_basket_created"] );
		
	}

	/*if($debug){
		$_POST['product_1'] = 2;
		$_POST['product_3'] = 0;
		$_POST['product_2'] = 1;
		$_POST["productselect"] = '';
	}*/

	$product_1 = isset ( $_POST ['product_1'] ) ? $_POST ['product_1']  : "";
	$product_2 = isset ( $_POST ['product_2'] ) ? $_POST ['product_2']  : "";
	$product_3 = isset ( $_POST ['product_3'] ) ? $_POST ['product_3']  : "";

	$isProductSelected = ($product_1 == 0 && $product_2 == 0 && $product_3 == 0)? false : true;

	if(!$isProductSelected && !isset($_SESSION["pot_basket"]["design"]["id"])){
		header ( sprintf ( "Location: %s", $base_path . $language. "/" . getMenuLinkByID(4, $structure_xml) ) );
	}

	$total_price = 0;
	$productCount = count($_POST);
	$productCounter = 0;
?>

<div class="fullscreen-container top-border-container"></div>
<div class="container vspace-full">
	<div class='row steps'>
		<div class='col-xs-12'>
			<?php echo createSubmenu(4, $structure_xml, $language, $base_path, false, true); ?>
		</div>
	</div>
</div>

<div class="container basket">
	<div class="row">
		<div class="col-md-9">
			<h1 class="blue"><?php echo $content_xml->buy->basket->title; ?></h1>
			<p class="description"> <?php echo $content_xml->buy->basket->description; ?></p>
		</div>
	</div>

	<div class="row">
		<div class='col-md-9'>
			<div class="readthis alert-info">
				<ul>
					<?php echo $content_xml->buy->basket->info_blob; ?>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="container basket payment_overview vspace-top-half">
	<div class="row">
		<div class="col-md-9">
			<div class="light-gray background-wrapper">
				<h2 class="payment_underline">
					<?php echo $content_xml->buy->basket->basket_title; ?>
				</h2>
				<div class="row payment_underline productheader">
					<div class="col-xs-0 col-sm-5 text-center col-header"></div>
					<div class="col-xs-6 col-sm-4 text-center col-header">
						<?php echo $content_xml->buy->basket->unit_title; ?>
					</div>
					<div class="col-xs-6 col-sm-3 text-right col-header">
						<?php echo $content_xml->buy->basket->subtotal_title; ?>
					</div>
				</div>
					
				<?php 
				
					// Post
				
					if($isProductSelected){ 
						$isFirst = true;
						foreach( $_POST as $key => $qty){
							if( $key != 'productselect'){ // ignore productselect in POST
								
								if($qty > 0){
									$price = $products ["$key"] ["price"] [$activeCurrency];
									
									$_SESSION ["pot_basket"] ["items"] [$key] ["qty"] = $qty;
									$_SESSION ["pot_basket"] ["items"] [$key] ["price"] = $price;
									
									$product_total = ($price * $qty);
									
									if($isFirst) {
										$isFirst = false;
										echo "<div class='row productrow'>";
									}else{
										echo "<div class='row productrow payment_aboveline'>";
									}
											echo "<div class='col-xs-12 col-sm-5 productname'>";
												$productname = $key . "_name";
												echo $content_xml->buy->basket->$productname;
											echo "</div>";

											echo "<div class='col-xs-6 col-sm-4 text-center'>";
												echo $qty . " x " . $price . " " . $currency[$activeCurrency];
											echo "</div>";

											echo "<div class='col-xs-6 col-sm-3 text-right'>";
												echo '<b>' . $product_total . " " . $currency[$activeCurrency] . "</b>";
											echo "</div>";

										echo "</div>";

									$total_price = $total_price + $product_total;
								}
							}
						}
						$flow = "a";
					}
					
					// Cookie
					
					if (isset($_SESSION["pot_basket"]["design"]["id"])) {
						
						foreach ($_SESSION ["pot_basket"] ["items"] as $key => $value) {
							
							$qty = $value["qty"];
							$price = $value["price"];
							
							$product_total = ($price * $qty);
							
							echo "<div class='row productrow'>";
							
							echo "<div class='col-xs-12 col-sm-5 productname'>";
							$productname = $key . "_name";
							echo $content_xml->buy->basket->$productname;
							echo "</div>";
							
							echo "<div class='col-xs-6 col-sm-4 text-center'>";
							echo $qty . " x " . $price . " " . $currency[$activeCurrency];
							echo "</div>";
							
							echo "<div class='col-xs-6 col-sm-3 text-right'>";
							echo '<b>' . $product_total . " " . $currency[$activeCurrency] . "</b>";
							echo "</div>";
							
							echo "</div>";
							
							$total_price = $total_price + $product_total;
							
						}
						$flow = "b";
					}
					
				?>

				<div class="row totals">
					<div class="">
						<div class="payment_total">
							<span>
								<?php echo $content_xml->buy->basket->total; ?>
							</span>
							<span class="right">
								<?php echo $total_price . " " . $currency[$activeCurrency]; ?>
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 order_edit">
					<?php 
					if (!isset($_SESSION['pot_basket']['design'])) {
						echo "<a href=".$language . "/" . getMenuLinkByID(4, $structure_xml) . "?c=1>" . $content_xml->buy->basket->label->previous . "</a><br>";
					}
					?>
				</div>
			</div>

		</div>
	</div>
</div>

<div class="container basket image vspace-top-half">
	<div class="row">
		<div class="col-md-9">
			<div class="container-overlay">
				<div class="dark">
					<?php echo $content_xml->buy->basket->message->dark; ?>
				</div>
				<div class="light">
					<ul>
						<?php echo $content_xml->buy->basket->message->light; ?>
					</ul>
				</div>
			</div>
			<img src="content/_global/images/payment_message.jpg"/>
		</div>
	</div>
</div>
<?php //echo "$language/" . getMenuLinkByID(42, $structure_xml);?>
<form method='POST' action='<?php echo "$language/" . getMenuLinkByID(42, $structure_xml);?>' role="form" id='basket_form' class="form-horizontal">
	<div class="container basket vspace-top-full">
		<div class="form-group">
			<label for='email' class="col-sm-3 control-label"><?php echo $content_xml->buy->basket->label->email; ?></label>
			<div class="col-sm-6 col-md-4">
				<input type='text' name='email' id='email' class='form-control required email' x-autocompletetype="email" 
					value='<?php echo isset($_SESSION['pot_basket']["user"]["email"]) ? $_SESSION['pot_basket']["user"]["email"] : "";?>'
					data-validation-required-message="<?php echo $content_xml->buy->basket->error->email; ?>">
					<br>
			</div>
		</div>
		<div class="form-group">
			<label for='confirm_email' class="col-sm-3 control-label"><?php echo $content_xml->buy->basket->label->confirm_email; ?></label>
			<div class="col-sm-6 col-md-4">
				<input type='text' name='confirm_email' id='confirm_email' class='form-control required email'
					value='<?php echo isset($_SESSION['pot_basket']["user"]["email"]) ? $_SESSION['pot_basket']["user"]["email"] : "";?>'
					data-validation-required-message="<?php echo $content_xml->buy->basket->error->email; ?>"
					data-validation-matches-message="<?php echo $content_xml->buy->basket->error->email_confirm; ?>">
					<br>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-3"></div>
			<div class="col-sm-6 col-md-5 payment_agree">
				<div class="input-group checkbox">
				<input type='checkbox' id='terms' name='terms' 
					data-validation-required-message="<?php echo $content_xml->buy->basket->error->terms; ?>"> <label for='terms' class="control-label"><?php echo $content_xml->buy->basket->terms; ?></label>
				</div>
			</div>
		</div>
	</div>

	<div class="container basket vspace-full">
		<div class="row">
			<div class="col-xs-12 col-md-9 text-right payment_ogone">
				<div class="cta">
					<button class='btn btn-primary' name='basket' type='submit'>
						<?php echo $content_xml->buy->basket->label->next; ?>
					</button>
				</div>
				<div class="ogone_details"> 
					<span class='ogone_handledby'><?php echo $content_xml->buy->basket->ogone_label; ?></span>
					<img class='ogone_icon' src='content/_global/images/icon_ogone.png' width="62" height="15"/>
				</div>
			</div>
		</div>
	</div>
</form>

<?php
	// Define language
	if (in_array("$language", $ogone_language_array) == true) {
		$ogone_language = $ogone_language_array["$ogone_language"];
	} else {
		// Default language = English
		$ogone_language = "en_US";
	}

	$_SESSION ["pot_basket"] ["currency"] = $activeCurrency;
	$_SESSION ["pot_basket"] ["total"] = $total_price;
	$_SESSION ["pot_basket"] ["ogone_language"] = $ogone_language;
	$_SESSION ["pot_basket"] ["flow"] = $flow;
	$_SESSION ['pot_basket_created'] = time();


	
	if ($debug == true) {
		explore($_SESSION);
	}	
?>

<!-- <div class="modal fade" id="disclaimer_modal" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title blue" id="modal-title"><?php echo $content_xml->buy->basket->terms_title; ?></h4>
			</div>
			<div class="modal-body">
				<?php echo $content_xml->buy->basket->terms_body; ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="specific_modal" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title blue" id="modal-title2"><?php echo $content_xml->buy->basket->specific_terms_title; ?></h4>
			</div>
			<div class="modal-body">
				<?php echo $content_xml->buy->basket->specific_terms_body; ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>-->

