<?php
	/*
	$test = FALSE;

	if( isset($_GET['x']) ){
		$test = ($_GET['x'] == 1 )? TRUE : FALSE;
	}

	if (isset($_SESSION['pot_basket']['flow']) && $_SESSION['pot_basket']['flow'] == 'l') {
		$token = $_SESSION ["pot_basket"]['tfl']["token"];
	}else{
		$flow = $_SESSION["pot_basket"]["flow"] = 'l';
		$email = ""; //$_SESSION['pot_basket']['user']['email'] = $fts_email;
		$_SESSION['pot_basket']['total'] = 40;

		$currency = $_SESSION["pot_basket"]["currency"] = "EUR";
		$ogone_language = $_SESSION["pot_basket"]["ogone_language"] = "en_US";
		$_SESSION['pot_basket_created'] = time();

		$_SESSION ["pot_basket"]["items"]['product_3'] ='';
		$_SESSION ["pot_basket"]["items"]['product_3']['qty'] = 1;
		$_SESSION ["pot_basket"]["items"]['product_3']['price'] = 40;

		$amount = $_SESSION['pot_basket']['total'] * 100;
		// Generate order id
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-orderid.php";
		// Insert order + order_items in database
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/save-order-autopay.php";
		// generate credit
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-credit.php";
		$order_db_id = retrieveOrderDBid($db, $order_id);
		$token = '';
		generateCredit($db,$order_id,$order_db_id, "large", 0, '', $token);
		$_SESSION ["pot_basket"]['tfl']["token"] = $token;
	}
	*/
?>

<div class="tfl_homepage container stone_bg">
	<div class="row">
		<div class="col-xs-12">
			<div class="tablet">
				<p>The winner gets the most desired gift of all times:<br><strong>a free tomorrowland ticket for life<br>for him/her and 3 friends.</strong></p>
				<p>
					All you have to do:<br>
					1. Create the most beautifull design OR
					<br>
					2. Write the most brilliant positive message OR
					<br>
					3. Just be brilliant and do BOTH
				</p>
				<div class="buttons">
					<div class="button">
						<h4>Feeling really creative?</h4>
						<a href="<?php echo $base_path . $language . '/participate/create#/upload/tomorrowlandforlife'; ?>" class='tfl_button upload'>Upload your own Design</a>
					</div>
					<div class="button">
						<h4>Use our Editor instead</h4>
						<a href="<?php echo $base_path . $language . '/participate/create#/large'?>" class='tfl_button editor'>Go to the online Editor</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
