<div class="fullscreen-container media-container">
	<img src="content/_global/images/founders_header.jpg">
</div>


<div class='container vspace-full'>
	<div class="row">
		<div class='col-md-8'>
			<h1><?php echo $content_xml->founders->title; ?></h1>
			<?php echo $content_xml->founders->body; ?>
		</div>
		<div class='col-md-4 sidebar'>
			<?php 
				require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_sidebar_community.tpl.php';
				getSidebarCommunityText($content_xml); 

				require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_socialButton.tpl.php';
			?>
		</div>
	</div>
</div>