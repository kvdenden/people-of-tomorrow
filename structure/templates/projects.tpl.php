
<div class="fullscreen-container media-container">
	<div class='container-overlay'>
		<div class='container vspace-top-full projects-overlay'>
			<div class="row">
				<div class="col-xs-12">
					<div class="oneworld-logo">
						<?php require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_header_title.tpl.php'; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class='oneworld-slogan'><?php echo $content_xml->home->title_subtext; ?></div>
				</div>
			</div>
			<div class="row vspace-top-half">
				<div class="col-xs-12">
					<div class='oneworld-quote'>
						<span class="dash">-</span> <?php echo $content_xml->home->title_quote; ?> <span class="dash">-</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<img src="content/_global/images/projects_header.jpg" />
</div>

<div class='container vspace-full projects'>
	<div class='row vspace-bottom-full'>
		<div class='col-md-4 sidebar'>
			<h2><?php echo $content_xml->projects->trailer->title; ?></h2>
			<p class='emphasis'><?php echo $content_xml->projects->trailer->text; ?></p>
			<p class="">
				<?php echo $content_xml->projects->trailer->info_text; ?>
				<a href='<?php echo $language . '/' . getMenuLinkByID(4, $structure_xml); ?>'><?php echo $content_xml->projects->trailer->info_text_link_label; ?></a> 
			</p>
			<br>
			<a class="btn btn-primary btn-large facebook-icon" href="https://www.facebook.com/arnequinze" target='_blank'><?php echo $content_xml->projects->trailer->cta;?></a>
		</div>
		
		<div class='col-md-8'>
			<iframe src="//player.vimeo.com/video/80568584?title=0&amp;byline=0&amp;portrait=0&amp;autoplay=1" width="100%" height="268" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>
		</div>
	</div>

	<div class="row">
		<div class='col-md-8'>
			<h2> <?php echo $content_xml->projects->title; ?></h2>
			<?php echo $content_xml->projects->body; ?>
		</div>
		<div class='col-md-4 sidebar'>
			<?php 
				require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_sidebar_community.tpl.php';
				getSidebarCommunityText($content_xml); 

				require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_socialButton.tpl.php';
			?>
			<img src="content/_global/images/projects_side.jpg" class="side">
		</div>
	</div>
	<div class="row vspace-top-half">
		<div class='col-xs-12'>
			<img src="content/_global/images/projects_small.jpg" class="small" >
			<img src="content/_global/images/projects_big.jpg" class="big" >
		</div>
	</div>
	<div class="row vspace-top-half">
		<div class='col-md-8'>
			<?php echo $content_xml->projects->body2; ?>
		</div>
	</div>
</div>