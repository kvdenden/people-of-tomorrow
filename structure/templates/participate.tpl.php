<div class="fullscreen-container media-container participate">
	<div class="container-overlay vspace-top-full">
		<div class='container'>
			<div class="row">
				<div class='col-sm-3 col-xs-12'>
					<div class='cta'><a class='btn btn-primary btn-lg' href="<?php echo $language . '/' . getMenuLinkByID(4, $structure_xml); ?>"><?php echo $content_xml->participate->title_cta; ?></a></div>
				</div>
				<div class='col-sm-9 col-xs-12'>
					<div class="overlay-centered dark smallfix">
						<h1 class="text-center"><?php echo $content_xml->participate->title_text->title;?></h1>
						<p class="text-center"><?php echo $content_xml->participate->title_text->body;?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<img src="content/_global/images/participate_step01.jpg">
</div>

<div class='container vspace-full'>
	<div class='row'>
		<div class='col-md-4 col-sm-6 col-xs-12'>
			<h2><?php echo $content_xml->participate->title;?></h2>
			<p><?php echo $content_xml->participate->body;?></p>
		</div>
		<div class='col-md-8 col-sm-6 col-xs-12'>
			<iframe class='youtube_video' width="" height="" src="<?php echo $global_xml->trailer['href'];?>" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
</div>


<div class="fullscreen-container media-container participate">
	<div class="container-overlay">
		<div class='container'>
			<div class="row">
				<div class='col-xs-12'>
					<div class="overlay-centered light">
						<h1 class="text-center"><?php echo $content_xml->participate->overlay01->title;?></h1>
						<p class="text-center"><?php echo $content_xml->participate->overlay01->body;?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<img src="content/_global/images/participate_step02.jpg">
</div>

<div class='container col-3 colEqualHeight vspace-full participate'>
	<div class='row'>
		<div class="col-sm-4 first">
			<h3><?php echo $content_xml->participate->columns01->col01->title?></h3>
			<p><?php echo $content_xml->participate->columns01->col01->body?></p>
		</div>
		<div class="col-sm-4">
			<h3><?php echo $content_xml->participate->columns01->col02->title?></h3>
			<p><?php echo $content_xml->participate->columns01->col02->body?></p>
		</div>
		<div class="col-sm-4 last">
			<h3><?php echo $content_xml->participate->columns01->col03->title?></h3>
			<p><?php echo $content_xml->participate->columns01->col03->body?></p>
		</div>
	</div>
</div>

<div class="fullscreen-container media-container participate">
	<div class="container-overlay">
		<div class='container'>
			<div class="row">
				<div class='col-md-8 col-xs-12'>
					<div class="overlay-centered dark">
						<h1 class="text-center"><?php echo $content_xml->participate->overlay02->title;?></h1>
						<p class="text-center"><?php echo $content_xml->participate->overlay02->body;?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<img class="alignRight" src="content/_global/images/participate_step03.jpg">
</div>

<div class='container col-3-type2 colEqualHeight vspace-full participate'>
	<div class='row'>
		<div class="col-sm-4 first">
			<p><?php echo $content_xml->participate->columns02->col01->body?></p>
			<h3><?php echo $content_xml->participate->columns02->col01->title?></h3>
		</div>
		<span><?php echo $content_xml->participate->columns02->separator; ?></span>
		<div class="col-sm-4">
			<p><?php echo $content_xml->participate->columns02->col02->body?></p>
			<h3><?php echo $content_xml->participate->columns02->col02->title?></h3>
		</div>
		<span><?php echo $content_xml->participate->columns02->separator; ?></span>
		<div class="col-sm-4 last">
			<p><?php echo $content_xml->participate->columns02->col03->body?></p>
			<h3><?php echo $content_xml->participate->columns02->col03->title?></h3>
		</div>
	</div>
</div>

<div class='container vspace-bottom-full'>
	<div class='row'>
		<div class='col-xs-12'>
			<div class='cta text-center'>
				<a class='btn btn-primary btn-lg<?php echo isset($_SESSION['pot_basket']['design']) ? " reset-basket" : ""?>' href="<?php echo $language . '/' . getMenuLinkByID(4, $structure_xml); ?>">
					<?php echo $content_xml->participate->bottom_cta; ?>
				</a>
			</div>
		</div>
	</div>
</div>