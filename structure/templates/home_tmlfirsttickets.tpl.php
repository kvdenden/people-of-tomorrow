<?php
	// do crazy stuff :) 

	// 1. Create order for user
	// 2. Create credit for user
	// 3. Save order and credit in session to be picked up later
	// 4. Save flow in session 
	$test = FALSE;

	if( isset($_GET['x']) ){
		$test = ($_GET['x'] == 1 )? TRUE : FALSE;
	}

	if (isset($_SESSION['pot_basket']['flow']) && $_SESSION['pot_basket']['flow'] == 't') {
		$token = $_SESSION ["pot_basket"]['tfs']["token"];
	}else{
		$flow = $_SESSION["pot_basket"]["flow"] = 't';
		$email = ""; //$_SESSION['pot_basket']['user']['email'] = $fts_email;
		$_SESSION['pot_basket']['total'] = 40;
		
		$currency = $_SESSION["pot_basket"]["currency"] = "EUR";
		$ogone_language = $_SESSION["pot_basket"]["ogone_language"] = "en_US";
		$_SESSION['pot_basket_created'] = time();

		$_SESSION ["pot_basket"]["items"]['product_3'] ='';
		$_SESSION ["pot_basket"]["items"]['product_3']['qty'] = 1;
		$_SESSION ["pot_basket"]["items"]['product_3']['price'] = 40;

		$amount = $_SESSION['pot_basket']['total'] * 100;
		// Generate order id
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-orderid.php";
		// Insert order + order_items in database
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/save-order-autopay.php";
		// generate credit
		require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-credit.php";
		$order_db_id = retrieveOrderDBid($db, $order_id);
		$token = '';
		generateCredit($db,$order_id,$order_db_id, "large", 0, '', $token);
		$_SESSION ["pot_basket"]['tfs']["token"] = $token;
	}
	
?>

<div class="fullscreen-container container fts_bg tmlfts">
	<div class='dots'>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 tml_logo">
					<img src="images/tml/tml-10yrs.png">
					<?php 
						if($test == TRUE){
							explore($order_id);
							explore($order_db_id);
							explore($token);
						}
					?>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2 tml_style">
					<p> <?php echo $content_xml->fts->title; ?> </p>
					<h2> <?php echo $content_xml->fts->body; ?> </h2>
					<div class="big"><?php echo $content_xml->fts->title2; ?></div>
					<div class="quote"><?php echo $content_xml->fts->quote; ?></div>
					<a class="tml_button" href="<?php echo $base_path . $language . "/participate/create/#large" ?>"><?php echo $content_xml->fts->cta; ?></a>
					<div class="photogrid">
						<div class="photo-item"> <img src='content/_global/images/homepage/people/01.jpg'> </div>
						<div class="photo-item"> <img src='content/_global/images/homepage/message/04.jpg'> </div>
						<div class="photo-item"> <img src='content/_global/images/homepage/message/09.jpg'> </div>
						<div class="photo-item"> <img src='content/_global/images/homepage/message/08.jpg'> </div>
						<div class="photo-item"> <img src='content/_global/images/homepage/message/07.jpg'> </div>
						<div class="photo-item"> <img src='content/_global/images/homepage/message/02.jpg'> </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>