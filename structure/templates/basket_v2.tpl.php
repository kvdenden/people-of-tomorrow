<?php
	//
	//$debug = true;
	if($debug === true){
		$_SESSION["pot_basket"]["design"]["design_type"] = "large custom";
		$_SESSION["pot_basket"]["design"]["id"] = "1";
	}
	//

	if(!isset($_SESSION["pot_basket"]["design"]["id"])){
		header ( sprintf ( "Location: %s", $base_path . $language. "/" . getMenuLinkByID(4, $structure_xml) ) );
	}

	$design_type_tmp =  $_SESSION["pot_basket"]["design"]["design_type"];
	$product_type = '';
	$label_type = '';
	$price = 0;



	if(($design_type_tmp == "small square") || (($design_type_tmp == "small rectangle"))){
		$product_type = 'product_1';
		$price = 10;
		$label_type = ($design_type_tmp == "small square")? 'square' : 'rectangle';
	}else if(($design_type_tmp == "large") || (($design_type_tmp == "large custom"))){
		$product_type = 'product_3';
		$price = 40;
		$label_type = ($design_type_tmp == "large")? 'large' : 'custom';
	}

	$_SESSION ["pot_basket"]["items"] = "";
	$_SESSION ["pot_basket"]["items"][$product_type] = '';
	$_SESSION ["pot_basket"]["items"][$product_type]['qty'] = 1;
	$_SESSION ["pot_basket"]["items"][$product_type]['price'] = $price;

	/*
	$hideEmail = (isset($_SESSION['pot_basket']["user"]["email"]) )? true : false;
	$isFree = false; // depends if the submit happened with a code or not.
	$hidePayement = false; //

	$isNormalFlow = false;
	$isGiftFlow = false;
	$isFreeFlow = false;
	$isNeighbourFlow = false;


	if( $isGiftFlow || $isNeighbourFlow){
		// redirect to confirmation page
	}
	*/
	// options:
		// NORMAL:		SHOW payment 	- 	SHOW email 	-	SHOW buy button
		// GIFT: 		hide payement 	-	hide email 	-	hide buy 				// ENTERED CODE ==> automatic redirect to confirmaton page
		// FREE:		hide payement 	- 	SHOW email 	- 	hide buy (show next) 	// ENTERED CODE
		// NEIGHBOUR	hide payement 	- 	hide email 	- 	hide buy 				// ENTERED CODE ==> automatic redirect to confirmaton page
?>

<div class="fullscreen-container container basket summary">
	<div class='row'>
		<div class="container vspace-full">
			<div class='row title'>
				<div class='col-sm-3 hidden-xs'></div>
				<div class='col-xs-6 col-sm-3 '>
					<?php echo $content_xml->buy->basket->basket_title; ?>
				</div>
				<div class="col-xs-6 col-sm-3">
					<?php echo $content_xml->buy->basket->total;?>
				</div>
			</div>

			<div class='row title content'>
				<div class='col-sm-3 hidden-xs'></div>
				<div class='col-xs-6 col-sm-3'>
					<?php echo $content_xml->buy->basket_v2->product->$label_type; ?>
				</div>
				<div class="col-xs-6 col-sm-3">
					<?php echo $price . " " . $currency[$activeCurrency]; ?>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container basket">
	<div class="row">
		<div class="col-xs-12">
			<form method='POST' action='<?php echo "$language/" . getMenuLinkByID(42, $structure_xml);?>' role="form" id='basket_form' class="form-horizontal">
				<div class="container basket vspace-top-full">
					<div class="form-group">
						<div class="col-sm-2 hidden-xs"></div>
						<label for='email' class="col-sm-2 control-label"><?php echo $content_xml->buy->basket->label->email; ?></label>
						<div class="col-sm-6 col-md-4 col-input">
							<input type='text' name='email' id='email' class='form-control required email' x-autocompletetype="email"
								value='<?php echo isset($_SESSION['pot_basket']["user"]["email"]) ? $_SESSION['pot_basket']["user"]["email"] : "";?>'
								data-validation-required-message="<?php echo $content_xml->buy->basket->error->email; ?>">
								<br>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2 hidden-xs"></div>
						<label for='confirm_email' class="col-sm-2 control-label"><?php echo $content_xml->buy->basket->label->confirm_email; ?></label>
						<div class="col-sm-6 col-md-4 col-input">
							<input type='text' name='confirm_email' id='confirm_email' class='form-control required email'
								value='<?php echo isset($_SESSION['pot_basket']["user"]["email"]) ? $_SESSION['pot_basket']["user"]["email"] : "";?>'
								data-validation-required-message="<?php echo $content_xml->buy->basket->error->email; ?>"
								data-validation-matches-message="<?php echo $content_xml->buy->basket->error->email_confirm; ?>">
								<br>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-2 col-sm-offset-2"></div>
						<div class="col-xs-6 col-checkbox payment_agree">
							<div class="input-group checkbox">
								<input type='checkbox' id='terms' name='terms'
									data-validation-required-message="<?php echo $content_xml->buy->basket->error->terms; ?>"> <label for='terms' class="control-label checkbox"><?php echo $content_xml->buy->basket->terms; ?></label>
								</div>
						</div>
					</div>
				</div>

				<div class="container basket vspace-half">
					<div class="row">
						<div class="col-sm-2 col-sm-offset-3"></div>
						<div class="col-xs-3 col-checkbox payment_agree">
							<div class="cta">
								<button class='btn btn-primary btn-submit' name='basket' type='submit'>
									<?php echo $content_xml->buy->basket_v2->cta_button; ?>
								</button>
							</div>
							<div class="ogone_details">
								<span class='ogone_handledby'><?php echo $content_xml->buy->basket->ogone_label; ?></span>
								<img class='ogone_icon' src='content/_global/images/icon_ogone.png' width="139" height="52"/>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
	// Define language
	if (in_array("$language", $ogone_language_array) == true) {
		$ogone_language = $ogone_language_array["$ogone_language"];
	} else {
		// Default language = English
		$ogone_language = "en_US";
	}

	$_SESSION ["pot_basket"] ["currency"] = $activeCurrency;
	$_SESSION ["pot_basket"] ["total"] = $price;
	$_SESSION ["pot_basket"] ["ogone_language"] = $ogone_language;
	$_SESSION ["pot_basket"] ["flow"] = "b";
	$_SESSION ['pot_basket_created'] = time();



	if ($debug == true) {
		explore($_SESSION);
	}
?>
