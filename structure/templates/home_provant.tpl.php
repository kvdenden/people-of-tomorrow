<?php
	// do crazy stuff :)

	// 1. Create order for user
	// 2. Create credit for user
	// 3. Save order and credit in session to be picked up later
	// 4. Save flow in session
	// $test = FALSE;

	// if( isset($_GET['x']) ){
	// 	$test = ($_GET['x'] == 1 )? TRUE : FALSE;
	// }

	// if (isset($_SESSION['pot_basket']['flow']) && $_SESSION['pot_basket']['flow'] == 'p') {
	// 	$token = $_SESSION ["pot_basket"]['open']["token"];
	// }else{
	// 	$flow = $_SESSION["pot_basket"]["flow"] = 'p';
	// 	$email = ""; //$_SESSION['pot_basket']['user']['email'] = $fts_email;
	// 	$_SESSION['pot_basket']['total'] = 40;

	// 	$currency = $_SESSION["pot_basket"]["currency"] = "EUR";
	// 	$ogone_language = $_SESSION["pot_basket"]["ogone_language"] = "en_US";
	// 	$_SESSION['pot_basket_created'] = time();

	// 	$_SESSION ["pot_basket"]["items"]['product_3'] ='';
	// 	$_SESSION ["pot_basket"]["items"]['product_3']['qty'] = 1;
	// 	$_SESSION ["pot_basket"]["items"]['product_3']['price'] = 40;

	// 	$amount = $_SESSION['pot_basket']['total'] * 100;
	// 	// Generate order id
	// 	require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-orderid.php";
	// 	// Insert order + order_items in database
	// 	require_once realpath(dirname(__FILE__) . '/..') . "/_private/save-order-autopay.php";
	// 	// generate credit
	// 	require_once realpath(dirname(__FILE__) . '/..') . "/_private/generate-credit.php";
	// 	$order_db_id = retrieveOrderDBid($db, $order_id);
	// 	$token = '';
	// 	generateCredit($db,$order_id,$order_db_id, "large", 0, '', $token);
	// 	$_SESSION ["pot_basket"]['open']["token"] = $token;
	// }
?>

<div class="fullscreen-container container homeopen">
	<div class="container productselectv2">
		<div class="row">
			<div class="col-xs-12">
				<ul id="slider" class='rslides'>
					<li><img src="/images/slideshow/vb_aoki.jpeg"/></li>
					<li><img src="/images/slideshow/vb_carlcox.jpeg"/></li>
					<li><img src="/images/slideshow/vb_julien.jpeg"/></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="container homeopen content">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2" style="text-align: left;">
				<h2>Een boodschap van jou en je collega’s op de One World brug van Arne Quinze!</h2>
				<p>In mei starten in De Schorre de werken aan de installatie ‘One World by People of Tomorrow’. Dit kunstwerk van Arne Quinze zal, net zoals Tomorrowland dat al 10 jaar doet in De Schorre, mensen van over de hele wereld verenigen. Het kunstwerk wordt een permanente brug opgebouwd uit duizenden houten latjes. Iedereen kan een latje aankopen en er een persoonlijke boodschap op laten zetten. De permanente brug wordt een prachtig permanent wandel- en fietspad dat voor een betere ontsluiting van de natuur en het domein zal zorgen. </p>

				<h2>Hoe ga je te werk?</h2>
				<p>Geef jouw of jullie ontwerp in vóór 1 juni via de link hieronder. Je kunt aan de slag met de opgegeven lettertypes en illustraties. Heb je een creatieve collega? Dan kun je een eigen ontwerp uploaden. De latjes worden allemaal gescreend. Positieve boodschappen met een originele insteek die gelinkt zijn aan (een dienst of product van) de provincie worden aangemaakt op een latje van 43cm x 11cm en vormen één van de duizenden latjes op de brug. Let op: het is niet de bedoeling dat je een puur commerciële boodschap ingeeft. Een logo wordt niet aanvaard als boodschap. Ook moet de boodschap verwijzen naar wat jouw dienst of de provincie doet, het mag niet louter een persoonlijke boodschap zijn. </p>
				<p>In de week van 2 juni kiezen we de meest originele boodschap. Deze wordt gedrukt op een grote lat van 2 meter.</p>
				<p>Laat je creativiteit de vrije loop en laat iedereen weten welke fantastische dingen de provincie doet!</p>
				<p>Nog vragen? Contacteer Tina Van Leuven via  <a href="mailto:tina.vanleuven@deschorre.provant.be">tina.vanleuven@deschorre.provant.be</a> of tel. 03 88 0 76 43.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
				<a class="cta" href="<?php echo $base_path . $language . "/participate/create/#/provant/large" ?>">Klik hier om je eigen ontwerp te maken</a>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="photogrid">
					<div class="photo-item"> <img src='content/_global/images/homepage/people/01.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/04.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/09.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/08.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/07.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/message/02.jpg'> </div>
					<div class="photo-item"> <img src='content/_global/images/homepage/art/01.jpg'></div>
					<div class="photo-item"> <img src='content/_global/images/homepage/people/03.jpg'></div>
				</div>
			</div>
		</div>

		<?php if($test): ?>
			<div class="row">
				<div class="col-xs-12">
					<?php
						echo "<pre>";
							print_r($_SESSION);
						echo "</pre>";
					?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
