<div class="fullscreen-container media-container">
	<img src="content/_global/images/whoarewe_header.jpg">
</div>

<div class='container vspace-full'>
	<div class="row">
		<div class='col-md-8'>
			<h1><?php echo $content_xml->who->title; ?></h1>
			<?php echo $content_xml->who->body; ?>
			<!--<p><a class='btn btn-primary' href="<?php //echo $language . '/' . getMenuLinkByID(51, $structure_xml); ?>"><?php //echo $content_xml->who->cta_label; ?></a></p>-->
		</div>
		<div class='col-md-4 sidebar'>
			<?php 
				require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_sidebar_community.tpl.php';
				getSidebarCommunityText($content_xml); 

				require_once realpath(dirname(__FILE__) . '/..') . '/snippets/_socialButton.tpl.php';
			?>
		</div>
	</div>
</div>