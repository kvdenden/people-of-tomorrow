<!DOCTYPE html>
<html lang="<?php echo $language; ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<meta name="application-name" content="Tomorrowland Sculpture"/>
	<meta name="msapplication-TileColor" content="#333233"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
	<meta http-equiv="cleartype" content="on">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <base href="/" />

    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>

	<!--<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" />-->
	<link rel="stylesheet" type="text/css" href="/bower_components/jquery-ui/themes/base/jquery-ui.css">
	<!-- <link rel="stylesheet" type="text/css" href="css/libraries/jquery.vegas.min.css"/> -->
	<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css"/>
	<link rel="stylesheet" type="text/css" href="/css/editor.css"/>
	<link rel="stylesheet" type="text/css" href="/css/shop.css"/>
	<link rel="stylesheet" type="text/css" href="/css/responsiveslides.css"/>

	<?php if($activmenuId != 42) { ?>
		<title><?php echo $page_title; ?></title>
   	<?php } ?>
</head>
<?php if (($header_type == "tfl")): ?>
	<body class="tfl_bg">
<?php else: ?>
	<body>
<?php endif; ?>
<?php if($activmenuId != 42) { ?>
	<div id="fb-root"></div>
	<script>
	  window.fbAsyncInit = function() {
	    // init the FB JS SDK
	    FB.init({
	      appId      : '691661990866871',                    // App ID from the app dashboard
	      status     : true,                                 // Check Facebook Login status
	      xfbml      : true                                  // Look for social plugins on the page
	    });
	    // Additional initialization code such as adding Event Listeners goes here
	  };
	  // Load the SDK asynchronously
	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/all.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	 <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!--[if lt IE 9]>
      <script src="/bower_components/es5-shim/es5-shim.js"></script>
      <script src="/bower_components/json3/lib/json3.min.js"></script>
    <![endif]-->
<?php }?>
