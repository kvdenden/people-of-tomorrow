<nav id='sub-navigation' role='sub-navigation'>
	<ul>
		<?php 
			foreach( $structure_xml->navitem[1] as $menuitem ){
				if( activesubmenu(0) == $menuitem['link']){
					echo "<li class='active'>";
				}else{
					echo "<li>";
				}
				
					echo "<a href='". activesubmenu(2) . "/". activesubmenu(1) . "/" . $menuitem['link'] ."' target='_self'>" . $menuitem['title'] . "</a>";
					if( count( $menuitem->anchor ) > 0){
						echo "<ul>";
						foreach( $menuitem->anchor as $anchor ){
							echo "<li>";
								echo "<a href='". activesubmenu(2) . "/". activesubmenu(1) . "/" . $menuitem['link'] ."#". $anchor['link'] ."' class='scrollto' id='". $anchor['link'] ."'>";
									echo "<span class='.glyphicon .glyphicon-chevron-right'></span>";
									echo $anchor["title"];
								echo "</a>";
							echo "</li>";
						}

						echo "</ul>";
					}
				echo "</li>";
			}
		?>
	</ul>
</nav>