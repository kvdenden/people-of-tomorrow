<?php if(($header_type == "home")){ ?>
	<header class='home'>
		<div class="container">
			<div class="row">
				<div class="resizer">
					<!-- logo -->
					<div class="col-xs-12 col-md-3 col-sm-4 col-logo">
						<div id="logo">
							<a href="<?php echo $language . '/home'; ?>">
								<img src="/images/pot_logo.svg"/>
							</a>
						</div>
						<div id="lang_wrapper" class="hidden-md hidden-lg">
							<?php require 'structure/snippets/_languageSwitch.tpl.php';?>
						</div>
					</div>

					<!-- menu -->
					<div class='col-xs-12 col-md-3 col-sm-4 col-menu'>
						<!-- social -->
						<div class="social">
							<a class="facebook" href="http://www.facebook.com/peopleoftomorrowofficial" target="_blank">
								<div class="fa"></div>
							</a>

							<a class="twitter" href="http://www.twitter.com/POTomorrow" target="_blank">
								<div class="fa"></div>
							</a>

							<a class="instagram" href="http://instagram.com/peopleoftomorrowofficial" target="_blank">
								<div class="fa"></div>
							</a>
						</div>

						<!-- learnmore -->
						<a class="menuitem learnmore" href="<?php echo $language . '/' . getMenuLinkByID(13, $structure_xml); ?>">
							<div class="label">
								<?php echo getMenuTitleByID(13, $structure_xml);?>
							</div>
							<div class='glyphicon glyphicon-info-sign'></div>
						</a>

						<!-- gift -->
						<!--
						<a class="menuitem gift visible-xs visible-sm" href="<?php //echo $language . '/' . getMenuLinkByID(15, $structure_xml); ?>">
							<div class="label">
								<?php //echo getMenuTitleByID(15, $structure_xml); ?>
							</div>
							<div class='glyphicon glyphicon-gift'></div>
						</a>
						-->
					</div>

					<!-- cta -->
					<div class="col-xs-12 col-md-3 col-sm-4 col-cta blue hidden-xs"></div>

					<!-- gift -->
					<div class='col-xs-12 col-md-3 col-sm-4 col-gift hidden-xs hidden-sm'>
						<?php require 'structure/snippets/_languageSwitch.tpl.php';?>
						<!--
						<a class="menuitem gift" href="<?php //echo $language . '/' . getMenuLinkByID(15, $structure_xml); ?>">
							<div class="label">
								<?php //echo getMenuTitleByID(15, $structure_xml); ?>
							</div>
							<div class='glyphicon glyphicon-gift'></div>
						</a>
						-->
					</div>

				</div>
			</div>
		</div>
	</header>

<?php }else if(($header_type == "learnmore")){ ?>

	<header class='learnmore'>
		<div class="container">
			<div class="row">
				<div class="resizer">

					<!-- logo -->
					<div class="col-xs-12 col-sm-4 col-md-3 col-logo">
						<div id="logo">
							<a href="<?php echo $language . '/home'; ?>">
								<img src="/images/pot_logo.svg"/>
							</a>
						</div>
					</div>

					<!-- menu -->
					<div class='col-xs-12 col-sm-4 col-md-3 col-menu hidden-sm'>
					</div>

					<!-- cta -->
					<div class="col-xs-12 col-sm-4 col-md-3 col-cta">
						<!--
						<a class="menuitem create" href="<?php //echo $language . '/' . getMenuLinkByID(4, $structure_xml); ?>">
							<div class="label">
								<?php
									if($language == "en"){
										//echo "Create your message <span>here</span>";
									}else if($language == "nl"){
										//echo "Maak je bericht <span>hier</span>";
									}
								?>
							</div>
						</a>
						-->
					</div>

					<!-- gift -->
					<div class='col-xs-12 col-sm-4 col-md-3 col-gift'>
						<?php require 'structure/snippets/_languageSwitch.tpl.php';?>
						<!--
						<a class="menuitem gift" href="<?php //echo $language . '/' . getMenuLinkByID(15, $structure_xml); ?>">
							<div class="label">
								<?php //echo getMenuTitleByID(15, $structure_xml); ?>
							</div>
							<div class='glyphicon glyphicon-gift'></div>
						</a>
					</div>
					-->
				</div>
			</div>
		</div>
	</header>

<?php }else if( ($header_type == "normal") || ($header_type == "")){ ?>
	<header class='normal'>
		<div class="container">
			<div class="row">
				<div class="resizer">

					<!-- logo -->
					<div class="col-xs-12 col-sm-4 col-md-3 col-logo">
						<div id="logo">
							<a href="<?php echo $language . '/home'; ?>">
								<img src="/images/pot_logo.svg"/>
							</a>
						</div>
						<div id="lang_wrapper" class="hidden-sm hidden-md hidden-lg">
							<?php require 'structure/snippets/_languageSwitch.tpl.php';?>
						</div>
					</div>

					<!-- menu -->
					<div class='col-xs-12 col-md-3 col-sm-4 col-menu'>
						<!-- social -->
						<div class="social">
							<a class="facebook" href="http://www.facebook.com/peopleoftomorrowofficial" target="_blank">
								<div class="fa"></div>
							</a>

							<a class="twitter" href="http://www.twitter.com/POTomorrow" target="_blank">
								<div class="fa"></div>
							</a>

							<a class="instagram" href="http://instagram.com/peopleoftomorrowofficial" target="_blank">
								<div class="fa"></div>
							</a>
						</div>

						<!-- learnmore -->
						<a class="menuitem learnmore" href="<?php echo $language . '/' . getMenuLinkByID(13, $structure_xml); ?>">
							<div class="label">
								<?php echo getMenuTitleByID(13, $structure_xml);?>
							</div>
							<div class='glyphicon glyphicon-info-sign'></div>
						</a>
					</div>

					<!-- menu -->
					<div class='col-xs-12 col-sm-4 col-md-3 col-menu hidden-sm'>
					</div>
					<div class='col-xs-12 col-sm-4 col-md-3'>
						<?php require 'structure/snippets/_languageSwitch.tpl.php';?>
					</div>
				</div>
			</div>
		</div>
	</header>
<?php }else if(($header_type == "nosocial")){ ?>
	<header class='normal'>
		<div class="container">
			<div class="row">
				<div class="resizer">

					<!-- logo -->
					<div class="col-xs-12 col-sm-4 col-md-3 col-logo">
						<div id="logo">
							<a href="<?php echo $language . '/home'; ?>">
								<img src="/images/pot_logo.svg"/>
							</a>
						</div>
					</div>

					<!-- menu -->
					<div class='col-xs-12 col-md-3 col-sm-4 col-menu'>
						<!-- learnmore -->
						<a class="menuitem learnmore" href="<?php echo $language . '/' . getMenuLinkByID(13, $structure_xml); ?>">
							<div class="label">
								<?php echo getMenuTitleByID(13, $structure_xml);?>
							</div>
							<div class='glyphicon glyphicon-info-sign'></div>
						</a>
					</div>

					<!-- menu -->
					<div class='col-xs-12 col-sm-4 col-md-3 col-menu hidden-sm'>
					</div>
				</div>
			</div>
		</div>
	</header>
<?php }else if(($header_type == "tfl")){ /* Tomorrowland For Life */ ?>
	<header class="tfl">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<img src="images/tfl/tfl-content_01.gif">
					<img src="images/tfl/tfl-content_02.gif">
				</div>
			</div>
		</div>
	</header><!-- /header -->
<?php }else if($header_type == "none"){
	/* don't show a header (orderdetails page) */
}
?>
