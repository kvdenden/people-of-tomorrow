<?php

$log_file = "error_log.log";
$timestamp = date ( 'Y-m-d H:i:s' );
error_log ( '[' . $timestamp . '] INFO: ' . "request received " . implode(", ", $_REQUEST) . PHP_EOL, 3, $log_file );
require_once realpath(dirname(__FILE__) . '/..') . "/helpers/shop.php";

// Offline status changes

foreach ( $_REQUEST as $key => $value ) {
	
	$key = strtoupper ( $key );
	if (in_array ( $key, $ogone_allowed_fields )) {
		$fields [$key] = $value;
	}
}
if (is_array ( $fields )) {
	ksort ( $fields );
	
	$shasign = $fields ['SHASIGN'];
	$sha_controle = generateShasign ( $fields, $passphrase_shaout );
	
	if ($shasign == $sha_controle) {
		
		$order_id = $fields ['ORDERID'];
		
		if (strpos ( $order_id, 'POT' ) !== FALSE)	{
			
			$pay_status = $fields ['STATUS'];
			$pay_id = $fields ['PAYID'];
			$transaction_date = datumformat_sql ( $fields ['TRXDATE'] );
			require_once 'db.php';
			try {
				if ($pay_status == 5 or $pay_status == 51 or $pay_status == 9 or $pay_status == 91) {
					$paid = 1;
				} else {
					$paid = 0;
				}
				
				$query = $db->prepare ( "UPDATE orders SET status = :status, paid = :paid, pay_id = :pay_id, transaction_date = :transaction_date WHERE order_id = :order_id LIMIT 1" );
				$success = $query->execute ( array ('status' => $pay_status, 'paid' => $paid, 'pay_id' => $pay_id, 'transaction_date' => $transaction_date, 'order_id' => $order_id ) );
				
				if (! $success) {
					error_log ( '[' . $timestamp . '] INFO: ' . "No DB update" . PHP_EOL, 3, $log_file );
				} else {
					error_log ( '[' . $timestamp . '] INFO: ' . "ORDER updated: $order_id" . PHP_EOL, 3, $log_file );
				}
			} catch ( Exception $e ) {
				error_log ( '[' . $timestamp . '] DBerror: ' . "$e" . PHP_EOL, 3, $log_file );
			}
		} else {
			error_log ( '[' . $timestamp . '] INFO: ' . "Order does not contain POT $order_id" . PHP_EOL, 3, $log_file );
		}
	} else {
		error_log ( '[' . $timestamp . '] INFO: ' . "SHA not correct -- SHA received = $shasign // SHA created = $sha_controle" . PHP_EOL, 3, $log_file );
	}
} else {
	error_log ( '[' . $timestamp . '] INFO: ' . "No array" . PHP_EOL, 3, $log_file );
}

?>