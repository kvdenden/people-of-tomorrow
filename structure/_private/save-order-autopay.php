<?php
try {
	if( !isset($db)){
		require_once 'db.php';
	}
	$paid = 1;
	// success
	$query = $db->prepare("INSERT INTO orders (order_id, email, amount, currency, language, flow, paid) VALUES (:order_id, :email, :amount, :currency, :language, :flow, :paid)");	
	$success = $query->execute(array('order_id' => $order_id, 'email' => $email,'amount' => $amount,'currency' => $currency,'language' => $ogone_language,'flow' => $flow, 'paid' => $paid));

	if($success) {
		$order_db_id = $db->lastInsertId();
		// Insert order items
		$query = $db->prepare("INSERT INTO order_items (order_id, product_id, quantity, price)
							VALUES (:order_id, :product_id, :quantity, :price)");
		
		foreach ($_SESSION ["pot_basket"] ["items"] as $product_id => $product ) {
			$quantity = $product["qty"];
			$price = $product["price"];	
			$query->execute(array(
					'order_id' => $order_db_id,'product_id' => $product_id, 'quantity' => $quantity, 'price' => $price));
		}
	} else {
		header('HTTP/1.1 500 Internal Server Error');
	}
} catch (Exception $e) {	
	header('HTTP/1.1 500 Internal Server Error');
}