<?php 

try {
	require_once 'db.php';
	$query = $db->prepare("INSERT INTO community (email, name) VALUES (:email, :name)");
	$success = $query->execute(array('email' => $community_email, 'name' => $community_name));
	
} catch (Exception $e) {
	header('HTTP/1.1 500 Internal Server Error');
}

?>