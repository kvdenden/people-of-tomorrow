<?php
// Database settings
$db_host = "mac21.be";
$db_port = "3306";
$db_name = "sculpture";
$db_user = "sculpture";
$db_password = "secret";

$db = new PDO("mysql:host={$db_host};port={$db_port};dbname={$db_name}", $db_user, $db_password);

// get URL vars
$amount = $_GET['amount'];
$cardholder = $_GET['cardholder'];
$product = $_GET['product'];
$test = $_GET['test'];

$price = ($product == "large")? 4000 : 1000;

generate($db, $amount, $product, $cardholder, $test, $price);

/**
 *
 *
 */
function generate($db, $amount_of_codes, $type, $cardholder, $test, $price){
	$i = 0;
	$code_arr = array();
	while($i < $amount_of_codes){
		$token = "";
		$order_id = generateOrderId($db);
		$order_db_id = saveOrder($db, $test, $order_id, $cardholder, $price);
		$token = generateCredit($db, $order_id, $order_db_id, $type, $token);
		array_push($code_arr, $token);
		$i++;
		//echo "<br>";
	}

	$j = 0;

	while($j < count($code_arr)){
		echo $code_arr[$j] . "<br>";
		$j++;
	}
}

/**
 *
 *
 */
function generateOrderId($db){
	// Generate order ID
	if (! isset ( $_SERVER ['HTTP_X_FORWARDED_FOR'] )) {
		$ip = $_SERVER ['REMOTE_ADDR'];
	} else {
		$ip = $_SERVER ['HTTP_X_FORWARDED_FOR'];
	}

	$i = 0;

	while ( $i < 100 ) {
		$random = rand ( 1, 99999 );
		$ip = str_pad ( substr ( preg_replace ( "/[^0-9]/", "", $ip ) * $random, 3, 6 ), 6, '0', STR_PAD_LEFT );
		$stamp = strtotime ( "now" );
		$order_id = "POT-$stamp-$ip";
		try {
			$query = $db->prepare ( "SELECT order_id
							FROM orders
							WHERE order_id = :order_id
							" );
			$query->execute ( array ('order_id' => $order_id ) );
			$result = $query->fetch ( PDO::FETCH_ASSOC );
			if (!$result) {
				// no result = success
				return $order_id;
				break;
			}
		} catch ( Exception $e ) {
			header ( 'HTTP/1.1 500 Internal Server Error' );
		}
		$i ++;
	}
}

/**
 *
 *
 */
function saveOrder($db, $test, $order_id, $cardholder, $price){
	// static
	$status = 99;
	$paid = 1;
	$amount = 1;
	$currency = "EUR";
	$language = "en_US";
	$flow = "g";
	$transaction_date = date('Y-m-d H:i:s');

	//echo "saveOrder: " . $test . " " . $order_id . " " . $cardholder . " " . $price . "<br>";

	if( ($test == false) || ($test == "false") ){
		try{
			$query = $db->prepare ( "INSERT INTO orders (status, paid, order_id, cardholder, transaction_date, amount, currency, language, flow) VALUES (:status, :paid, :order_id, :cardholder, :transaction_date, :amount, :currency, :language, :flow)" );
			$success = $query->execute ( array ('status' => $status, 'paid' => $paid, 'order_id' => $order_id, 'cardholder' => $cardholder, 'transaction_date' => $transaction_date, "amount" => $price, "currency" => $currency, "language" => $language, "flow" => $flow ) );
			if($success){
				return $db->lastInsertId();
			}
		} catch (Exception $e){
			echo $e;
		}
	}else{
		return "doesnotmatter";
	}
}

/**
 *
 *
 */
function generateCredit($db, $order_id, $order_db_id, $type, &$token) {
	// Generate credit ID
	$group_id = 0;

	$i = 0;
	
	while ( $i < 100 ) {
		$random = rand ( 1, 99999 );
		$token = substr ( sha1 ( $order_id . "-$random" ), 0, 10 );
		
		try {
			$query = $db->prepare ( "SELECT token FROM credits WHERE token = :token");
			$query->execute ( array ('token' => $token ) );
			$result = $query->fetch ( PDO::FETCH_ASSOC );
			
			if (!$result) {
				// no result = success
				// Create credit in database
				$query = $db->prepare ( "INSERT INTO credits (token, group_id, order_id, type)
							VALUES (:token, :group_id, :order_id, :type)" );
				$success = $query->execute ( array ('token' => $token, 'group_id' => $group_id, 'order_id' => $order_db_id, 'type' => $type ) );
				
				//echo "generateCredit: " . $order_id . " " . $order_db_id . " " . $type . " " . $group_id . " " . $token . "<br>";	
				break;
			}
		} catch ( Exception $e ) {
			// header ( 'HTTP/1.1 500 Internal Server Error' );
			echo $e;
		}
		$i++;
	}
	return $token;	
}
?>