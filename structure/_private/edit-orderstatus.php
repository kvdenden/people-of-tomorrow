<?php
try {
	require_once 'db.php';

	if ($pay_status == 5 or $pay_status == 51 or $pay_status == 9 or $pay_status == 91) {
		$paid = 1;
	} else {
		$paid = 0;
	}
	
	$query = $db->prepare ( "UPDATE orders SET status = :status, paid = :paid, pay_id = :pay_id, transaction_date = :transaction_date, cardholder = :cardholder WHERE order_id = :order_id LIMIT 1" );
	$success = $query->execute ( array ('status' => $pay_status, 'paid' => $paid, 'pay_id' => $pay_id, 'transaction_date' => $transaction_date,'order_id' => $order_id,'cardholder' => $cardholder ) );
			
	if(!$success) {
		header('HTTP/1.1 500 Internal Server Error');
	} 
} catch (Exception $e) {

	header('HTTP/1.1 500 Internal Server Error');
}

?>