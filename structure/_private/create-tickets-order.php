<?php

try {
	require_once 'db.php';

	if(isset($_POST['name']) && isset($_POST['email']) && isset($_POST['type']) && isset($_POST['width']) && isset($_POST['height']) && isset($_POST['filename'])) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$credit_type = $_POST['type'];
		$width = $_POST['width'];
		$height = $_POST['height'];
		$filename = $_POST['filename'];

   	$token = substr ( sha1 ( $filename ), 0, 10 );

		$order_id = "POT-" . $token;
		$flow = $_POST['flow'];
		if ($flow == '') {
			$flow = 'e';
		}
		$paid = 1;

		error_log("before order creation query");

		$query = $db->prepare("INSERT INTO orders (order_id, cardholder, email, flow, paid) VALUES (:order_id, :cardholder, :email, :flow, :paid)");	
		$success = $query->execute(array('order_id' => $order_id, 'cardholder' => $name, 'email' => $email,'flow' => $flow, 'paid' => $paid));

		error_log("after order creation query");

		if($success) {


			// create credit for this order
			$order_db_id = $db->lastInsertId();

			error_log("order created: " . $order_db_id);

			$i = 0;

			while ( $i < 100 ) {
			
				$random = rand ( 1, 999 );
				$token = mb_substr ( (string)sha1 ( $order_id . "-$random" ), 0, 10 );
			
				$query = $db->prepare ( "SELECT token
									FROM credits
									WHERE token = :token
									" );
				$query->execute ( array ('token' => $token ) );
				$result = $query->fetch ( PDO::FETCH_ASSOC );
		
				if (! $result) {
					// no result = success
						
					// Create credit in database

					error_log("generated new token: " . $token);
						
					$query = $db->prepare ( "INSERT INTO credits (order_id, token, type,group_id)
										VALUES (:order_id, :token, :type,:group_id)" );
					$success = $query->execute ( array ('order_id' => $order_db_id, 'token' => $token, 'type' => $credit_type,'group_id' => 0 ) );

					error_log("after credit creation query");

					if($success) {
						// create design for this credit
						$credit_id = $db->lastInsertId();
						$data = '';

						$query = $db->prepare("INSERT INTO designs
							(credit_id, data, width, height, filename) VALUES (:credit_id, :data, :width, :height, :filename)");
						$success = $query->execute(array('credit_id' => $credit_id, 'data' => $data, 'width' => $width, 'height' => $height, 'filename' => $filename));
						if($success) {
							echo json_encode(array('token' => $token));
						} else {
							echo json_encode(array('error' => 'CANT_CREATE_DESIGN'));		
						}

					} else {
						echo json_encode(array('error' => 'CANT_CREATE_CREDIT'));	
					}
					return;
				}
			}
			// we tried 100 times to generate a token...
			echo json_encode(array('error' => 'CANT_CREATE_TOKEN'));	

		} else {
			echo json_encode(array('error' => 'CANT_CREATE_ORDER'));	
		}
	}
	else {
		echo json_encode(array('error' => 'MISSING_PARAMS'));
	}
} catch (Exception $e) {
	error_log($e->getMessage());
	header('HTTP/1.1 500 Internal Server Error');
}

?>