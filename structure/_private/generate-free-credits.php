<?php

function generateCredit($db, $order_id,$order_db_id, &$token) {
		// Generate credit ID
		
		$i = 0;
		
		while ( $i < 100 ) {
			
			$random = rand ( 1, 99999 );
			$token = substr ( sha1 ( $order_id . "-$random" ), 0, 10 );
			
			try {
				$query = $db->prepare ( "SELECT token
								FROM credits
								WHERE token = :token
								" );
				$query->execute ( array ('token' => $token ) );
				$result = $query->fetch ( PDO::FETCH_ASSOC );
				
				if (! $result) {
					// no result = success
					
					// Create credit in database
					
					$query = $db->prepare ( "INSERT INTO neighbours (token)
									VALUES (:token)" );
					$success = $query->execute ( array ('token' => $token ) );
					
					$group_id = 0;
					$type = "normal";
					
					if ($success) {
						$query = $db->prepare ( "INSERT INTO credits (token,group_id,order_id,type)
									VALUES (:token,:group_id,:order_id,:type)" );
						$success = $query->execute ( array ('token' => $token, 'group_id' => $group_id, 'order_id' => $order_db_id, 'type' => $type ) );
					}
					
					break;
				}
			} catch ( Exception $e ) {
				// header ( 'HTTP/1.1 500 Internal Server Error' );
				echo $e;
			}
			$i ++;
		}
	
}

$db_host = "mac21.be";
$db_port = "3306";
$db_name = "sculpture";
$db_user = "sculpture";
$db_password = "secret";

$db = new PDO("mysql:host={$db_host};port={$db_port};dbname={$db_name}", $db_user, $db_password);

// Create free order


$status = 9;
$paid = 1;
$order_id = "POT-FREE";

$query = $db->prepare ( "INSERT INTO orders (status,paid,order_id)
					VALUES (:status,:paid,:order_id)" );
$success = $query->execute ( array ('status' => $status, 'paid' => $paid, 'order_id' => $order_id ) );

if ($success) {
	
	$order_db_id = $db->lastInsertId();
	
	$o = 0;
	$random = rand ( 1, 99999 );
	$ip = str_pad ( substr ( preg_replace ( "/[^0-9]/", "", $ip ) * $random, 3, 6 ), 6, '0', STR_PAD_LEFT );
	$stamp = strtotime ( "now" );
	$order_id = "POT-$stamp-$ip";
	
	while ( $o < 2000 ) {
		generateCredit ( $db, $order_id,$order_db_id, $token );
		echo "$token<br>";
		$o ++;
	}
	
}



?>