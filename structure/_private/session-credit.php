<?php
	session_start();
	if (isset($_SESSION['credits'])) {
		echo json_encode($_SESSION['credits']);
	} else {
		echo json_encode(array());
	}
?>