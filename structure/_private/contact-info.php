<?php 
try {
	require_once 'db.php';

	if(isset($_POST['token'])) {
		$token = $_POST['token'];
		$email = isset($_POST['email']) ? $_POST['email'] : '';
		$name = isset($_POST['name']) ? $_POST['name'] : '';

		$query = $db->prepare("SELECT orders.id, orders.status, orders.flow
			FROM credits, orders
			WHERE credits.order_id = orders.id
			AND credits.token = :token");
		$query->execute(array('token' => $token));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		if($result && ($result['status'] == 99 || $result['flow'] == 't' || $result['flow'] == 'v' || $result['flow'] == 'o' || $result['flow'] == 'e')) {
			$id = $result['id'];
			$query = $db->prepare ( "UPDATE orders SET cardholder = :name, email = :email WHERE id = :id" );
			$query->execute(array('name' => $name, 'email' => $email, 'id' => $id));
			echo json_encode(array(true));
		} else {
			echo json_encode(array(false));
		}
	}
	else {
		echo json_encode(array(false));
	}
} catch (Exception $e) {
	
	header('HTTP/1.1 500 Internal Server Error');
}

?>