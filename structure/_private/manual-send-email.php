<?php
	// SCRIPT TO SEND CONFIRMATION EMAIL MANUALLY

	/*
		URL PARAMS:
		- goandmultiply = 1 // else the script is not activated
		- showresults == 1// shows all credits generate, 0: shows less info 
		- lang = en // language for the emails. defaults to en if no param is given
		- limit = x // max amount of items to process.  defaults to 0 (all)
		- test = true/false if set to true no emails are sent. defaults to true
		- type = // confirmation (to manually send out confirmation emails) || reminder (1week after purchase) || lastreminder (2 weeks before submission deadline) REQUIRED because there is no default value.
		- scheduled //needed for the scheduled tasks, DEFAULTS to false if SET can only take "week" as parameters. and must be accompanied by type* 
			*only 1 possibility: IF scheduled = week -> type MUST be reminder
	*/


	/********************/
	/****** WARNING *****/
	/********************/
	// The script assumes fixed prices!
		// - normal = €10
		// - large = €40
		// - group = €35
	// Also assumes $base_path = "http://www.peopleoftomorrow.com"

	require_once realpath(dirname(__FILE__) . '/..') . '/helpers/dumpy.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/helpers/cleanurl.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/helpers/active_menu.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/_private/generate-credit.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/_private/Critsend.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/_private/db.php';

	$mail_from = "contact@peopleoftomorrow.com";
	$mail_from_name = "People of Tomorrow";
	$mail_replyTo = "contact@peopleoftomorrow.com";
	$mail_replyTo_name = "People of Tomorrow";
	$base_path = "http://www.peopleoftomorrow.com/";

	$showresults = false;
	if( isset($_GET['showresults']) ){
		$showresults = ($_GET['showresults']== 1)? true : false;
	}

	$activeCurrency = "EUR";
	$language = "en";
	if( isset($_GET['lang']) ){
		$language = $_GET['lang'];
	}

	$limit = 0;
	if( isset($_GET['limit']) ){
		$limit = intval($_GET['limit']);
	}

	if($limit == 0){
		echo "<p>Limit SET to " . $limit . " (All)</p>";
	}else{
		echo "<p>Limit SET to " . $limit . "</p>";
	}
	

	$testing = true;
	if( isset($_GET['test']) ){
		$testing = ($_GET['test'] == 1)? true : false;
	}
	echo "<p>testing is SET to". $testing ."</p>";

	$email_type ="";
	if( isset($_GET['type'])){
		if(($_GET['type'] == 'confirmation') || ($_GET['type'] == 'reminder') || ($_GET['type'] == "lastreminder") ){
			$email_type = $_GET['type'];
			echo "<p>Type is SET to " . $type . "</p>";
		}else{
			echo "<p>Type is NOT SET: nothing will happen</p>";
		}
	}

	$scheduled = false;
	if( isset($_GET['scheduled']) ){
		if($_GET['scheduled'] == "week"){
			if($email_type == "reminder"){
				$scheduled = true;
			}
		}
	}

	echo "<hr>";
	$content_xml =  simplexml_load_file( realpath(dirname(__FILE__) . '/../..') . '/content/' . $language . '/content.xml');
	$structure_xml = simplexml_load_file( realpath(dirname(__FILE__) . '/../..') . '/content/' . $language . '/structure.xml');

	if( $_GET["goandmultiply"] == true){
		// 1. check all the orders without credits
		// 2. generate credits for these orders
		// 3. get all orders with open credits
		// 4. create mail batch for all emails with open credits
		// 5. push mail batch 

		echo "<h2>1. Orders without any credits</h2>";

		try {
			$query_step1 = $db->prepare("SELECT id, order_id, email, amount FROM orders WHERE status IN (5, 9, 51, 91) AND paid = 1 AND id NOT IN (SELECT DISTINCT order_id FROM credits)");
			$success_step1 = $query_step1->execute();
			//$query = $db->prepare("INSERT INTO orders (order_id, email, amount, currency, language, flow) VALUES (:order_id, :email, :amount, :currency, :language, :flow)");
			
			//$success = $query->execute(array('order_id' => $order_id, 'email' => $email,'amount' => $amount,'currency' => $currency,'language' => $ogone_language,'flow' => $flow));
		
			if($success_step1){
				$result = $query_step1->fetchAll(PDO::FETCH_ASSOC);
				$cantgenerate_arr = array();
				echo "<p><b>" . count($result) . "</b> orders without credits</p>";
				if(  count($result) == 0 ){
					echo "<h2 style='text-decoration:line-through;'>2. Generate credits for these orders</h2>";
				}else{
					echo "<h2>2. Generate credits for these orders</h2>";
				}

				$order_counter  = 0;
				$failed_order_counter = 0;
				$normal_credit_counter = 0;
				$large_credit_counter = 0;
				$group_credit_counter = 0;

				$limitcounter = 0;

				foreach ($result as $order){
					if( ($limit == 0) || ($limitcounter < $limit) ){
						$query_step3 = $db->prepare("SELECT product_id, quantity, price FROM order_items WHERE order_id = :order_id");
						$query_step3->bindParam(':order_id', intval($order["id"]), PDO::PARAM_INT);
						$success_step3 = $query_step3->execute();

						if($success_step3){
							$order_counter++;

							$result_step3 = $query_step3->fetchAll(PDO::FETCH_ASSOC);
							$type_conversion = array("product_1"=>"normal", "product_2"=>"group", "product_3"=>"large");

							
							if( count($result_step3) > 0 ){
								if($showresults){
									echo "<p><b>Order: " . $order['order_id'] . "</b></p>";
									echo "<ul>";
								}

								foreach($result_step3 as $order_item){
									$type = $type_conversion[ $order_item["product_id"] ];

									for( $c = 0 ; $c < intval($order_item['quantity']) ; $c++ ){
										if($type === "group"){
											$group_id = $order["order_id"] . "-" . ($c+1);
											for($g = 0 ; $g < 4; $g++){
												$group_credit_counter++;
												if($showresults){
													echo "<li>";
														echo "generatecredit(db, " . $order["order_id"] . ", " . $order["id"] . ", " . $type . ", ". $group_id .")";
													echo "</li>";
												}

												if($testing == false){
													generateCredit($db, $order["order_id"], $order["id"] ,$type, $group_id, 0, $token);
												}
											}
										}else{
											$group_id = 0;

											if($type === 'normal'){
												$normal_credit_counter++;
											}else if($type === "large"){
												$large_credit_counter++;
											}

											if($showresults){
												echo "<li>";
													echo "generatecredit(db, " . $order["order_id"] . ", " . $order["id"] . ", " . $type . ")";
												echo "</li>";
											}
											if($testing == false){
												generateCredit($db, $order["order_id"], $order["id"] ,$type, $group_id, 0, $token);
											}
										}
									}
								}

								if($showresults){
									echo "</ul>";
								}

							}else{
								//no ordered_items found
								array_push($cantgenerate_arr, $order);
								$failed_order_counter++;
							}
						}
					}

					$limitcounter++;
				}
				$total_credit_counter = $normal_credit_counter + $large_credit_counter + $group_credit_counter;

				echo "<p><b>". $total_credit_counter . "</b> credits generated in <b>". $order_counter . "</b> orders and <b>" . $failed_order_counter . "</b> failed.</p>"; 
				echo "<ul>";
					echo "<li><b>". $normal_credit_counter ."</b> normal credits</li>";
					echo "<li><b>". $large_credit_counter ."</b> large credits</li>";
					echo "<li><b>". $group_credit_counter ."</b> group credits (4 in each group = ".($group_credit_counter/4)." group orders)</li>";
				echo "</ul>";  
				// display all credits that cannot be generated due to non exclusive amount
				
				if( count($cantgenerate_arr) > 0 ){
					echo "<p>There are <b>" . count($cantgenerate_arr) . "</b> orders where we couldn't generate credits for automatically.<p>";
					echo "<table>";
						echo "<tr>";
							echo "<th>Order ID</th>";
							echo "<th>Email</th>";
							echo "<th>amount</th>";
						echo "</tr>";
						foreach($cantgenerate_arr as $order){
							echo "<tr>";
								echo "<td>";
									echo $order["order_id"];
								echo "</td>";
								echo "<td>";
									echo $order["email"];
								echo "</td>";
								echo "<td>";
									echo $order["amount"];
								echo "</td>";
							echo "</tr>";
						}
					echo "</table>";
				}


				//explore($result);
			} else {
				echo "<p style='background:#F00; padding: 10px; color: #F00;'>ERROR: FAILED TO RETRIEVE ORDERS WITHOUT CREDITS</p>";
			}
		} catch (Exception $e) {
			echo "<p style='background:#F00; padding: 10px; color: #F00;'>ERROR: FAILED TO GENERATE CREDITS FOR EMPTY ORDERS</p>";
			explore($e);
		}

		try{
			echo "<h2>3. Get all orders with open credits</h2>";
			if($scheduled == true){
				$query_step3 = $db->prepare("SELECT email, id, order_id, cardholder, amount FROM orders 
										WHERE paid = 1 
											AND status IN (5,9,51,91) 
											AND amount >= 1000
											AND order_id != 'POT-FREE'
											AND transaction_date = DATE(DATE_SUB(NOW(), INTERVAL 1 WEEK))
											AND id IN (SELECT DISTINCT order_id FROM credits WHERE id NOT IN (
													SELECT credit_id from designs WHERE credit_id IS NOT NULL AND data IS NOT NULL ))");
			}else{
				$query_step3 = $db->prepare("SELECT email, id, order_id, cardholder, amount FROM orders 
										WHERE paid = 1 
											AND status IN (5,9,51,91) 
											AND amount >= 1000
											AND order_id != 'POT-FREE'
											AND id IN (SELECT DISTINCT order_id FROM credits WHERE id NOT IN (
													SELECT credit_id from designs WHERE credit_id IS NOT NULL AND data IS NOT NULL ))");
			}
			

			$success_step3 = $query_step3->execute();

			if($success_step3){
				$result_step3 = $query_step3->fetchAll(PDO::FETCH_ASSOC);
				$mailcounter = 0;
				//explore($result_step3);
				echo "<p><b>" . count($result_step3) . "</b> orders with open credits</p>";
				echo "<h2>4. Prepare confirmation email for all these orders</h3>";

				// START EMAIL PREP
				$critsend = new MxmConnect();
				$emails = array();

				if($email_type=="confirmation"){
					$email_body = file_get_contents($base_path . "mails/" . $language . "/orderconfirmation.html");
					$content = array('subject'=> 'Order Confirmation | People of Tomorrow', 'html'=> $email_body, 'text'=>$email_body);
				}else if($email_type=="reminder"){
					$email_body = file_get_contents($base_path . "mails/" . $language . "/reminder.html");
					$content = array('subject'=> 'Reminder | People of Tomorrow', 'html'=> $email_body, 'text'=>$email_body);
				}else if($email_type=="lastreminder"){
					$email_body = file_get_contents($base_path . "mails/" . $language . "/last-reminder.html");
					$content = array('subject'=> 'Reminder | People of Tomorrow', 'html'=> $email_body, 'text'=>$email_body);
				}
				
				$param = array(	'tag'=>array('payment_confirmation'), 
								'mailfrom'=> $mail_from, 
								'mailfrom_friendly'=> $mail_from_name, 
								'replyto'=>$mail_replyTo, 
								'replyto_filtered'=> 'true');

				$limitcounter = 0;
				foreach($result_step3 as $order){
					if( ($limit == 0) || ($limitcounter < $limit) ){
							if($email_type=="confirmation"){
								$query_step4 = $db->prepare("SELECT token, type, group_id FROM credits WHERE order_id = :order_id");
							}else if( ($email_type=="reminder") || ($email_type=="lastreminder") ){
								$query_step4 = $db->prepare("SELECT credits.id, credits.token, credits.type, credits.group_id, IFNULL(designs.data, '') as data FROM credits LEFT JOIN designs ON designs.credit_id = credits.id WHERE credits.order_id = :order_id");
							}

							$query_step4->bindParam(':order_id', $order["id"], PDO::PARAM_INT);
							$success_step4 = $query_step4->execute();


						if($success_step4){
							$result_step4 = $query_step4->fetchAll(PDO::FETCH_ASSOC);
							// prepare confirmation email object
							$editor_link = getMenuLinkByID(51, $structure_xml);
							if (count($result_step4) == 1) {
								if ($result_step4[0]['type'] == "normal") {
									$editor_type = "small";
								} else {
									$editor_type = $type;
								}
								$editor_link = getMenuLinkByID(51, $structure_xml) . "#/$editor_type";
							}

							$link_before = "<a style='color: #30c0fc; font-weight: bold;' href='" . $base_path . $language . "/" . $editor_link . "'>" . $content_xml->buy->email_flow1->top->link_editor . "</a>";
							$link_after = "<a style='color: #30c0fc; font-weight: bold;' href='" . $base_path . $language . "/" . $editor_link . "'>" . $content_xml->buy->email_flow1->bottom->start_link . "</a>";

							if($email_type=="confirmation"){
								$credits_html = htmlCreditDetail($result_step4, $content_xml, $activeCurrency, $language);
							}else if($email_type=="reminder"){
								$credits_html = htmlCreditDetailReminder($result_step4, $content_xml, $activeCurrency, $language);
							}else if($email_type=="lastreminder"){
								$credits_html = htmlCreditDetailReminder($result_step4, $content_xml, $activeCurrency, $language);
							}

							$emails[$mailcounter] = array(
								'email' => $order['email'],
								'field1'=> $order['cardholder'],
								'field2'=> $credits_html,
								'field3'=> round($order['amount']/100) . " " . $activeCurrency,
								'field4'=> $link_before,
								'field5'=> $link_after
							);
							$mailcounter++;
						}else{
							// FAILED TO CREATE EMAIL
						}
					}
					$limitcounter++;
				}

					//explore($emails);

				if((count($emails) > 0 )&& ($testing == false) ){
					$critsend->sendCampaign($content, $param, $emails);
				}

				echo "<p><b>". $mailcounter . "</b> email(s) sent</p>";
			}else{
				echo "<p style='background:#F00; padding: 10px; color: #F00;'>ERROR: FAILED TO RETRIEVE ORDERS WITH OPEN CREDITS</p>";
			}
		}
		catch (Exception $e){
			echo "<p style='background:#F00; padding: 10px; color: #F00;'>ERROR: UNABLE TO SEND EMAILS TO ORDERS WITH OPEN CREDITS</p>";
			explore ($e);
		}

	}else{
		echo "<h1>Nothing to see here, carry on</h1>";
	}

	function htmlCreditDetail($credits, $content_xml, $activeCurrency, $language){
		$html_str = "";
		$normal_arr = array();
		$large_arr = array();
		$group_arr = array();

		foreach($credits as $credit){
			if($credit["type"] === 'normal'){
				array_push($normal_arr, $credit);
			}else if($credit['type'] === 'large'){
				array_push($large_arr, $credit);
			}else if($credit['type'] === 'group'){
				array_push($group_arr, $credit);
			}
		}

		$counter = 0;
		$price = 0;
		if(count($normal_arr) > 0){
			$counter = 0; 
			$price = 10;
			foreach($normal_arr as $credit){
				$counter ++;
				$html_str .="<tr>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>". $content_xml->buy->confirmation->product->product_1 ."</p></td>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>$price $activeCurrency</td>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>". $credit['token'] ."</td>
							</tr>";
			}
		}

		if(count($large_arr) > 0){
			$price = 40;
			foreach($large_arr as $credit){
				$counter ++;
				$html_str .="<tr>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>". $content_xml->buy->confirmation->product->product_3 ."</p></td>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>$price $activeCurrency</td>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>". $credit['token'] ."</td>
							</tr>";
			}
		}

		if(count($group_arr) > 0){
			$price = 35;
			$curr_group_id = "666";

			foreach($group_arr as $credit){
				if($credit['group_id'] != $curr_group_id){
					$html_str .= "<tr style='border-top: 1px solid #AAA;'>";
				}else{
					$html_str .= "<tr>";
				}

				$html_str.="<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>";
					if($credit['group_id'] != $curr_group_id){
						$html_str .= $content_xml->buy->confirmation->product->product_2;
					}else{
						$html_str .= "&nbsp;";
					}
				$html_str .= "</td>";
				
				$html_str.="<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>";
					if($credit['group_id'] != $curr_group_id){
						$html_str.= $price . " " .$activeCurrency;
					}else{
						$html_str.= "&nbsp;";
					}
				$html_str.= "</td>";

				$html_str .= "<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>".$credit['token']."</td>";
				$html_str .= "</tr>";
				$curr_group_id = $credit["group_id"];
			}
		}

		$html_str .= "<!-- for empty space -->
					<tr>
						<td width='200' valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>&nbsp;</p></td>
						<td width='100' valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>&nbsp;</td>
						<td width='100' valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>&nbsp;</td>
					</tr>
					<!-- /for empty space -->";

		return $html_str;
	}


	function htmlCreditDetailReminder ($credits, $content_xml, $activeCurrency, $language){
		$html_str = "";
		$normal_arr = array();
		$large_arr = array();
		$group_arr = array();

		foreach($credits as $credit){
			if($credit["type"] === 'normal'){
				array_push($normal_arr, $credit);
			}else if($credit['type'] === 'large'){
				array_push($large_arr, $credit);
			}else if($credit['type'] === 'group'){
				array_push($group_arr, $credit);
			}
		}

		$counter = 0;
		$price = 0;
		if(count($normal_arr) > 0){
			$counter = 0; 
			$price = 10;
			foreach($normal_arr as $credit){
				$counter ++;
				if($credit['data'] == ""){
				$html_str .="<tr>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>". $content_xml->buy->confirmation->product->product_1 ."</p></td>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>". $credit['token'] ."</td>
							</tr>";
				}else{
					$html_str .="<tr>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'><strike>". $content_xml->buy->confirmation->product->product_1 ."</strike></p></td>
							<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><strike>". $credit['token'] ."</strike></td>
							</tr>";
				}
			}
		}

		if(count($large_arr) > 0){
			$price = 40;
			foreach($large_arr as $credit){
				$counter ++;
				if($credit['data'] == ""){
					$html_str .="<tr>
								<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>". $content_xml->buy->confirmation->product->product_3 ."</p></td>
								<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>". $credit['token'] ."</td>
								</tr>";
				}else{
					$html_str .="<tr>
								<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'><strike>". $content_xml->buy->confirmation->product->product_3 ."</strike></p></td>
								<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><strike>". $credit['token'] ."</strike></td>
								</tr>";
				}
			}
		}

		if(count($group_arr) > 0){
			$price = 35;
			$curr_group_id = "666";

			foreach($group_arr as $credit){
				if($credit['group_id'] != $curr_group_id){
					$html_str .= "<tr style='border-top: 1px solid #AAA;'>";
				}else{
					$html_str .= "<tr>";
				}

				$html_str.="<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>";
					if($credit['group_id'] != $curr_group_id){
						$html_str .= $content_xml->buy->confirmation->product->product_2;
					}else{
						$html_str .= "&nbsp;";
					}
				$html_str .= "</td>";
				
				if($credit['data'] == ""){
					$html_str .= "<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>".$credit['token']."</td>";
				}else{
					$html_str .= "<td valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><strike>".$credit['token']."</strike></td>";
				}

				$html_str .= "</tr>";
				$curr_group_id = $credit["group_id"];
			}
		}

		$html_str .= "<!-- for empty space -->
					<tr>
						<td width='200' valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'><p style='margin: 0; margin-top: 0;'>&nbsp;</p></td>
						<td width='100' valign='top' style='font-family: helvetica; color: #676767; font-size: 12px; line-height: 18px;'>&nbsp;</td>
					</tr>
					<!-- /for empty space -->";

		return $html_str;
	}
?>
