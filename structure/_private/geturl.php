<?php
	require_once '../helpers/cleanurl.php';

	$id = $_POST['id'];
	$language = $_POST['language'];

	$structure_xml_path = "../../content/" . $language . "/structure.xml"; 
	$structure_xml =  simplexml_load_file( $structure_xml_path );

	$return_path = "/". $language . "/" . getMenuLinkByID($id, $structure_xml);

	echo json_encode($return_path);
?>