<?php
// error_reporting(E_ALL); 
// ini_set( 'display_errors','1');

session_start();
	if(!isset($_POST['data']) || !isset($_POST['filename']) || !isset($_POST['width']) || !isset($_POST['height'])) {
		echo json_encode(array()); // return {}
	} else {
		$credit_id = isset($_POST['credit_id']) ? $_POST['credit_id'] : null;
		$data = isset($_POST['data']) ? $_POST['data'] : null;
		$filename = $_POST['filename'];
		$width = $_POST['width'];
		$height = $_POST['height'];
		$type = $_POST['type'];
		$token = isset($_POST['token']) ? $_POST['token'] : "";

		$custom = isset($_POST['custom']) ? $_POST['custom'] : false;

		$design_type = $type;
		if($design_type == "normal") {
			if($width == 693 && $height == 693) {
				$design_type = "small square";
			} else if($width == 1417 && $height == 331) {
				$design_type = "small rectangle";
			}
		}
		if($custom ==="true") {
			$design_type .= " custom";
		}

		try {
			include 'db.php';
			$result = false;
			if($credit_id) {
				$query = $db->prepare("SELECT credits.id
				FROM credits, orders
				WHERE credits.id = :credit_id
				AND credits.type = :type
				AND credits.order_id = orders.id
				AND orders.paid = 1
				AND credits.id NOT IN( SELECT credit_id FROM designs where credit_id is not null )");
				$query->execute(array('credit_id' => $credit_id, 'type' => $type));
				$result = $query->fetch(PDO::FETCH_ASSOC);
			} else {
				$result = true;
			}
			if($result) {
				// success
				$query = $db->prepare("INSERT INTO designs
					(credit_id, data, width, height, filename) VALUES (:credit_id, :data, :width, :height, :filename)");
				$success = $query->execute(array('credit_id' => $credit_id, 'data' => $data, 'width' => $width, 'height' => $height, 'filename' => $filename));
				if($success) {
					$design_id = $db->lastInsertId();

					
					$q = $db->prepare("select orders.cardholder, orders.email from orders, credits where orders.id = credits.order_id and credits.id = :credit_id");
					$q->execute(array('credit_id' => $credit_id));
					$details = $q->fetch(PDO::FETCH_ASSOC);

					$name = $details['cardholder'];
					$email = $details['email'];

					if(empty($email)) {
						$q = $db->prepare("select firstname, lastname, email from neighbours where token = :token");
						$q->execute(array('token' => $token));
						$details = $q->fetch(PDO::FETCH_ASSOC);
						if($details) {
							$name = $details['firstname'] . ' ' . $details['lastname'];
							$email = $details['email'];
						}
					}
					// echo(json_encode($details));
					
					// Delete old session
					unset ( $_SESSION ["pot_basket"] );
					unset ( $_SESSION ["pot_basket_created"] );
					
					// Store session
					require_once realpath(dirname(__FILE__) . '/..') . "/helpers/shop.php";
					
					// Product type always 'normal'?
					if ($type == "normal") {
						$key = "product_1";
					} elseif ($type == "group") {
						$key = "product_2";
					} elseif ($type == "large") {
						$key = "product_3";
					}
					
					$_SESSION["pot_basket"]["design"]["id"] = $design_id;
					$_SESSION["pot_basket"]["design"]["type"] = $type;
					$_SESSION["pot_basket"]["design"]["file"] = $filename;
					$_SESSION["pot_basket"]["items"][$key]["qty"] = 1;
					$_SESSION["pot_basket"]["items"][$key]["price"] = $products[$key]["price"][$activeCurrency];
					$_SESSION["pot_basket"]["design"]["design_type"] = $design_type;
					
					echo json_encode(array('result' => $design_id, 'type' => $type, 'name' => $name, 'email' => $email));
					
				} else {
					echo json_encode(array()); // return {}
				}
				// header('HTTP/1.1 200 OK');
			} else {
				// invalid token
				echo json_encode(array()); // return {}
			}
		} catch (Exception $e) {
			echo json_encode(array($e->getMessage())); // return {}
			header('HTTP/1.1 500 Internal Server Error');
			// echo json_encode(array()); // return {}
		}
	}

?>