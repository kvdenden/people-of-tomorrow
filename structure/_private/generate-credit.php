<?php
function retrieveOrderDBid ($db,$order_id) {
	
	try {
		$query = $db->prepare ( "SELECT id
							FROM orders
							WHERE order_id = :order_id
							" );
		$query->execute ( array ('order_id' => $order_id ) );
		$result = $query->fetch ( PDO::FETCH_ASSOC );
		if ($result) {
			$order_db_id = $result ['id'];
		}
	
	} catch ( Exception $e ) {
		header ( 'HTTP/1.1 500 Internal Server Error' );
	
	}
	return $order_db_id;
}

function generateCredit($db,$order_id,$order_db_id,$type,$group_id,$design_id,&$token) {
	
	// Generate credit ID
	
	$i = 0;
	
	while ( $i < 100 ) {
	
		$random = rand ( 1, 999 );
		$token = mb_substr ( (string)sha1 ( $order_id . "-$random" ), 0, 10 );
	
		try {
			$query = $db->prepare ( "SELECT token
								FROM credits
								WHERE token = :token
								" );
			$query->execute ( array ('token' => $token ) );
			$result = $query->fetch ( PDO::FETCH_ASSOC );
	
			if (! $result) {
				// no result = success
					
				// Create credit in database
					
				$query = $db->prepare ( "INSERT INTO credits (order_id, token, type,group_id)
									VALUES (:order_id, :token, :type,:group_id)" );
				$success = $query->execute ( array ('order_id' => $order_db_id, 'token' => $token, 'type' => $type,'group_id' => $group_id ) );
					
				if (! $success) {
					header ( 'HTTP/1.1 500 Internal Server Error' );
				} else {
					// Is there a design? Lock credit by updating design with the credit id
					if ($design_id > 0 && isset($design_id)) {
							
						$credit_id = $db->lastInsertId();
						$query = $db->prepare ( "UPDATE designs SET credit_id = :credit_id WHERE id = :design_id LIMIT 1" );
						$success = $query->execute ( array ('credit_id' => $credit_id, 'design_id' => $design_id) );
	
						if (! $success) {
							header ( 'HTTP/1.1 500 Internal Server Error' );
						}
					}
	
				}
				//return $token;
				break;
			}
		} catch ( Exception $e ) {
			//header ( 'HTTP/1.1 500 Internal Server Error' );
			echo $e;
		}
		$i ++;
	}
	return $token;
}

?>