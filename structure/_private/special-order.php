<?php 
try {
	require_once 'db.php';

	if(!isset($_GET['token'])) {
	// missing parameters
		echo json_encode(array('special' => false));
	}
	else {
		$token = $_GET['token'];

		$query = $db->prepare("SELECT orders.status, orders.flow
			FROM credits, orders
			WHERE credits.order_id = orders.id
			AND credits.token = :token");
		$query->execute(array('token' => $token));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		// echo json_encode($result);
		if($result) {
			echo json_encode(array('special' => ($result['status'] == 99 || $result['flow'] == 't' || $result['flow'] == 'v' || $result['flow'] == 'o')));
		} else {
			echo json_encode(array('special' => false));
		}
	}
} catch (Exception $e) {
	
	header('HTTP/1.1 500 Internal Server Error');
}

?>