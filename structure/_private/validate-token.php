<?php

// return {id: credit_id} if token is valid
// return {error: reason} if token is invalid

	if(!isset($_POST['type']) || !isset($_POST['token'])) {
		// missing parameters
		echo json_encode(array("error" => "MISSING_PARAMETERS"));
	} else {
		$type = $_POST['type'];
		$token = $_POST['token'];

		try {
			include 'db.php';

			$query = $db->prepare("SELECT id
				FROM credits
				WHERE credits.token = :token");
			$query->execute(array('token' => $token));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			if($result) {
				$query = $db->prepare("SELECT id
					FROM credits
					WHERE credits.token = :token
					AND credits.type = :type");
				$query->execute(array('type' => $type, 'token' => $token));
				$result = $query->fetch(PDO::FETCH_ASSOC);

				if(!$result && $type == 'normal') {
					// check if credit type is group
					$query = $db->prepare("SELECT id
						FROM credits
						WHERE credits.token = :token
						AND credits.type = 'group'");
					$query->execute(array('token' => $token));
					$result = $query->fetch(PDO::FETCH_ASSOC);
				}

				if($result) {
					$credit_id = $result['id'];

					$query = $db->prepare("SELECT credits.id, credits.type
						FROM credits, orders
						WHERE credits.token = :token
						AND credits.order_id = orders.id
						AND orders.paid = 1");
					$query->execute(array('token' => $token));
					$result = $query->fetch(PDO::FETCH_ASSOC);
					if($result) {
						// TODO: check if design already exists
						$query = $db->prepare("select credits.id, credits.type from credits where credits.id = :credit_id and credits.id not in (select credit_id from designs where credit_id is not null)");
						$query->execute(array('credit_id' => $credit_id));
						$result = $query->fetch(PDO::FETCH_ASSOC);
						if($result) {
							echo json_encode($result);
						} else {
							echo json_encode(array("error" => "CREDIT_ALREADY_USED"));
						}
					} else {
						echo json_encode(array("error" => "ORDER_NOT_PAID"));
					}
				} else {
					echo json_encode(array("error" => "INCORRECT_TYPE"));
				}
			} else {
				echo json_encode(array("error" => "TOKEN_DOES_NOT_EXIST"));
			}

		} catch (Exception $e) {
			// echo json_encode(array($e->getMessage())); // return {}
			header('HTTP/1.1 500 Internal Server Error');
		}
	}

?>