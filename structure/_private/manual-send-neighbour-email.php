<?php
	// SCRIPT TO SEND CONFIRMATION EMAIL MANUALLY

	/*
		URL PARAMS:
		- goandmultiply = 1 // else the script is not activated
		- showresults == 1// shows all credits generate, 0: shows less info 
		- lang = en // language for the emails. defaults to en if no param is given
		- limit = x // max amount of items to process.  defaults to 0 (all)
		- test = true/false if set to true no emails are sent. defaults to true
	*/


	/********************/
	/****** WARNING *****/
	/********************/
	// The script assumes fixed prices!
		// - normal = €10
		// - large = €40
		// - group = €35
	// Also assumes $base_path = "http://www.peopleoftomorrow.com"

	require_once realpath(dirname(__FILE__) . '/..') . '/helpers/dumpy.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/helpers/cleanurl.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/helpers/active_menu.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/_private/generate-credit.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/_private/Critsend.php';
	require_once realpath(dirname(__FILE__) . '/..') . '/_private/db.php';

	$mail_from = "contact@peopleoftomorrow.com";
	$mail_from_name = "People of Tomorrow";
	$mail_replyTo = "contact@peopleoftomorrow.com";
	$mail_replyTo_name = "People of Tomorrow";
	$base_path = "http://www.peopleoftomorrow.com/";

	$showresults = false;
	if( isset($_GET['showresults']) ){
		$showresults = ($_GET['showresults']== 1)? true : false;
	}

	$activeCurrency = "EUR";
	$language = "en";
	if( isset($_GET['lang']) ){
		$language = $_GET['lang'];
	}

	$limit = 0;
	if( isset($_GET['limit']) ){
		$limit = intval($_GET['limit']);
	}

	if($limit == 0){
		echo "<p>Limit SET to " . $limit . " (All)</p>";
	}else{
		echo "<p>Limit SET to " . $limit . "</p>";
	}
	

	$testing = true;
	if( isset($_GET['test']) ){
		$testing = ($_GET['test'] == 0)? false : true;
	}
	echo "<p>testing is SET to". $testing ."</p>";
	echo "<hr>";

	$content_xml =  simplexml_load_file( realpath(dirname(__FILE__) . '/../..') . '/neighbours/content/' . $language . '/content.xml');
	$structure_xml = simplexml_load_file( realpath(dirname(__FILE__) . '/../..') . '/neighbours/content/' . $language . '/structure.xml');

	if( $_GET["goandmultiply"] == true){
		echo "<h2>1. Neighbour codes without submitted design</h2>";

		try {
			$query_step1 = $db->prepare("SELECT neighbours.email, neighbours.firstname, neighbours.lastname, neighbours.token FROM neighbours LEFT JOIN credits ON credits.token = neighbours.token LEFT JOIN designs ON credits.id = designs.credit_id WHERE neighbours.email != '' AND designs.data = ''");
			$success_step1 = $query_step1->execute();
			$mailcounter = 0;
			if($success_step1){
				$result = $query_step1->fetchAll(PDO::FETCH_ASSOC);
				echo "<p><b>" . count($result) . "</b> neighbours that did not submit a design</p>";				
				echo "<h2>2. Prepare emails for neighbours</h2>";

				$order_counter  = 0;
				$failed_order_counter = 0;
				$normal_credit_counter = 0;
				$large_credit_counter = 0;
				$group_credit_counter = 0;

				$limitcounter = 0;
				$critsend = new MxmConnect();

				$email_body = file_get_contents($base_path . "neighbours/mails/" . $language . "/neighbours.html");
				$content = array('subject'=> 'Reminder | People of Tomorrow', 'html'=> $email_body, 'text'=>$email_body);
				
				$param = array(	'tag'=>array('payment_confirmation'), 
						'mailfrom'=> $mail_from, 
						'mailfrom_friendly'=> $mail_from_name, 
						'replyto'=>$mail_replyTo, 
						'replyto_filtered'=> 'true');

				echo "<table>";
					echo "<tr>";
							echo "<th width='30'></th>";
							echo "<th width='220' >Name</th>";
							echo "<th width='250' >Email</th>";
							echo "<th width='150' >Token</th>";
  						echo "</tr>";
				foreach ($result as $nb){
					if( ($limit == 0) || ($limitcounter < $limit) ){
						$emails[$limitcounter] = array(
							'email' => $nb['email'],
							'field1'=> $nb['firstname'] . " " . $nb['lastname'] ,
							'field2'=> $nb['token']
						);

						$limitcounter++;
						
						echo "<tr>";
							echo "<td>" . $limitcounter . ". </td>";
							echo "<td>" . $nb['firstname'] . " " . $nb['lastname'] . "</td>";
							echo "<td>" . $nb['email'] . "</td>";
							echo "<td>" . $nb['token'] . "</td>";
  						echo "</tr>";
					}
				}
				echo "</table>";


				if((count($emails) > 0 )&& ($testing == false) ){
					$critsend->sendCampaign($content, $param, $emails);
				}
				echo "<p><b>". $limitcounter ."</b> email(s) sent.</p>";
			}
		}
		catch (Exception $e){
			echo "<p style='background:#F00; padding: 10px; color: #F00;'>ERROR: UNABLE TO SEND EMAILS</p>";
			explore ($e);
		}
	}				
?>