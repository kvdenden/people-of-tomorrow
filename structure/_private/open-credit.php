<?php
	session_start();
	if (isset($_SESSION['pot_basket']['flow']) && $_SESSION['pot_basket']['flow'] == 'o') {
		$token = $_SESSION ["pot_basket"]["open"]["token"];
		unset($_SESSION['pot_basket']['flow']);
		unset($_SESSION ["pot_basket"]["open"]["token"]);
		echo json_encode(array('token' => $token ));
	} else {
		echo json_encode(array());
	}
?>