<?php

require_once realpath(dirname(__FILE__)) . '/Critsend.php';

try {

	// $language = 'en';
	// $template = 'designed.html';
	// $title = "Hello world";
	// $tag = "designed";
	// $vars = array("email" => "ken@enden.be", "field1" => "Ken Van den Enden", "field2" => "<img src='/preview/01f700e7c6541ce9e59184cfdad1247a.png' />");

	$language = $_POST['language'];
	$template = $_POST['template'];
	$title = $_POST['title'];
	$tag = $_POST['tag'];

	$vars = array("email" => $_POST['email'], "field1" => $_POST['name'], "field2" => $_POST['image']);

	$global_xml = simplexml_load_file("../../content/global.xml");

	$base_path = '/';
	if( ($_SERVER['SERVER_ADDR'] == "127.0.0.1") || ($_SERVER['SERVER_ADDR'] == "::1") ){
		$base_path = $global_xml->serverconfig->base_path_local['value'][0];
	}else if( ($_SERVER['SERVER_ADDR'] == "87.238.163.181") || ($_SERVER['SERVER_ADDR'] == "87.238.163.18")){
		$base_path = $global_xml->serverconfig->base_path_staging['value'][0];
	}else{
		$base_path =$global_xml->serverconfig->base_path_live['value'][0];		
	}

	$mail_from = $global_xml->emailconfig->mail_from;
	$mail_from_name = $global_xml->emailconfig->mail_from_name;
	$mail_replyTo = $global_xml->emailconfig->mail_replyto;
	$mail_replyTo_name = $global_xml->emailconfig->mail_replyto_name;


	$tmp_base_path = ($base_path === 'http://pot.local/') ? 'http://pot.mac21.be' : $base_path; 
	$email_body = file_get_contents($tmp_base_path . "/mails/" . $language . '/' . $template);
	$content = array('subject'=> $title, 'html'=> $email_body, 'text'=>$email_body);

	$param = array(	'tag'=>array($tag), 
									'mailfrom'=> $mail_from,
									'mailfrom_friendly'=> $mail_from_name, 
									'replyto'=>$mail_replyTo, 
									'replyto_filtered'=> 'true');

	$emails[0] = $vars;

	$success = false;

	$critsend = new MxmConnect();
	$success = $critsend->sendCampaign($content, $param, $emails);
	echo json_encode(array('success' => $success, 'name' => $vars['field1'], 'email' => $vars['email']));


} catch (Exception $e) {}

?>