<?php if(($activmenuId != 32) && ($header_type != "tfl")): ?>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-5 col-md-4 collaboration">
					<h3><?php echo $content_xml->footer->title1; ?></h3>
					<a href="http://www.tomorrowland.be" target="_blank" class="tml">
						<img src="/images/footer_tml.png" />
					</a>
					<a href="https://www.facebook.com/arnequinze" target="_blank" class="arne">
						<img src="/images/footer_arne.png" />
					</a>
					<div class="grouped">
						<a href="http://www.provant.be/" target="_blank" class="provant">
							<img src="/images/footer_provant_new_white.png" />
						</a>
						<a href="http://www.provant.be/vrije_tijd/domeinen/de_schorre/" target="_blank" class="schorre">
							<img src="/images/footer_schorre.png" />
						</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-md-4 social-footer">
					<h3><?php echo $content_xml->footer->title2; ?></h3>
					<div><a href="https://www.facebook.com/peopleoftomorrowofficial" target="_blank" class="facebook"><img src='/images/footer_social_facebook.png'> Facebook</a></div>
					<div><a href="https://www.twitter.com/POTomorrow" target="_blank" class='twitter'><img src='/images/footer_social_twitter.png'> Twitter</a></div>
					<div><a href="http://instagram.com/peopleoftomorrowofficial" target="_blank" class='instagram'><img src='/images/footer_social_instagram.png'> Instagram</a></div>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-4 contact">
					<h3><?php echo $content_xml->footer->title3; ?></h3>
					<a href="mailto:contact@peopleoftomorrow.com"><div class="glyphicon glyphicon-envelope"></div> <span><?php echo $content_xml->footer->helpdesk; ?></span></a>
					<a href="mailto:press@peopleoftomorrow.com"><div class="glyphicon glyphicon-envelope"></div> <span><?php echo $content_xml->footer->press; ?></span></a>

					<div class="ogone_footer">
						<p><?php echo $content_xml->footer->payment; ?></p>
						<img src="/content/_global/images/icon_ogone.png" width="80"/>
					</div>
				</div>
			</div>
		</div>
	</footer>
<?php elseif (($header_type == "tfl")): ?>
	<footer class="tfl">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<img src="images/tfl/footer-top.png">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<img src="images/tfl/tfl-content_04.gif">
				</div>
			</div>
			<div class="row last">
				<div class="col-xs-12">
					<div class="tfl-terms-wrapper">
						<a href="http://www.tomorrowland.com/tomorrowlandforlife/tfl-terms.html" target='_parent' class='first'>Terms and Conditions</a>
						<div class="seperator">|</div>
						<a href="http://www.tomorrowland.com/tomorrowlandforlife/tfl-bridge.html" target='_parent' class='last'>About the bridge</a>
					</div>
					<a href="https://www.facebook.com/tomorrowland" target="_blank">
						<img src="images/tfl/tfl-content_06.gif">
					</a>
					<a href="https://twitter.com/Tomorrowland" target="_blank">
						<img src="images/tfl/tfl-content_07.gif">
					</a>
					<a href="http://instagram.com/tomorrowland#" target="_blank">
						<img src="images/tfl/tfl-content_08.gif">
					</a>
					<img src="images/tfl/tfl-content_09.gif">
				</div>
			</div>
		</div>
	</footer>
<?php endif; ?>

<!-- Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-45895636-1', 'peopleoftomorrow.com');
		ga('send', 'pageview');
		ga('require', 'ecommerce', 'ecommerce.js');   // Load the ecommerce plug-in.
	</script>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="/bower_components/angular/angular.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/libraries/jquery.videoBG.js"></script>
<script type="text/javascript" src="/js/libraries/holder.js"></script>
<script type="text/javascript" src="/js/libraries/jquery.validate.min.js"></script>
<script type="text/javascript" src="/js/libraries/jquery.anythingslider.js"></script>
<script type="text/javascript" src="/js/libraries/froogaloop2.min.js"></script>
<script type="text/javascript" src="/js/vendor/underscore-min.js"></script>
<script type="text/javascript" src="/js/vendor/jquery.form.js"></script>
<script type="text/javascript" src="/js/vendor/jqBootstrapValidation-1.3.7.min.js"></script>

<script type="text/javascript" src="/bower_components/angular-ui-slider/src/slider.js"></script>
<script type="text/javascript" src="/bower_components/angular-bootstrap/ui-bootstrap.js"></script>
<script type="text/javascript" src="/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
<script type="text/javascript" src="/bower_components/angular-translate/angular-translate.js"></script>
<script type="text/javascript" src="/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js"></script>

<script type="text/javascript" src="/bower_components/raphael/raphael.js"></script>
<script type="text/javascript" src="/bower_components/raphael.free_transform/raphael.free_transform.js"></script>
<script type="text/javascript" src="/bower_components/raphael.json/raphael.json.js"></script>
<script type="text/javascript" src="/bower_components/raphael.export/raphael.export.js"></script>
<script type="text/javascript" src="/bower_components/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="/bower_components/modernizr/modernizr.js"></script>

<script type="text/javascript" src="/js/vendor/responsiveslides.min.js"></script>
<script type="text/javascript" src="/js/vendor/chardinjs.min.js"></script>
<script type="text/javascript" src="/js/vendor/jquery.cookie.js"></script>
<script type="text/javascript" src="/js/vendor/rgbcolor.js"></script>
<script type="text/javascript" src="/js/vendor/StackBlur.js"></script>
<script type="text/javascript" src="/js/vendor/canvg.js"></script>
<script type="text/javascript" src="/js/vendor/wordwrap.js"></script>
<script type="text/javascript" src="/js/vendor/simplify.js"></script>
<!-- endbuild -->

<!-- build:js({.tmp,app}) scripts/scripts.js -->
<script type="text/javascript" src="/js/app.js"></script>
<script type="text/javascript" src="/js/controllers/paper.js"></script>

<script type="text/javascript" src="/js/main-ck.js"></script>

<!-- shop -->
<script type="text/javascript" src="content/<?php echo $language?>/<?php echo $language?>.js"></script>
<script type="text/javascript">
$(document).ready( function(){
	$('.reset-basket').click(function(e) {
		e.preventDefault();

		var redirect = $(this).attr('href');

		$.ajax({
		     type: "POST",
		     url: "structure/_private/reset-basket.php",
		     dataType: "json",
		     cache: false,
		     complete: function(){

			    	window.location = redirect;

	         }
		});

	});
});
</script>

</body>
</html>
