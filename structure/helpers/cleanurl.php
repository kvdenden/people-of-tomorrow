<?php
function parse_path($base_path = '/') {
	$path = array();
	if (isset($_SERVER['REQUEST_URI'])) {
		$request_path = explode('?', $_SERVER['REQUEST_URI']);

		$path['base'] = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/');
		$path['call_utf8'] = substr(urldecode($request_path[0]), strlen($path['base']) + 1);
		$path['query'] = (count($request_path) > 1)? $request_path[1] : '';

		$path['call'] = utf8_decode($path['call_utf8']);

		if ($path['call'] == basename($_SERVER['PHP_SELF'])) {
			$path['call'] = '';
		}
		if( strlen($path['call']) == 0 ){
			$path['call_parts'][0] = "en"; //default to English
			$path['call_parts'][1] = "home"; //default to English home page
		}else{
			$tmp_parts = explode('/', $path['call']);
			$i = 0;
			foreach($tmp_parts as $part){
				if(strlen($part) >= 1){
					$path['call_parts'][$i] = $part;
				}
				$i++;
			}
		}
	}



	return $path;
}


function contentselector($path_info, $structure_xml){
	$contentfile = '';
	$_source = '';
	if( count($path_info['call_parts']) > 1 ){
		$end_id = '';
		$_source = $structure_xml;

		$selectedMenuItem = _recursiveStructureByPath($_source, $path_info['call_parts'][count($path_info['call_parts'])-1] );
		$contentfile = $selectedMenuItem['link'];
	}else{
		$content_tmp = _recursiveStructureByID($structure_xml, 0);
		$contentfile = $content_tmp['link']->__toString();
	}

	return $contentfile;
}

/** returns the menuitem that belongs to a specified path **/
function _recursiveStructureByPath($structure, $path) {
	$selectedMenuItem = NULL;

	foreach ($structure as $menu_item) {
		if(is_null($selectedMenuItem) ){
			if($menu_item['path'] == $path ){
				$selectedMenuItem = $menu_item;
			}

			if( (is_null($selectedMenuItem)) && (count($menu_item->navitem)>0) ){
				$tmpSelect = _recursiveStructureByPath( $menu_item->navitem, $path);
				if(!is_null($tmpSelect)){
					$selectedMenuItem = $tmpSelect;
				}
			}
		}
    }

    return $selectedMenuItem;
}

/** returns the menuitem that belongs to a specified ID **/
function _recursiveStructureByID($structure, $id) {
	$selectedMenuItem = NULL;
	foreach ($structure as $menu_item) {
		if(is_null($selectedMenuItem) ){
			if($menu_item['id'] == $id ){
				$selectedMenuItem = $menu_item;
			}

			if( (is_null($selectedMenuItem)) && (count($menu_item->navitem)>0) ){
				$tmpSelect = _recursiveStructureByID( $menu_item->navitem, $id);
				if(!is_null($tmpSelect)){
					$selectedMenuItem = $tmpSelect;
				}
			}
		}
    }

    return $selectedMenuItem;
}



function _getPathByID($structure, $id, &$fullpath_arr){
	$menuitem = _recursiveStructureByID($structure, $id);
	array_push($fullpath_arr, $menuitem['path']);

	if( isset($menuitem["parent"]) ){
		_getPathByID($structure, intval($menuitem['parent']), $fullpath_arr );
	}
	return $fullpath_arr;
}





/** give a url link (no language) of the id given **/
function getMenuLinkByID($id, $structure_xml){
	$resultString = "";
	$fullpath_arr = array();
	$fullpath_result =  _getPathByID($structure_xml, $id, $fullpath_arr);

	//explore($fullpath_result);

	for($i = 0; $i < count($fullpath_result) ; $i++){

		if($i > 0){
			$resultString = $fullpath_result[$i] . "/" . $resultString;
		}else{
			$resultString = $fullpath_result[$i];
		}
	}
	return $resultString;
}

/* retrieves a menu title from given ID */
function getMenuTitleByID($id, $structure_xml){
	$menuitem = _recursiveStructureByID($structure_xml, $id);
	return $menuitem['title'];
}

function getMenuTitleByPath($path_info, $structure_xml) {
	$active_path = $path_info['call_parts'][count($path_info['call_parts']) - 1];
	$menuitem = _recursiveStructureByPath($structure_xml, $active_path);
	return $menuitem['title'];
}

?>
