<?php 
$debug = false;

// Mail headers

$email_from = "diego@onetouchbvba.be";

$headers = "From: $email_from \r\n";
$headers .= "Reply-To:  $email_from \r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


// Shop settings
$activeCurrency = "EUR"; // We need a way to know what currency people will be using ?? 

$ogone_allowed_fields = array("ACCEPTANCE","AMOUNT","BRAND","CARDNO","CN","CURRENCY","ED","IP","NCERROR","ORDERID","PAYID","PM","SHASIGN","STATUS","TRXDATE");
$ogone_language_array = array("en" => "en_US", "nl" => "nl_NL");

// Products

$products["product_1"] = array("price" => array("EUR" => "10", "USD" => "13.46"),"type" => "normal","group" => false,"credits" => 1); // INDIVIDUAL SMALL
$products["product_2"] = array("price" => array("EUR" => "35", "USD" => "40.32"),"type" => "group","group" => true,"credits" => 4); // GROUP MESSAGE
$products["product_3"] = array("price" => array("EUR" => "40", "USD" => "54.16"),"type" => "large","group" => false,"credits" => 1); // INDIVIDUAL LARGE

$currency = array(
			"EUR" => "Euro",
			"USD" => "Dollar"
			);

// Gateway settings

$pspid = "Tomorrowland";
$passphrase = "TOMORROWLAND#86#SOCIALMINGLE#95#MIGHTY";
$passphrase_shaout = "TOMORROWLAND#86#SOCIALMINGLE#95#MIGHTY";
$ogone_url = "https://secure.ogone.com/ncol/prod/orderstandard.asp";

/*if (in_array ( $_SERVER ['HTTP_HOST'], array ('localhost', '127.0.0.1' ) )) {
	$pspid = "relaxybe";
	$passphrase = "tomorrowlandistop1";
	$passphrase_shaout = "tomorrowlandistop2";
	$ogone_url = "https://secure.ogone.com/ncol/test/orderstandard.asp";
} else {
	$pspid = "relaxybe";
	$passphrase = "tomorrowlandistop1";
	$passphrase_shaout = "tomorrowlandistop2";
	$ogone_url = "https://secure.ogone.com/ncol/test/orderstandard.asp";
	
	// Production settings
	
	$pspid = "Tomorrowland";
	$passphrase = "TOMORROWLAND#86#SOCIALMINGLE#95#MIGHTY";
	$passphrase_shaout = "TOMORROWLAND#86#SOCIALMINGLE#95#MIGHTY";
	$ogone_url = "https://secure.ogone.com/ncol/prod/orderstandard.asp";
}*/

// Session check if older than 1 day = destroy

if (isset($_SESSION['pot_basket_created']) && time() - $_SESSION['pot_basket_created'] < 86400) {
	$_SESSION['pot_basket_created'] = time();
	
} else if (isset($_SESSION['pot_basket_created']) && isset($_SESSION['pot_basket'])) {
	session_destroy();
}

// Functions

function datumformat_sql($datum) {

	if (strtotime ( $datum ) != "") {
		$datum = strftime ( "%Y-%m-%d", strtotime ( $datum ) );
	} else {
		$datum = "";
	}
	return $datum;
}

function generateShasign($fields,$passphrase) {
	unset($fields['SHASIGN']);
	ksort($fields);
	$phrase = '';
	foreach($fields as $key => $field){
		if(empty($field) && $field !== '0') continue;
		$phrase .= strtoupper($key) . '=' . $field . $passphrase;
	}
	return strtoupper(sha1($phrase));
}

function checkbox($waarde) {

	if ($waarde == "on") {

		$waarde = "1";
	} else {
		$waarde = "0";
	}

	return $waarde;
}

function display_quantity ($product) {
	
	$qty = isset($_SESSION['pot_basket']["items"]["$product"]['qty'])  ? $_SESSION['pot_basket']["items"]["$product"]['qty'] : 0;
	if ($qty > 0) {
		echo "<option value='$qty' selected='selected'>$qty</option>";
	}
}
function createOgoneUrl ($order_id,$type,$base_path,$language,$structure_xml,$pspid,$passphrase,$ogone_url) {
	
	if ($type == "session") {
		
		$email = $_SESSION['pot_basket']['user']['email'];
		$amount = $_SESSION['pot_basket']['total'] * 100;
		$currency = $_SESSION["pot_basket"]["currency"];
		$ogone_language = $_SESSION['pot_basket']['ogone_language'];
		
	}
	
	// TODO: retrieve information from database (this could be used to build the link for email reminders)
	
	// Feedback url's
	
	$accept_url = "$base_path$language/" . getMenuLinkByID(46, $structure_xml);
	$cancel_url = "$base_path$language/" . getMenuLinkByID(46, $structure_xml);
	$decline_url = "$base_path$language/" . getMenuLinkByID(46, $structure_xml);
	$exception_url = "$base_path$language/" . getMenuLinkByID(46, $structure_xml);
	$back_url = "$base_path$language/" . getMenuLinkByID(42, $structure_xml);
	$template_url = "$base_path" . "structure/templates/payment.tpl.php";
	$home_url = "$base_path$language/" . getMenuLinkByID(4, $structure_xml);
	$catalog_url = "$base_path$language/" . getMenuLinkByID(4, $structure_xml);

	
	$fields = array (
		"ACCEPTURL" => $accept_url,
		"AMOUNT" => $amount,
		"BACKURL" => $back_url,
		"CANCELURL" => $cancel_url,
		"CATALOGURL" => $catalog_url,
		"CURRENCY" => $currency,
		"DECLINEURL" => $decline_url,
		"EMAIL" => $email,
		"EXCEPTIONURL" => $exception_url,
		"EXCLPMLIST" => "Bank transfer BE;Bank transfer DE;Bank transfer FR",
		"HOMEURL" => $home_url,
		"LANGUAGE" => $ogone_language,
		"ORDERID" => $order_id,
		"PSPID" => $pspid,
		"TP" => $template_url
	);
	
	$ogone_sha = generateShasign($fields,$passphrase);
		
	$fields['SHASIGN'] = $ogone_sha;
	
	// Prepare query string
	$query_string = http_build_query($fields);
	
	$ogone_paymenturl = $ogone_url . "?" . $query_string;
	
	return $ogone_paymenturl;
	
}
?>
