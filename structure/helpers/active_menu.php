<?php
	function activesubmenu($offset = 0){

		$url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if( $offset < 0){ $offset = 0; }
		$offset++;
		if( $offset > (count($url_arr))){ $offset = count($url_arr); }
  		return $url_arr[count($url_arr) - $offset];
	}

	function getActiveMainMenu($path_info){
		return $path_info['call_parts'][1];
	}

	/*
	function getActiveMenu($path_info){
		$active_menu_link = $path_info['call_parts'][ count($path_info['call_parts']) -1 ];
		$active_main_menu_link = getActiveMainMenu($path_info);
		if($active_menu_link != $active_main_menu_link){
		}else{
			return $active_main_menu_link;
		}
	}
	*/

	function getActiveMenuID($path_info, $structure_xml){
		$active_path = getActiveMainMenu($path_info);
		$active_id = NULL;
		foreach($structure_xml as $menuItem){
			if($menuItem['path'] == $active_path){
				$active_id = $menuItem['id'];
			}
		}
		return $active_id;
	}


	/** hide main menu or not */
	function hidemenu($path_info, $structure_xml){
		$active_path = $path_info['call_parts'][count($path_info['call_parts']) - 1];

		$cur_menu_item = _recursiveStructureByPath($structure_xml, $active_path);

		$return = false;
		
		if(isset($cur_menu_item['hidemainmenu'])){
			if($cur_menu_item['hidemainmenu'] == "1"){
				$return = true;
			}
		}

		return $return;
	}
?>