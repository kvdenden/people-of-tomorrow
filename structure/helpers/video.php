<?php
	
	function createHtml5Video($source){
		$htmlString = "<div class='video__source'>";
			$htmlString .= "<video autoplay='autoplay' preload='auto' loop>";
				$htmlString .= "<source src='".$source.".mp4' />";
				$htmlString .= "<source src='".$source.".webm' type='video/webm; codecs=vp8,vorbis' />";
				$htmlString .= "<source src='".$source.".ogv' type='video/ogg; codecs=theora,vorbis' />";
				
			$htmlString .= "</video>";
		$htmlString .= "</div>";
		//$htmlString .= "<img class='video__poster' src='".$source.".png' />";
		return $htmlString;
	}
?>