<?php 
	$showOwLogo_arr = array(4, 41, 42, 43, 44, 5, 51 ,52, 666); 

	$activeMenuItem = _recursiveStructureByPath($structure_xml, $path_info['call_parts'][count($path_info['call_parts'])-1]);

	if( $activeMenuItem['id'] == '' ){
		$activeMenuItem['id'] = 666;
	}

	$showOwLogo = in_array($activeMenuItem['id'], $showOwLogo_arr);
?>

<header class='container'>
		<div class='row'>
			<?php if($showOwLogo){ ?>
				<div class='col-md-2 col-sm-3 col-xs-12'>
					<div id='logo'>
						<a href='<?php echo $language . "/home";?>'>
							<img src='images/pot_logo.svg'/>
						</a>
					</div>
				</div>

				<div class='col-md-5 col-sm-3 col-xs-12 ow_logo_wrapper'>
					<?php require_once dirname(__FILE__) . '/snippets/_oneworld_header_logo.tpl.php'; ?>
				</div>

				<div class='col-md-5 col-sm-6 col-xs-12 menudiv'>
					<?php
						if(!hidemenu($path_info, $structure_xml) ){
							require_once dirname(__FILE__) . '/_mainmenu.php';
						} else {
							?>
							<div class="social pull-right">
								<a href="http://www.facebook.com/peopleoftomorrowofficial" target="_blank">
									<img src="content/_global/images/social/facebook_icon_blue.png"/>
								</a>
							</div>
							<?php
						}
					?>
				</div>
			<?php }else{ ?>
				<div class='col-xs-12 col-sm-3'>
					<div id='logo'>
						<a href='<?php echo $language . "/home";?>'>
							<img src='images/pot_logo.svg'/>
						</a>
					</div>
				</div>

				<div class='col-xs-12 col-sm-3 menudiv'>
					<a class="learnmore" href="<?php echo $language . '/' . getMenuLinkByID(12, $structure_xml); ?>">
						<?php echo getMenuTitleByID(12, $structure_xml);?>
					</a>
					
					<div class="social social-mobile">
						<a href="http://www.facebook.com/peopleoftomorrowofficial" target="_blank">
							<div class="fa fa-facebook-square"></div>
						</a>

						<a href="http://www.twitter.com/POTomorrow" target="_blank">
							<div class="fa fa-twitter-square"></div>
						</a>
					  <!--
					  <a href="http://www.instagram.com/peopleoftomorrow" target="_blank">
					    <i class="fa fa-instagram"></i>
					  </a>
					  -->
					</div>
				</div>

				<div class='col-xs-12 col-sm-3 blue'>
					
				</div>

				<div class='col-xs-12 col-sm-3 menudiv'>
					<?php 
						if( !hidemenu($path_info, $structure_xml) ){
							//require_once dirname(__FILE__) . '/_mainmenu.php';
						?>
							<a class="gift" href="<?php echo $language . '/' . getMenuLinkByID(5, $structure_xml); ?>">
								<div class="center-middle">
									<?php echo getMenuTitleByID(5, $structure_xml); ?>
								</div>
								<div class='glyphicon glyphicon-gift'></div>
							</a>
						<?php
						
						} else {
							?><!--<div class="social pull-right"><a href="http://www.facebook.com/peopleoftomorrowofficial" target="_blank"><img src="content/_global/images/social/facebook_icon_blue.png"/></a></div>-->
							<?php
						}
						
					?>
				</div>
			<?php } ?>
		</div>
</header>
