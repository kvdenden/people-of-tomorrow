<header class='container'>
	<div class='row'>
		<div class='col-sm-8'>
			<div id='logo'>
				<!--<a href=''>-->
					<img src='../../images/oneworld_logo_small.png'/>
				<!--</a>-->
			</div>
			<p>A PUBLIC ARTPIECE BY <b>ARNE QUINZE</b></p>
		</div>

		<div class='col-sm-4'>
			<nav id='main-navigation' class='navbar' role='navigation'>
				<ul class="nav navbar-nav">
					<li><a href='mailto:kristien@tomorrowland.com'>Need Assistance?</a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>