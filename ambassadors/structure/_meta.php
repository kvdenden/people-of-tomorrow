<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<meta name="application-name" content="People of Tomorrow - One world"/>
	<meta name="msapplication-TileColor" content="#333233"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
	<meta http-equiv="cleartype" content="on">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   	<meta name="robots" content="noindex,nofollow"/>

    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>
	
	<!--<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" />-->
	<!-- <link rel="stylesheet" type="text/css" href="css/libraries/jquery.vegas.min.css"/> -->
	<link rel="stylesheet" type="text/css" href="../../css/main.css"/>

    <title>People of Tomorrow - One World</title>
</head>
<body>
	 <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--[if lt IE 9]>
      <script src="bower_components/es5-shim/es5-shim.js"></script>
      <script src="bower_components/json3/lib/json3.min.js"></script>
    <![endif]-->