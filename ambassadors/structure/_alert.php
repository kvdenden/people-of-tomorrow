<div class="alert alert alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <div class="message"></div>
</div>