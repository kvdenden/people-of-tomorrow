<div id="upload-modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Upload your message</h4>
      </div>
      <div class="modal-body">
        <form id="upload-form" action="../../private/upload.php" method="post" enctype="multipart/form-data" role="form">
          <div class="form-group">
            <label for="name" class="required">Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Please enter your name" required>
          </div>
          <div class="form-group">
            <label for="country">Country</label>
            <input type="text" class="form-control" id="country" name="country" placeholder="Please enter your country">
          </div>
          <div class="form-group">
            <label for="file" class="required">File</label>
            <input type="file" id="file" name="file" required>
            <!--<p class="help-block">Upload your message as a transparent png file.</p>-->
          </div>
          <input id="submit" type="submit" value="submit" class="hidden">
        </form>
        <div id="progress">
        <span>Uploading...</span>
        <div class="progress progess-striped">
          <div class="progress-bar progress-info active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            0% Complete
          </div>
        </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
        <button id="upload-button" type="button" class="btn btn-primary">Upload your message</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->