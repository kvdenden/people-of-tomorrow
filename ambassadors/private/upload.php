<?php  
// error_reporting(E_ALL);
include 'db.php';

if (isset($_POST["name"]) && isset($_FILES["file"]) && $_FILES["file"]["error"] == UPLOAD_ERR_OK) {
	$name = $_POST["name"];
	$country = isset($_POST["country"]) ? $_POST["country"] : null;
	$ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
  $filename = md5(rand()) . '.' . $ext;
  $success = move_uploaded_file( $_FILES["file"]["tmp_name"], "../uploads/" . $filename);
  if ($success) {
  	// save to database
  	$query = $db->prepare("INSERT INTO uploads (name, country, filename) VALUES (:name, :country, :filename)");
		$query_success = $query->execute(array('name' => $name, 'country' => $country, 'filename' => $filename));
		if ($query_success) {
  		// return success alert
// var_dump($_POST);  
// var_dump($_FILES);
?>
<strong>Thank you!</strong> Your message has been received.
<?php
		} else {
			header('HTTP/1.1 500 Internal Server Error');
		}
  } else {
  	// return
  	header('HTTP/1.1 500 Internal Server Error');
  }
} else {
	header('HTTP/1.1 500 Internal Server Error');
}