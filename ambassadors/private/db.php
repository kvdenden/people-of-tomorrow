<?php
	class DBController {
		private static $_INSTANCE;
		
		private $_connection;
		
		private function DBController($db_host, $db_port, $db_user, $db_password, $db_name) {
			$this->__construct($db_host, $db_port, $db_user, $db_password, $db_name);
		}

		/* ::::: constructor ::::: */
		private function __construct($db_host, $db_port, $db_user, $db_password, $db_name) {
			$this->_connection = new PDO("mysql:host={$db_host};port={$db_port};dbname={$db_name}", $db_user, $db_password);
			$this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
	
		public function getConnection() {
			return $this->_connection;
		}

		/* ::::: singleton ::::: */
		public static function getInstance($db_host, $db_port, $db_user, $db_password, $db_name) 
		{ 
			if (!self::$_INSTANCE) {
				self::$_INSTANCE = new DBController($db_host, $db_port, $db_user, $db_password, $db_name); 
			} 
			
			return self::$_INSTANCE; 
		} 
	}

	$dbc = DBController::getInstance('mac21.be', '3306', 'ambassador', '0Gg7T850', 'ambassador');
	$db = $dbc->getConnection();
?>