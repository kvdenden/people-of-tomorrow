
$(document).ready( function(){
	//$(window).resize(function(){
	//	var parent = $(".btn-primary").parent()
	//	console.log( $(parent).css('width') + " " + $(".btn-primary").css('width'));
	//});

/**** Scroll to anchor ****/
	function scrollToAnchor(aid){
		var aTag = $("a[name='"+ aid +"']");
		$('html,body').animate({scrollTop: aTag.offset().top}, 400);
	}

	$(".scrollto").click( function(event){
		event.preventDefault();
		scrollToAnchor( $(this).attr('id') );
	});

	function updateProgess(percentComplete) {
		$("#progress .progress-bar").css("width", percentComplete + "%");
		$("#progress .progress-bar").text(percentComplete + "% Complete");
	}

	function start(formData, jqForm, options) {
			$('#progress').hide().fadeIn();
	}

	function success(responseText, statusText, xhr, $form)  {
		$('#upload-modal').modal('hide');

		updateProgess(100);
		$('#file').clearFields();
		$('#progress').hide();
		$("#progress .progress-bar").css("width", "0%");
		$("#progress .progress-bar").text("0% Complete");

		$alert = $(".hidden .alert").clone();
		$alert.addClass("alert-success");
		$alert.find('.message').html(responseText);
		// show success message
		$("#alerts").append($alert);
	}
	function failure() {
		$('#progress').hide();
		$alert = $(".hidden .alert").clone();
		$alert.addClass("alert-danger");
		$alert.find('.message').html("Something went wrong... Please contact <a class='alert-link' href='mailto:help@example.com'>help@example.com</a> for more information");
		$("#upload-modal .modal-body").prepend($alert);	
	}
	function progress(event, position, total, percentComplete) {
		updateProgess(percentComplete);
	}

	function submitForm(event) {
		$("#submit").click();
		event.preventDefault();
	}

	$("#upload-button").click(submitForm);
	$("#upload-form input").not('[type=submit]').jqBootstrapValidation({
		submitSuccess: function($form, event) {
			$form.ajaxSubmit({
				beforeSubmit: start,
				success: success,
				error: failure,
				uploadProgress: progress
			});
			event.preventDefault();

		}
	});

});