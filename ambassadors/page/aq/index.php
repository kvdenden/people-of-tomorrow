<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', '1');
?>

<?php require_once dirname(__FILE__) . '/../../structure/_meta.php'; ?>
<?php require_once dirname(__FILE__) . '/../../structure/_header.php'; ?>

<div id='main' role='content'>

	<div class="fullscreen-container media-container aq">
		<div class="media-container__overlay">
		</div>
		<img src="../../images/aq_header_bg.jpg">
	</div>

	<div class='container'>
		<div id="alerts" class="row"></div>
		<div class='row'>
			<div class='col-sm-8 col-xs-12'>
				<h1>Welcome to the People of Tomorrow.</h1> 
				<p>
					We are Excited to welcome you as one of the first ambassadors of the People of Tomorrow, an exiting 
					project that will kick-off on 29th November.
				</p>
				<p>
					May we kindly ask you to keep this information confidential for the time being until the official launch.
					Thanks for your support!
				</p>
			</div>

			<div class='col-sm-4 col-xs-12'>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload-modal">
					Upload<br/>your<br/>message<br/>here
				</button>
			</div>
		</div>
	</div>

	<div class="fullscreen-container gray">
		<div class='container'>
			<div class='row'>
				<div class='col-sm-8 col-sx-12'>
					<h1>Project People Of Tomorrow, One World, Bridge project by Arne Quinze</h1>
					<p>
						Arne Quinze and the makers of Tomorrowland join forces for a prestigious project.
					</p>

					<p>
						People Of Tomorrow, a new community started by Arne Quinze and the makers of Tomorrowland, based on the international feeling of friendship and unity, will kick-off with a amazing project, called One Bridge by Arne Quinze.  
					</p>

					<p>
						Arne Quinze, well known from his public installations and community projects, has created a <b>unique public art installation</b>. This work of art will be built in a park near Antwerp, Belgium. The park, home of the biggest festival in the world Tomorrowland will host this permanent art project as a symbol for freedom and unity. The extraordinary character of this work of art will be the 202.000 unique laser engraved messages into its wood. A record breaking crowd funding will make it possible to 202.000 persons from all over the world (+200 nationalities ) to get there voices heard via a unique message they can customise. With each contribution, we will donate a part of the turnover to the organisation Human Rights Watch.  	
					</p>

					<p>
						A project open for everybody in the world who wants to proclaim the positive message ' to unite '.Offering the opportunity to a conscious movement of people to join their willpower and halo a positive and united message to the world.
With a strong online and application driven backbone this art installation, built out of thousands of wooden slates metal and glass, will brigde a path of 600 meters long and 30 meters high. It will confront it’s audience and evoke communication. 
					</p>
 						
 					<p>
 						Arne Quinze and Tomorrowland would like to ask you to become ambassador for this project and send us your message of tomorrow.  We will turn your message into a design and show it to the world at the global launch beginning of December. 
						<br>
						Of course, you can send us a designed message too, should you wish.  
 					</p>

					<!--<p>
						<a href="">In the enclosed file</a>, you will find some visuals and the model of the bridge.
					</p>
					-->

					<p>
						Arne hopes you will join our community and is looking forward to read your message!
					</p>

				</div>

				<div class='col-sm-4 col-xs-12'>
					<div class='imgcontainer bm'><img src='../../images/bridge02.jpg' /></div>
					<div class='imgcontainer'><img src='../../images/aq_engraveyourmessage.jpeg' /></div>
				</div>
			</div>
		</div>
	</div>
	
	<div class='container'>
		<div class='row'>
			<div class='col-sm-8 col-xs-12'>
				<h1>Format</h1>
				<p>
					2 OPTIONS<br/> 
					square: 10cm x 10cm or 3.94 inch x 3.94 inch<br/>
					rectangular:  20cm x 5 cm or 7.87 inch x 1.97 inch<br />
					in 160dpi
				</p>
				<p>
					Need assistance? <a href="mailto:kristien@tomorrowland.com">click here</a>
				</p>
			</div>

			<div class='col-sm-4 col-xs-12'>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload-modal">Upload<br/>your<br/>message<br/>here</button>
			</div>
		</div>
	</div>
</div>

</div>
<div class="hidden">
	<?php require_once dirname(__FILE__) . '/../../structure/_alert.php'; ?>
</div>
<?php require_once dirname(__FILE__) . '/../../structure/_modal.php'; ?>

<?php require_once dirname(__FILE__) . '/../../structure/_footer.php'; ?>