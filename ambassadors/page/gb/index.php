<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', '1');
?>

<?php require_once dirname(__FILE__) . '/../../structure/_meta.php'; ?>
<?php require_once dirname(__FILE__) . '/../../structure/_header.php'; ?>

<div id='main' role='content'>

	<div class="fullscreen-container media-container gb">
		<div class="media-container__overlay">
		</div>
		<img src="../../images/header_bg.jpg">
	</div>

	<div class='container'>
		<div id="alerts" class="row"></div>
		<div class='row'>
			<div class='col-sm-8 col-xs-12'>
				<h1>Welcome to the People of Tomorrow.</h1> 
				<p>
					We are Excited to welcome you as one of the first ambassadors of the People of Tomorrow, an exiting 
					project that will kick-off on 29th November.
				</p>
				<!--
				<p>
					Have already confirmed their contribution: David Guetta, Dimitri Vegas & Like Mike, Drew Seskunas, Sunny Que, ... 
				</p>
				-->
				<p>
					May we kindly ask you to keep this information confidential for the time being until the official launch.
					Thanks for your support!
				</p>
			</div>

			<div class='col-sm-4 col-xs-12'>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload-modal">
					Upload<br/>your<br/>message<br/>here
				</button>
			</div>
		</div>
	</div>

	<div class="fullscreen-container gray">
		<div class='container'>
			<div class='row'>
				<div class='col-sm-6 col-sx-12'>
					<h1>What is People of Tomorrow?</h1>
					<p>
						People Of Tomorrow, our new community, based on the international feeling of friendship, from the 
						people of Tomorrowland - just picture the numerous flags - will kick-off with a amazing project, called One 
						Bridge. 
					</p>

					<p>
						Arne Quinze, well known from his public installations and community projects, has designed a unique 
						bridge. This work of art will be built in The Schorre, the home of Tomorrowland, thanks to the contribution 
						of the Province of Antwerp and will be ready before the 2014 edition of Tomorrowland. Bikers, walkers & 
						visitors of The Schorre, will benefit from this bridge and use it throughout the entire year. 
					</p>

					<p>
						But there’s more… the extraordinary character of this art work will be the 200.000 unique and laser 
						engraved messages into the wood of the bridge. These messages and designs will be available for sales 
						via crowd funding and sent to us from people all over the world and become the symbol of The People of 
						Tomorrow. With each message sold, we will donate a part to the organisation Human Rights Watch. 	
					</p>

					<p>
						Arne Quinze and Tomorrowland would like to ask you to become ambassador for this project and send us 
						your message of tomorrow. We will turn your message into a design and show it to the world at the 
						launch beginning of December. Of course, you can send us a designed message too, should you wish. 
					</p>
 

					<!--<p>
						<a href="">In the enclosed file</a>, you will find some visuals and the model of the bridge and some examples of 
						designed messages.
					</p>
					-->

					<p>
						Arne & Tomorrowland hope you will join our community and are looking forward to reading your message!
					</p>

				</div>

				<div class='col-sm-6 col-xs-12'>
					<div class='imgcontainer bm'><img src='../../images/bridge01.jpg' /></div>
					<div class='imgcontainer'><img src='../../images/aq_engraveyourmessage.jpeg' /></div>
					<div class='imgcontainer bm2'><img src='../../images/bridge02.jpg' /></div>

				</div>
			</div>
		</div>
	</div>
	
	<div class='container'>
		<div class='row'>
			<div class='col-sm-8 col-xs-12'>
				<h1>Format</h1>
				<p>
					2 OPTIONS<br/> 
					square: 10cm x 10cm or 3.94 inch x 3.94 inch<br/>
					rectangular:  20cm x 5 cm or 7.87 inch x 1.97 inch<br />
					in 160dpi
				</p>
				<p>
					Need assistance? <a href="mailto:kristien@tomorrowland.com">click here</a>
				</p>
			</div>

			<div class='col-sm-4 col-xs-12'>
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#upload-modal">Upload<br/>your<br/>message<br/>here</button>
			</div>
		</div>
	</div>
</div>

</div>
<div class="hidden">
	<?php require_once dirname(__FILE__) . '/../../structure/_alert.php'; ?>
</div>
<?php require_once dirname(__FILE__) . '/../../structure/_modal.php'; ?>

<?php require_once dirname(__FILE__) . '/../../structure/_footer.php'; ?>