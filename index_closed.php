<!DOCTYPE html>
<html lang="en">
<head>
 	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<meta name="application-name" content="Tomorrowland Sculpture"/>
	<meta name="msapplication-TileColor" content="#333233"/>
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="MobileOptimized" content="width">
	<meta name="HandheldFriendly" content="true">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
	<meta http-equiv="cleartype" content="on">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <base href="/" />

    <link rel="icon" href="/favicon.ico" type="image/x-icon"/>

	<!--<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" />-->
	<link rel="stylesheet" type="text/css" href="/bower_components/jquery-ui/themes/base/jquery-ui.css">
	<!-- <link rel="stylesheet" type="text/css" href="css/libraries/jquery.vegas.min.css"/> -->
	<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css"/>
	<link rel="stylesheet" type="text/css" href="/css/editor.css"/>
	<link rel="stylesheet" type="text/css" href="/css/shop.css"/>
	<link rel="stylesheet" type="text/css" href="/css/responsiveslides.css"/>
</head>
<body id="closingpage">
	<div class="fullscreen-container container thankyou">
		<div class="row logo">
			<div class="col-xs-12">
				<div style='width:200px; margin:0 auto;'><img class="pot_logo" src="images/pot_logo_white.svg"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class='big_text'>Thank you!</div>
				<p class='sub-text'>for being part of this wonderful piece of art</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<p class="text-center">Building of this amazing bridge is now in full progress.<br><u><strong>It is no longer possible to submit any messages.</strong></u></p>
				<div class="social_wrapper text-center">
					<p class="text-center">Follow the building process on</p>
					<a class='social-linkss facebook' href='https://www.facebook.com/peopleoftomorrowofficial'><i class="fa fa-facebook"></i></a>
					<a class='social-linkss twitter' href='https://www.twitter.com/POTomorrow'><i class="fa fa-twitter"></i></a>
					<a class='social-linkss instagram' href='https://www.instagram.com/peopleoftomorrowofficial'><i class="fa fa-instagram"></i></a>
				</div>
			</div>
		</div>
	</div>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-45895636-1', 'peopleoftomorrow.com');
		ga('send', 'pageview');
		ga('require', 'ecommerce', 'ecommerce.js');   // Load the ecommerce plug-in.
	</script>
</body>
</html>

<!-- Google Analytics -->

