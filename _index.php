<?php
	session_start();

	require_once dirname(__FILE__) . '/structure/helpers/shop.php';
	require_once dirname(__FILE__) . '/structure/helpers/cleanurl.php';
	require_once dirname(__FILE__) . '/structure/helpers/active_menu.php';
	require_once dirname(__FILE__) . '/structure/helpers/submenu.php';
	require_once dirname(__FILE__) . '/structure/helpers/video.php';
	require_once dirname(__FILE__) . '/structure/helpers/social.php';

	//error_reporting(E_ALL);
	//if( ($_SERVER['SERVER_ADDR'] == "127.0.0.1") || ($_SERVER['SERVER_ADDR'] == "::1") ){
		//error_reporting(E_ALL & ~E_NOTICE);
		//ini_set('display_errors', '0');
	//}

		//error_reporting(E_ALL | E_STRICT);
		//ini_set('display_errors', '1');



	// REMOVE FORM PRODUCTION
	require_once dirname(__FILE__) . '/structure/helpers/dumpy.php';
	//

	$global_xml = simplexml_load_file('content/global.xml');

	// get the correct base path
	$base_path = '/';
	if( ($_SERVER['SERVER_ADDR'] == "127.0.0.1") || ($_SERVER['SERVER_ADDR'] == "::1") ){
		$base_path = $global_xml->serverconfig->base_path_local['value'][0];
	}else if( ($_SERVER['SERVER_ADDR'] == "87.238.163.181") || ($_SERVER['SERVER_ADDR'] == "87.238.163.18")){
		$base_path = $global_xml->serverconfig->base_path_staging['value'][0];
	}else{
		$base_path =$global_xml->serverconfig->base_path_live['value'][0];
	}

	$path_info = parse_path($base_path);
	// check if language folder exists otherwise show 404 in default language (English)
	if( !file_exists(dirname(__FILE__) . '/content/' . $path_info['call_parts'][0] . '/') ){
		$path_info['call_parts'][0] = 'en';
		$path_info['call_parts'][1] = '404';
	}

	$structure_en_xml = simplexml_load_file( 'content/en/structure.xml');
	$structure_nl_xml = simplexml_load_file( 'content/nl/structure.xml');


	$language = $path_info['call_parts'][0];
	$structure_xml =  simplexml_load_file( 'content/' . $language . '/structure.xml');
	$content_xml =  simplexml_load_file( 'content/' . $language . '/content.xml');

	$page_title = $content_xml->title;
	try {
		$current_title = getMenuTitleByPath($path_info, $structure_xml);
		if($current_title) {
			$page_title = $current_title . " | " . $page_title;
		}
	} catch(Exception $e) {}


	$path_tmp = _recursiveStructureByID($structure_xml, 0);

	if(!isset($path_info['call_parts'][1])){
		$path_info['call_parts'][1] = $path_tmp['link']->__toString();
	}else{
		if( strlen($path_info['call_parts'][1]) <= 1 ){

			$path_info['call_parts'][1] = $path_tmp['link']->__toString();
		}
	}

	$activmenuId = getActiveMenuID($path_info, $structure_xml);
	$activeMenuItem = $structure_xml->xpath('navitem[@id="'. $activmenuId . '"]');

	if( isset($_GET['debug']) ){
		if($_GET['debug'] == 1){
			explore($activmenuId);
			explore($activeMenuItem);
		}
	}

	if($activmenuId == 34){
		$header_type = "tfl";
	}

	//explore($_SESSION);
	//explore($activmenuId);
	//explore($activeMenuItem[0]);


	if( isset($activeMenuItem[0])){
		$header_type = $activeMenuItem[0]['header'];
	}else{
		$header_type = "home";
	}

	if($header_type == ""){
		$header_type = "normal";
	}

	//explore($activeMenuItem);
	//explore( $activmenuId );
	/*** Tomorrowland Ticket Sale ***/
	if($activmenuId == 30){
		// search email and extra token for the
		//$fts_email = (isset($_GET['e']))? $_GET['e'] : "";
		$fts_flow = (isset($_GET['t']))? $_GET['t'] : "";

		if(($fts_flow != "fts") && ($fts_flow != "fts<")){
			header('Location: ' . $base_path . $language . "/" . getMenuLinkByID(999, $structure_xml));
		}
	}

	if($activmenuId != 42){
		require_once dirname(__FILE__) . '/structure/_meta.php';

		if($activmenuId != 32){
			require_once dirname(__FILE__) . '/structure/_header.php';
		}

		echo "<div id='main' class='".$header_type. ' ' . $language . "' role='content'>";
	}

?>
<?php
	// load the correct content
	$file_path = dirname(__FILE__) . '/structure/templates/'. contentselector($path_info, $structure_xml) . '.tpl.php';

	if( file_exists($file_path) ){
		require_once $file_path;
	}else{
		require_once dirname(__FILE__) . '/structure/templates/404.tpl.php';
	}
?>
<?php
	if($activmenuId != 42){
		echo "</div>";
		require_once dirname(__FILE__) . '/structure/_footer.php';
	}
?>
