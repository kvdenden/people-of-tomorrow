$(document).ready( function(){
	/** TML homepage - ticket sale image sizer**/
	var tml_bg_w = 1024;
	var tml_bg_h = 1134;
	var ratio = tml_bg_w/tml_bg_h;
	var alt_ratio = tml_bg_h/tml_bg_w;

	$(window).resize( function(){
		var window_w = $(window).width();
		var window_h = $(document).height();
		var footer_h = $('footer').height();
		var new_h = window_w * alt_ratio;
		var new_w = window_w;

		if( new_h < (window_h - footer_h) ){
			new_w = ratio * window_h;
			new_h = window_h;
		}

		$("body").css('background-size', new_w + "px " + new_h + "px");
	});

	var window_w = $(window).width();
	var window_h = $(document).height();
	var footer_h = $('footer').height();
	var new_h = window_w * alt_ratio;
	var new_w = window_w;

	if( new_h < (window_h - footer_h) ){
		new_w = ratio * window_h;
		new_h = window_h;
	}

	$("body").css('background-size', new_w + "px " + new_h + "px");
});