<?php
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
?>

<?php

	require_once dirname(__FILE__) . '/structure/helpers/cleanurl.php';
	require_once dirname(__FILE__) . '/structure/helpers/active_menu.php';
	
	// REMOVE FORM PRODUCTION
	require_once dirname(__FILE__) . '/structure/helpers/dumpy.php';
	//
	
	$global_xml = simplexml_load_file('content/global.xml');

	// get the correct base path
	$base_path = '/';
	if( ($_SERVER['SERVER_ADDR'] == "127.0.0.1") || ($_SERVER['SERVER_ADDR'] == "::1") ){
		$base_path = $global_xml->serverconfig->base_path_local['value'][0];
	}else if( ($_SERVER['SERVER_ADDR'] == "87.238.163.181") || ($_SERVER['SERVER_ADDR'] == "87.238.163.18")){
		$base_path = $global_xml->serverconfig->base_path_staging['value'][0];
	}else{
		$base_path =$global_xml->serverconfig->base_path_live['value'][0];		
	}
		
	$path_info = parse_path($base_path);
	$language = $path_info['call_parts'][0];

	$structure_xml =  simplexml_load_file( 'content/' . $language . '/structure.xml');
	$content_xml =  simplexml_load_file( 'content/' . $language . '/content.xml');
?>


<?php require_once dirname(__FILE__) . '/structure/_meta.php'; ?>

<?php require_once dirname(__FILE__) . '/structure/_header.php'; ?>

<div id='main' role='content'>
<?php
	// load the correct content
	require_once dirname(__FILE__) . '/structure/templates/'. contentselector($path_info, $structure_xml) . '.tpl.php';
?>
</div>

<?php require_once dirname(__FILE__) . '/structure/_footer.php'; ?>