<!-- Google Analytics -->
<script>
	/*
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-45895636-1', 'peopleoftomorrow.com');
	  ga('send', 'pageview');
  */
</script>
   
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/libraries/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/vendor/underscore-min.js"></script>
<script type="text/javascript" src="js/libraries/jquery.anythingslider.js"></script>
<script type="text/javascript" src="js/vendor/jquery.form.js"></script>
<script type="text/javascript" src="js/vendor/jqBootstrapValidation-1.3.7.min.js"></script>

<script src="bower_components/angular-bootstrap/ui-bootstrap.js"></script>
<script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>

<script type="text/javascript" src="js/main-ck.js"></script>

<script type="text/javascript" src="js/neighbours.js"></script>

</body>
</html>