<?php
	try {
		include 'db.php';
		
		$query = $db->prepare("SELECT COUNT(token) FROM neighbours WHERE used = 0");
		$query->execute(array());
		$result = $query->fetchAll();
		echo(json_encode($result[0][0]));
		// echo(json_encode(0));
	} catch (Exception $e) {
		header('HTTP/1.1 500 Internal Server Error');
	}

?>