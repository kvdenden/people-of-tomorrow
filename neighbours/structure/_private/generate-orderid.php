<?php
require_once 'db.php';
// Generate order ID

if (! isset ( $_SERVER ['HTTP_X_FORWARDED_FOR'] )) {
	$ip = $_SERVER ['REMOTE_ADDR'];
} else {
	$ip = $_SERVER ['HTTP_X_FORWARDED_FOR'];
}

$i = 0;

while ( $i < 100 ) {
	$random = rand ( 1, 99999 );
	$ip = str_pad ( substr ( preg_replace ( "/[^0-9]/", "", $ip ) * $random, 3, 6 ), 6, '0', STR_PAD_LEFT );
	$stamp = strtotime ( "now" );
	$order_id = "POT-$stamp-$ip";

	try {
		$query = $db->prepare ( "SELECT order_id
						FROM orders
						WHERE order_id = :order_id
						" );
		$query->execute ( array ('order_id' => $order_id ) );
		$result = $query->fetch ( PDO::FETCH_ASSOC );
		
		if (!$result) {
			// no result = success
			break;
		}
	} catch ( Exception $e ) {
		header ( 'HTTP/1.1 500 Internal Server Error' );
	}
	$i ++;
}

?>