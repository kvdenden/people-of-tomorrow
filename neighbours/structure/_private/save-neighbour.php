<?php
	// if(!isset($_POST['form_email']) || !isset($_POST['form_name']) || !isset($_POST['form_surname']) || !isset($_POST['form_street']) || !isset($_POST['form_number']) || !isset($_POST['form_city']) || !isset($_POST['form_zip']) || !isset($_POST['form_country'])) {
	// 	echo json_encode(array()); // return {}
	// } else {

// echo(json_encode($_POST));

		try {
			
			$email = $_POST['form_email'];
			$first_name = $_POST['form_name'];
			$last_name = $_POST['form_surname'];
			$street = $_POST['form_street'];
			$streetnr = $_POST['form_number'];
			$city = $_POST['form_city'];
			$zip = $_POST['form_zip'];
			$country = $_POST['form_country'];

			include 'db.php';

			$query = $db->prepare("SELECT token FROM neighbours WHERE used = 0");
			$query->execute();
			$result = $query->fetch(PDO::FETCH_ASSOC);
			if($result) {
				// token available
				$token = $result['token'];

				// $str = "UPDATE neighbours SET used=1,email={$email},firstname={$first_name},lastname={$last_name},street={$street},streetnr={$streetnr},city={$city},zip={$zip},country={$country} WHERE token={$token}";
				// echo($str);

				$query = $db->prepare("UPDATE neighbours SET
					used=1,email=:email,firstname=:firstname,lastname=:lastname,street=:street,streetnr=:streetnr,city=:city,zip=:zip,country=:country
					WHERE token=:token");
				$success = $query->execute(array('email' => $email, 'firstname' => $first_name, 'lastname' => $last_name, 'street' => $street, 'streetnr' => $streetnr, 'city' => $city, 'zip' => $zip, 'country' => $country, 'token' => $token));

				if($success){
					$result_arr = array( 	'token' => $token, 
											'email' => $email, 
											'firstname' => $first_name, 
											'lastname' => $last_name
										);
					echo json_encode($result_arr); 

					//return $result_arr;
				} else {
					echo json_encode(array()); // return {}
				}
			} else {
				echo json_encode(array()); // return {}
			}
		} catch (Exception $e) {
			echo json_encode(array($e->getMessage())); // return {}
			// header('HTTP/1.1 500 Internal Server Error');
			// echo json_encode(array()); // return {}
		}
	// // }

?>