<?php

// return {id: credit_id} if token is valid
// return {} if token is invalid

	if(!isset($_POST['type']) || !isset($_POST['token'])) {
		// missing parameters
		echo json_encode(array()); // return {}
	} else {
		$type = $_POST['type'];
		$token = $_POST['token'];

		try {
			include 'db.php';
			$query = $db->prepare("SELECT credits.id
				FROM credits, orders
				WHERE credits.type = :type
				AND credits.token = :token
				AND credits.order_id = orders.id
				AND orders.paid = 1
				AND credits.id NOT IN( SELECT credit_id FROM designs where credit_id is not null )");
			$query->execute(array('type' => $type, 'token' => $token));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			if($result) {
				// success
				echo json_encode($result); // return {id: credit_id}
			} else {
				// invalid token
				echo json_encode(array()); // return {}
			}
		} catch (Exception $e) {
			header('HTTP/1.1 500 Internal Server Error');
		}
	}

?>