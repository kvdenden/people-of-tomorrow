<?php
	if(!isset($_POST['data']) || !isset($_POST['filename']) || !isset($_POST['width']) || !isset($_POST['height'])) {
		echo json_encode(array()); // return {}
	} else {
		$credit_id = isset($_POST['credit_id']) ? $_POST['credit_id'] : null;
		$data = isset($_POST['data']) ? $_POST['data'] : null;
		$filename = $_POST['filename'];
		$width = $_POST['width'];
		$height = $_POST['height'];

		try {
			include 'db.php';
			$result = false;
			if($credit_id) {
				$query = $db->prepare("SELECT credits.id
				FROM credits, orders
				WHERE credits.id = :credit_id
				AND credits.order_id = orders.id
				AND orders.paid = 1
				AND credits.id NOT IN( SELECT credit_id FROM designs where credit_id is not null )");
				$query->execute(array('credit_id' => $credit_id));
				$result = $query->fetch(PDO::FETCH_ASSOC);
			} else {
				$result = true;
			}
			if($result) {
				// success
				$query = $db->prepare("INSERT INTO designs
					(credit_id, data, width, height, filename) VALUES (:credit_id, :data, :width, :height, :filename)");
				$success = $query->execute(array('credit_id' => $credit_id, 'data' => $data, 'width' => $width, 'height' => $height, 'filename' => $filename));
				if($success) {
					$design_id = $db->lastInsertId();
					echo json_encode(array('result' => $design_id));
				} else {
					echo json_encode(array()); // return {}
				}
				// header('HTTP/1.1 200 OK');
			} else {
				// invalid token
				echo json_encode(array()); // return {}
			}
		} catch (Exception $e) {
			// echo json_encode(array($e->getMessage())); // return {}
			header('HTTP/1.1 500 Internal Server Error');
			// echo json_encode(array()); // return {}
		}
	}

?>