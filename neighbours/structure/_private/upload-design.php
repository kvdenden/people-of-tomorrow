<?php
	if(isset($_FILES["file"]) && $_FILES["file"]["error"] == UPLOAD_ERR_OK) {
		// $credit_id = isset($_POST['credit_id']) ? $_POST['credit_id'] : null;

		$max_filesize = 1024*1024*1; # 1MB

		try {
			$upload_dir = '../../preview/';
			// include 'db.php';
			list($width, $height, $type) = getimagesize($_FILES["file"]["tmp_name"]);
			$filesize = filesize($_FILES["file"]["tmp_name"]);
			if($type == 3) { # png image
				if(($width == 630 && $height == 630) || ($width == 1260 && $height == 315)) { # valid size
					if($filesize < $max_filesize) {
						$ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
		  			$filename = md5(rand()) . '.' . $ext;
		  			while(file_exists($upload_dir . $filename)) {
		  				$filename = md5(rand()) . '.' . $ext;
		  			}
		  			$success = move_uploaded_file( $_FILES["file"]["tmp_name"], $upload_dir . $filename);
		  			if($success) {
		  				echo json_encode(array('result' => array('width' => $width, 'height' => $height, 'filename' => $filename)));
		  			} else {
		  				echo json_encode(array());
		  			}
					}
					else {
						echo json_encode(array('error' => 'filesize'));
					}
				} else {
					echo json_encode(array('error' => 'dimensions'));
				}
			} else {
				echo json_encode(array('error' => 'filetype'));
			}

		} catch (Exception $e) {
			echo json_encode(array()); // return {}
		}
	} else {
				echo json_encode(array()); // return {}
	}

?>