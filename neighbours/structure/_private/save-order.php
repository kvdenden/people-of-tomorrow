<?php 
try {
	require_once 'db.php';
	
		// success
		$query = $db->prepare("INSERT INTO orders (order_id, first_name, last_name,
							 email, telno, address, zip, city, state, country, shipping,
							 shipping_address, shipping_zip, shipping_city, shipping_state,
							 shipping_country, amount, currency, brand, payment_method, language)
							 VALUES (:order_id, :first_name, :last_name, :email, :telno, :address,
							 :zip, :city, :state, :country, :shipping, :shipping_address, :shipping_zip,
							 :shipping_city, :shipping_state, :shipping_country, :amount, :currency,
							 :brand, :payment_method, :language)");
		$success = $query->execute(array(
									'order_id' => $order_id, 'first_name' => $first_name,'last_name' => $last_name, 'email' => $email,
									'telno' => $owner_telno,'address' => $owner_address,'zip' => $owner_zip,
									'city' => $owner_city,'state' => $owner_state,'country' => $owner_country,'shipping' => $shipping,
									'shipping_address' => $shipping_address,'shipping_zip' => $shipping_zip,'shipping_city' => $shipping_city,
									'shipping_state' => $shipping_state,'shipping_country' => $shipping_country,'amount' => $amount,
									'currency' => $currency,'brand' => $brand,'payment_method' => $payment_method,
									'language' => $ogone_language
									));
		if($success) {
			$order_db_id = $db->lastInsertId();
			
			// Insert order items
			
			$query = $db->prepare("INSERT INTO order_items (order_id, product_id, quantity, price, shipping_price, total)
								VALUES (:order_id, :product_id, :quantity, :price, :shipping_price, :total)");
			
			foreach ($_SESSION ["pot_basket"] ["items"] as $product_id => $product ) {
				
				$quantity = $product["qty"];
				$price = $product["price"];
				$shipping_price = isset($product["shipping_total"]) ? $product["shipping_total"] : "" ;
				$total = $product["subtotal"];
				
				$query->execute(array(
						'order_id' => $order_db_id,'product_id' => $product_id, 'quantity' => $quantity, 'price' => $price,
						'shipping_price' => $shipping_price, 'total' => $total
						));
			}
			
		} else {
			header('HTTP/1.1 500 Internal Server Error');
		}
} catch (Exception $e) {
	
	header('HTTP/1.1 500 Internal Server Error');
}

?>