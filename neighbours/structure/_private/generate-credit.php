<?php

require_once 'db.php';

// Generate credit ID
$i = 0;

try {
	$query = $db->prepare ( "SELECT id
						FROM orders
						WHERE order_id = :order_id
						" );
	$query->execute ( array ('order_id' => $order_id ) );
	$result = $query->fetch ( PDO::FETCH_ASSOC );
	
	if ($result) {
		$order_db_id = $result ['id'];
	}

} catch ( Exception $e ) {
	header ( 'HTTP/1.1 500 Internal Server Error' );

}

while ( $i < 100 ) {
	
	$random = rand ( 1, 999 );
	$token = substr ( sha1 ( $order_id . "-$random" ), 0, 10 );
	
	try {
		$query = $db->prepare ( "SELECT token
						FROM credits
						WHERE token = :token
						" );
		$query->execute ( array ('token' => $token ) );
		$result = $query->fetch ( PDO::FETCH_ASSOC );
		
		if (! $result) {
			// no result = success
			
			if ($key == "product_1") {
				$type = "normal";
			} else if ($key == "product_2") {
				$type = "art";
			} else if ($key == "product_3") {
				$type = "souvenir";
			} else {
				$type = "";
			}
			
			// Create credit in database
			
			$query = $db->prepare ( "INSERT INTO credits (order_id, token, type)
								VALUES (:order_id, :token, :type)" );
			$success = $query->execute ( array ('order_id' => $order_db_id, 'token' => $token, 'type' => $type ) );
			
			if (! $success) {
				header ( 'HTTP/1.1 500 Internal Server Error' );
			}
			
			break;
		}
	} catch ( Exception $e ) {
		header ( 'HTTP/1.1 500 Internal Server Error' );
	}
	$i ++;
}
?>