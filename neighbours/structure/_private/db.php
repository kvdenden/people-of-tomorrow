<?php
	class DBController {
		private static $_INSTANCE;
		
		private $_connection;
		
		private function DBController($db_host, $db_port, $db_user, $db_password, $db_name) {
			$this->__construct($db_host, $db_port, $db_user, $db_password, $db_name);
		}

		/* ::::: constructor ::::: */
		private function __construct($db_host, $db_port, $db_user, $db_password, $db_name) {
			$this->_connection = new PDO("mysql:host={$db_host};port={$db_port};dbname={$db_name}", $db_user, $db_password);
			$this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
	
		public function getConnection() {
			return $this->_connection;
		}

		/* ::::: singleton ::::: */
		public static function getInstance($db_host, $db_port, $db_user, $db_password, $db_name) 
		{ 
			if (!self::$_INSTANCE) {
				self::$_INSTANCE = new DBController($db_host, $db_port, $db_user, $db_password, $db_name); 
			} 
			
			return self::$_INSTANCE; 
		} 
	}
	
	if (in_array ( $_SERVER ['HTTP_HOST'], array ('localhost', '127.0.0.1' ) )) {
		$dbc = DBController::getInstance('localhost', '3306', 'root', '', 'sculpture');
	} else {
		$dbc = DBController::getInstance('mac21.be', '3306', 'sculpture', 'secret', 'sculpture');
	}
	
	$db = $dbc->getConnection();
?>