<?php
	function getSidebarCommunityText($content_xml, $text="sidebar", $social_url = "http://www.peopleoftomorrow.com" ){
		echo "<h2>" . $content_xml->snippets->sidebar->community->title . "</h2>";
		echo "<p class='emphasis'>";
			if( $text == 'sidebar'){
				echo $content_xml->snippets->sidebar->community->text;
			}else{
				echo $content_xml->$text->sidebar->community->text;
			}
		echo "</p>";
		echo getSocialIcons($social_url, $content_xml);
	}
?>