<?php 

$debug = false;

// Shop settings
$activeCurrency = "EUR"; // We need a way to know what currency people will be using ?? 

$ogone_allowed_fields = array("ACCEPTANCE","AMOUNT","BRAND","CARDNO","CN","CURRENCY","ED","IP","NCERROR","ORDERID","PAYID","PM","SHASIGN","STATUS","TRXDATE");
$ogone_language_array = array("en" => "en_US", "nl" => "nl_NL");

// Products

$products["product_1"] = array("price" => array("EUR" => "10", "USD" => "13.46"),"shipping" => array("EUR" => "10", "USD" => "13.46"));
$products["product_2"] = array("price" => array("EUR" => "30", "USD" => "40.32"),"shipping" => array("EUR" => "30", "USD" => "40.32"));
$products["product_3"] = array("price" => array("EUR" => "5", "USD" => "0"),"shipping" => array("EUR" => "16", "USD" => "0"));

$currency = array(
			"EUR" => "Euro",
			"USD" => "Dollar"
			);

// Gateway settings

if (in_array ( $_SERVER ['HTTP_HOST'], array ('localhost', '127.0.0.1' ) )) {
	$pspid = "relaxybe";
	$passphrase = "tomorrowlandistop1";
	$passphrase_shaout = "tomorrowlandistop2";
	$ogone_url = "https://secure.ogone.com/ncol/test/orderstandard.asp";
} else {
	$pspid = "relaxybe";
	$passphrase = "tomorrowlandistop1";
	$passphrase_shaout = "tomorrowlandistop2";
	$ogone_url = "https://secure.ogone.com/ncol/test/orderstandard.asp";
}

// Session check if older than 1 day = destroy

if (isset($_SESSION['pot_basket_created']) && time() - $_SESSION['pot_basket_created'] < 86400) {
	$_SESSION['pot_basket_created'] = time();
	
} else if (isset($_SESSION['pot_basket_created']) && isset($_SESSION['pot_basket'])) {
	session_destroy();
}

// Functions

function datumformat_sql($datum) {

	$datum = str_replace ( "/", "-", $datum );

	if (strtotime ( $datum ) != "") {

		$datum = strftime ( "%Y-%m-%d", strtotime ( str_replace ( '/', '-', $datum ) ) );
	} else {
		$datum = "";
	}
	return $datum;
}

function generateShasign($fields,$passphrase) {
	unset($fields['SHASIGN']);
	ksort($fields);
	$phrase = '';
	foreach($fields as $key => $field){
		if(empty($field) && $field !== '0') continue;
		$phrase .= strtoupper($key) . '=' . $field . $passphrase;
	}
	//echo "<br>".$phrase."<br>";
	return strtoupper(sha1($phrase));
}

function checkbox($waarde) {

	if ($waarde == "on") {

		$waarde = "1";
	} else {
		$waarde = "0";
	}

	return $waarde;
}

function display_quantity ($product) {
	
	$qty = isset($_SESSION['pot_basket']["items"]["$product"]['qty'])  ? $_SESSION['pot_basket']["items"]["$product"]['qty'] : 0;
	if ($qty > 0) {
		echo "<option value='$qty' selected='selected'>$qty</option>";
	}
}

function isTokenValid($token) {
	if (! empty ( $_SESSION ['tokens'] [$token] )) {
		unset ( $_SESSION ['tokens'] [$token] );
		return true;
	}
	return false;
}

function getToken() {
	unset ( $_SESSION ['tokens'] );
	$token = sha1 ( mt_rand () );
	if (! isset ( $_SESSION ['tokens'] )) {
		$_SESSION ['tokens'] = array ($token => 1 );
	} else {
		$_SESSION ['tokens'] [$token] = 1;
	}
	return $token;
}

function createOgoneUrl ($order_id,$type,$base_path,$language,$structure_xml,$pspid,$passphrase,$ogone_url) {
	
	if ($type == "session") {
		
		$first_name = $_SESSION['pot_basket']['user']['first_name'];
		$last_name = $_SESSION['pot_basket']['user']['last_name'];
		$email = $_SESSION['pot_basket']['user']['email'];
		$owner_telno = $_SESSION['pot_basket']['user']['telno'];
		$owner_address = $_SESSION['pot_basket']['user']['address'];
		$owner_zip = $_SESSION['pot_basket']['user']['zip'];
		$owner_city = $_SESSION['pot_basket']['user']['city'];
		$owner_state = $_SESSION['pot_basket']['user']['state'];
		$owner_country = $_SESSION['pot_basket']['user']['country'];
		$ogone_language = $_SESSION['pot_basket']['ogone_language'];
		$amount = $_SESSION['pot_basket']['total'] * 100;
		$currency = $_SESSION["pot_basket"]["currency"];
		$brand = $_SESSION['pot_basket']['user']['payment']['brand'];
		$payment_method = $_SESSION['pot_basket']['user']['payment']['method'];
		
	}
	if ($type == "database") {
	
	}
	
	// Feedback url's
	
	$accept_url = "$base_path$language/" . getMenuLinkByID(44, $structure_xml);
	$cancel_url = "$base_path$language/" . getMenuLinkByID(44, $structure_xml);
	$decline_url = "$base_path$language/" . getMenuLinkByID(44, $structure_xml);
	$exception_url = "$base_path$language/" . getMenuLinkByID(44, $structure_xml);
	$back_url = "$base_path$language/" . getMenuLinkByID(42, $structure_xml);
	$template_url = "$base_path" . getMenuLinkByID(43, $structure_xml);
	$home_url = "$base_path$language/home";
	
	$fields = array (
		"ACCEPTURL" => $accept_url,
		"AMOUNT" => $amount,
		"BACKURL" => $back_url,
		"BRAND" => $brand,
		"CANCELURL" => "$cancel_url",
		"CN" => $first_name . " " . $last_name,
		"CURRENCY" => $currency,
		"DECLINEURL" => $decline_url,
		"EMAIL" => $email,
		"EXCEPTIONURL" => $exception_url,
		"HOMEURL" => $home_url,
		"LANGUAGE" => $ogone_language,
		"ORDERID" => $order_id,
		"OWNERADDRESS" => $owner_address,
		"OWNERCTY" => $owner_country, // Country
		"OWNERTELNO" => $owner_telno,
		"OWNERTOWN" => $owner_city . " " . $owner_state,
		"OWNERZIP" => $owner_zip,
		"PM" => $payment_method,
		"PSPID" => $pspid,
		"TP" => $template_url
	);
	
	$ogone_sha = generateShasign($fields,$passphrase);
		
	$fields['SHASIGN'] = $ogone_sha;
	
	// Prepare query string
	$query_string = http_build_query($fields);
	
	$ogone_paymenturl = $ogone_url . "?" . $query_string;
	
	return $ogone_paymenturl;
	
}

/*function verifySHA($_REQUEST,&$pay_status) {
	
	// Alle request velden overlopen en checken of ze van ogone zijn
	
	foreach ( $_REQUEST as $key => $value ) {
	
		$value = mysql_real_escape_string ( $value );
		$value = addslashes ( $value );
		$value = strip_tags ( $value );
	
		$key = strtoupper ( $key );
	
		if (in_array($key, $ogone_allowed_fields)) {
			$fields [$key] = $value;
		}
	}
	
	// Alfabetisch overlopen
	
	ksort ( $fields );
	
	$shasign = $fields ['SHASIGN'];
	$sha_controle = generateShasign ( $fields, $passphrase_shaout );
	
	if ($shasign == $sha_controle) {
	
		$pay_status = $fields ['STATUS'];
		$pay_id = $fields ['PAYID'];
		$transaction_date = datumformat_sql ( $fields ['TRXDATE'] );
		$order_id = $fields ['ORDERID'];
	
		$order_id_encrypt = aes_encrypt ( $order_id );
		$pay_id_encrypt = aes_encrypt ( $pay_id );
	
		$query = "UPDATE $tbl_orders SET status='$pay_status',pay_id='$pay_id_encrypt',transaction_date='$transaction_date' WHERE order_id='$order_id_encrypt' LIMIT 1";
		if (mysql_query ( $query )) {
	
			$feedback[result] = true;
	
		} else {
	
			// Kan niet opslaan..
			$feedback[result] = false;
			$feedback[error] = "MYSQL error";
	
		}
	
	} else {
	
		// Sha sign stemt niet overeen - fraude - Error afhandeling
	
		$feedback[result] = false;
		$feedback[error] = "Sha controle stemt niet overeen";
		
	}
	
	return $feedback;
}*/

?>