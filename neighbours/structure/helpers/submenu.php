<?php
	function createSubmenu($parentId, $structure_xml, $language, $base_path, $clickable=false, $includeParent = true){
		$menu_structure = _recursiveStructureByID($structure_xml, $parentId);
		$path_info = parse_path($base_path);
		$active_id = getActiveMenuID($path_info, $structure_xml);
		$currentPath = $path_info['call_parts'][ count($path_info['call_parts']) -1 ];
		$arrow = "<span class='glyphicon glyphicon-chevron-right'></span>";
		$counter = 1;
		
		$countedNavs = ($includeParent)? 1 : 0;
		foreach($menu_structure->navitem as $navitem){
			if( !isset($navitem['nomenu'])){
				$countedNavs++;
			}	
		}	

		
		$returnHtml = "<nav role='payment navigation' class='payment-navigation'><ul>";
			if($includeParent){
				if( $menu_structure['path'] == $currentPath){
					$returnHtml .="<li class='active first'>";
				}else{
					$returnHtml .="<li class='first'>";
				}
				
					if($clickable){
						$returnHtml .= "<a href='" . $language . "/" . getMenuLinkByID($menu_structure['id'], $structure_xml) . "'>";
					}
					$returnHtml .= $counter . ". " . $menu_structure['title'];
					$returnHtml .= $arrow;
				$returnHtml .= "</li>";
				$counter++;
			}

			foreach($menu_structure->navitem as $navitem){
				$set = true;
				if( isset($navitem['nomenu']) ){
					$nomenu_arr = explode(' ', $navitem['nomenu']);
					foreach ($nomenu_arr as $nomenu_item){
						if( ($nomenu_item == $active_id) || ($nomenu_item == "all") )
							$set = false;
					}
				}

				if($set){
					if( $navitem['path'] == $currentPath){
						$returnHtml .="<li class='active'>";
					}else{
						$returnHtml .="<li>";
					}
						if($clickable){
							$returnHtml .= "<a href='" . $language . "/" . getMenuLinkByID($navitem['id'], $structure_xml) . "'>";
						}
						
						$returnHtml .= $counter . ". " .$navitem['title'];
						$counter++;

						if( $counter <= $countedNavs ){
							$returnHtml .= $arrow; // don't put arrow on last menu item
						}
						
						if($clickable){
							$returnHtml .= "</a>";
						}
					$returnHtml .= "</li>";	
				}
			}

			$returnHtml .= "</ul>";

		return $returnHtml;
	}
?>