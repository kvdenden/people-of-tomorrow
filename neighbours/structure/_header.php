<header class='container'>
		<div class='row'>
			<div class='col-xs-10 ow_logo_wrapper'>
				<?php require_once dirname(__FILE__) . '/snippets/_oneworld_header_logo.tpl.php'; ?>
			</div>
			<div class="col-xs-2">
				<div class="neighbours language_menu">
					<?php
						$langToLink = ($language === "nl")? "en" : "nl"; 
						echo "<a href='". $base_path . "/" . $langToLink . "'>" . $langToLink . "</a>";
					?>
				</div>
			</div>
		</div>
</header>