<?php require_once realpath(dirname(__FILE__) . '/..') . '/_private/Critsend.php'; ?>
<div class="fullscreen-container media-container">
	<img src="content/_global/images/whoarewe_header.jpg">
</div>


<div class='container vspace-full'>
	<div class="row">
		<div class='col-xs-12'>
			<?php echo $content_xml->home->confirmation; ?>
		</div>
	</div>
</div>

<?php
// Prepare email
	$mail_to = $_GET['e'];
	$firstname = $_GET['fn'];
	$lastname = $_GET['ls'];
	$token = $_GET['t'];

	$mail_from = "contact@peopleoftomorrow.com";
	$mail_from_name = "People of Tomorrow";
	$mail_replyTo = "contact@peopleoftomorrow.com";
	$mail_replyTo_name = "People of Tomorrow";
			
	$subject = "People of Tomorrow - Neighbours confirmation";

	$critsend = new MxmConnect();

	$tmp_base_path = ($base_path === 'http://pot.local/') ? 'http://pot.mac21.be' : $base_path; 
	$email_body = file_get_contents($tmp_base_path . "/mails/" . $language . "/neighbours.html");
	
	$content = array('subject'=> $subject, 'html'=> $email_body, 'text'=>$email_body);
	
	$param = array(	'tag'=>array('neighbour_confirmation'), 
					'mailfrom'=> $mail_from, 
					'mailfrom_friendly'=> $mail_from_name, 
					'replyto'=>$mail_replyTo, 
					'replyto_filtered'=> 'true');

	$emails[0] = array('email' => $mail_to,
					'field1' => $firstname.' '.$lastname,
					'field2' => $token);
	
	try {
		print_r( $critsend->sendCampaign($content, $param, $emails) );
	} catch (MxmException $e) {
		echo $e->getMessage();
	}
?>