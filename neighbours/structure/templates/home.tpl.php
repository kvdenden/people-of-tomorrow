<div class="fullscreen-container media-container">
	<img src="content/_global/images/whoarewe_header.jpg">
</div>

<div class='container vspace-full neighbours'>
	<div class="row">
		<div class='col-xs-12'>
			<div class="error-message alert alert-danger alert-dismissable" style="display:none;">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <?php echo $content_xml->home->error_message; ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class='col-xs-12 col-md-8'>
			<h1><?php echo $content_xml->home->title; ?></h1>
			<?php echo $content_xml->home->body; ?>
		</div>
	</div>
</div>

<form role="form" class="form" id="neighbour_form" action="../neighbours/structure/_private/save-neighbour.php" method="post" style="display: none;">
	<div class='container vspace-full neighbours'>
		<div class="row">
			<div class="col-xs-12">
				<h2 class="form-title"><?php echo $content_xml->home->form->title; ?></h2>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="group-title"><?php echo $content_xml->home->form->grouptitle01;?></div>
			</div>
		</div>
	

		<div class="row">
			<!-- email -->
			<div class="col-md-4">
				<div class="form-group">
					<label class="sr-only control-label" for="form_email"><?php echo $content_xml->home->form->email;?><span class="required">*</span></label>
					<div>
						<input 	type="email" class="form-control" id="form_email" name="form_email" required 
								placeholder="<?php echo $content_xml->home->form->email;?>*"
								data-validation-email-message="<?php echo $content_xml->home->form->validation->email; ?>">
					</div>
				</div>
			</div>
			
			<!-- email confirm -->
			<!-- <div class="col-md-4">
				<div class="form-group">
					<label class="sr-only control-label" for="form_emailagain"><?php echo $content_xml->home->form->emailagain;?><span class="required">*</span></label>
					<div>
						<input type="email" class="form-control" id="form_emailagain" name="form_emailagain" 
							required 
							placeholder="<?php echo $content_xml->home->form->emailagain;?>*"
		  					data-validation-matches-message="<?php echo $content_xml->home->form->validation->emailagain; ?>"
		  					data-validation-email-message="<?php echo $content_xml->home->form->validation->email; ?>">
					</div>
				</div>	
			</div> -->
		</div>
		<br>

		<div class="row group_02">
			<div class="col-xs-12">
				<div class="group-title"><?php echo $content_xml->home->form->grouptitle02;?></div>
			</div>
		</div>
			
		<div class="row">
			<!-- name -->
			<div class="col-md-4">
				<div class="form-group">
					<label class="sr-only control-label" for="form_name">
						<?php echo $content_xml->home->form->name;?><span class="required">*</span>
					</label>
					<div>
						<input type="text" class="form-control" id="form_name" name="form_name" 
								required 
								placeholder="<?php echo $content_xml->home->form->name;?>*"
								data-validation-required-message="<?php echo $content_xml->home->form->validation->name;?>">
					</div>
				</div>
			</div>
			
			<!-- surname -->
			<div class="col-md-4">
				<div class="form-group">
					<label class="sr-only control-label" for="form_surname">
						<?php echo $content_xml->home->form->surname;?><span class="required">*</span>
					</label>
					<div>
						<input type="text" class="form-control" id="form_surname" name="form_surname"
								required 
								placeholder="<?php echo $content_xml->home->form->surname;?>*"
								data-validation-required-message="<?php echo $content_xml->home->form->validation->surname;?>">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<!-- street -->
					<label class="sr-only control-label" for="form_street">
						<?php echo $content_xml->home->form->address->street;?> <span class="required">*</span>
					</label>
					<div>
						<input type="text" class="form-control" id="form_street" name="form_street"
								required 
								placeholder="<?php echo $content_xml->home->form->address->street;?>*"
								data-validation-required-message="<?php echo $content_xml->home->form->validation->address->street;?>">
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="form-group">
					<!-- number -->
					<label class="sr-only control-label" for="form_number">
						<?php echo $content_xml->home->form->address->number;?><span class="required">*</span>
					</label>
					<div class="controls">
						<input type="text" class="form-control number" id="form_number" name="form_number" 
								required 
								placeholder="<?php echo $content_xml->home->form->address->number;?>*"
								data-validation-required-message="<?php echo $content_xml->home->form->validation->address->number;?>">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<!-- city -->
			<div class="col-md-4">
				<div class="form-group">
					<label class="sr-only control-label" for="form_city">
						<?php echo $content_xml->home->form->address->city;?><span class="required">*</span>
					</label>
					<div >
						<input type="text" class="form-control" id="form_city" name="form_city" 
								required 
								placeholder="<?php echo $content_xml->home->form->address->city;?>*"
								data-validation-required-message="<?php echo $content_xml->home->form->validation->address->city;?>">
					</div>	
				</div>
			</div>
			
			<!-- zip -->
			<div class="col-md-4">
				<div class="form-group">
					<label class="sr-only control-label" for="form_zip">
						<?php echo $content_xml->home->form->address->zip;?><span class="required">*</span>
					</label>
					<div class="controls">
						<input type="text" class="form-control zip" id="form_zip" name="form_zip" 
								required 
								placeholder="<?php echo $content_xml->home->form->address->zip;?>*"
								data-validation-required-message="<?php echo $content_xml->home->form->validation->address->zip;?>">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<!-- country -->
			<div class="col-md-4">
				<div class="form-group">
					<label class="sr-only control-label" for="form_country">
						<?php echo $content_xml->home->form->address->country;?><span class="required">*</span>
					</label>
					<div class="controls">
						<input type="text" class="form-control" id="form_country" name="form_country"
								required 
								placeholder="<?php echo $content_xml->home->form->address->country;?>*"
								data-validation-required-message="<?php echo $content_xml->home->form->validation->address->country;?>">
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-8">
				<?php echo $content_xml->home->confirmation; ?>
			</div>
		</div>
		
		<div class="row">
			<!-- submit -->
			<div class="col-md-8">		
				<div class="form-actions">
					<button type="submit" class="btn btn-primary" value="<?php echo $content_xml->home->cta_label; ?>"><?php echo $content_xml->home->cta_label; ?></button>
				</div>
			</div>
		</div>
	</div>
</form>

<div class="container">
	<div id="no-credits"  style="display: none;">
		<?php echo $content_xml->home->no_credits; ?>
	</div>
</div>
