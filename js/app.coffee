'use strict'

app = angular.module('sculpture', ['pascalprecht.translate', 'ui.bootstrap', 'ui.slider'])

app.factory 'Design', () ->
	data = null
	filename = null
	width = null
	height = null
	type = null
	token = null
	return {
		getData: () -> return data
		getFilename: () -> return filename
		getWidth: () -> return width
		getHeight: () -> return height
		getType: () -> return type
		getToken: () -> return token
		setData: (d) -> data = d
		setFilename: (fn) -> filename = fn
		setWidth: (w) -> width = w
		setHeight: (h) -> height = h
		setType: (t) -> type = t
		setToken: (t) -> token = t
	}

app.config ["$routeProvider", "$translateProvider", 'DesignProvider', ($routeProvider, $translateProvider, DesignProvider) ->

	$translateProvider.useStaticFilesLoader
		prefix: 'content/editor/'
		suffix: '.json'

	$translateProvider.preferredLanguage "en"

	$routeProvider
	.when '/choose',
		templateUrl: 'structure/editor/choose.html',
		controller: 'ChooseCtrl'
	.when '/choose/upload',
		templateUrl: 'structure/editor/choose-upload.html',
		controller: 'ChooseUploadCtrl',
	.when '/small',
		redirectTo: '/square'
	.when '/square',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 693
			paperHeight: () -> 693
			paperType: () -> 'square'
	.when '/rectangle',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 1417
			paperHeight: () -> 331
			paperType: () -> 'rectangle'
	.when '/large',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 2835
			paperHeight: () -> 693
			paperType: () -> 'large'
	.when '/creative',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 2835
			paperHeight: () -> 693
			paperType: () -> 'creative'
	.when '/open/square',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 693
			paperHeight: () -> 693
			paperType: () -> 'open-square'
	.when '/open/rectangle',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 1417
			paperHeight: () -> 331
			paperType: () -> 'open-rectangle'
	.when '/tickets/square',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 693
			paperHeight: () -> 693
			paperType: () -> 'tickets-square'
	.when '/tickets/rectangle',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 1417
			paperHeight: () -> 331
			paperType: () -> 'tickets-rectangle'
	.when '/tickets/large',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 2835
			paperHeight: () -> 693
			paperType: () -> 'tickets-large'
	.when '/provant/large',
		templateUrl: 'structure/editor/paper.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 2835
			paperHeight: () -> 693
			paperType: () -> 'provant-large'
	.when '/tickets/upload',
		templateUrl: 'structure/editor/upload-tickets.html',
		controller: 'UploadTicketsCtrl',
	.when '/submit',
		templateUrl: 'structure/editor/submit.html',
		controller: 'SubmitCtrl',
	.when '/confirm',
		templateUrl: 'structure/editor/confirm.html',
		controller: 'ConfirmCtrl',
	.when '/upload',
		templateUrl: 'structure/editor/upload.html',
		controller: 'UploadCtrl',
	.when '/upload/small',
		templateUrl: 'structure/editor/upload.html',
		controller: 'UploadSmallCtrl',
	.when '/upload/creative',
		templateUrl: 'structure/editor/upload-creative.html',
		controller: 'UploadCreativeCtrl',
	.otherwise
		redirectTo: '/choose'
]

tfl_app = angular.module('tfl', ['pascalprecht.translate', 'ui.bootstrap', 'ui.slider'])

tfl_app.factory 'Design', () ->
	data = null
	filename = null
	width = null
	height = null
	type = null
	token = null
	return {
		getData: () -> return data
		getFilename: () -> return filename
		getWidth: () -> return width
		getHeight: () -> return height
		getType: () -> return type
		getToken: () -> return token
		setData: (d) -> data = d
		setFilename: (fn) -> filename = fn
		setWidth: (w) -> width = w
		setHeight: (h) -> height = h
		setType: (t) -> type = t
		setToken: (t) -> token = t
	}

tfl_app.config ["$routeProvider", "$translateProvider", 'DesignProvider', ($routeProvider, $translateProvider, DesignProvider) ->

	$translateProvider.useStaticFilesLoader
		prefix: 'content/editor/'
		suffix: '.json'

	$translateProvider.preferredLanguage "en"

	$routeProvider
	.when '/tomorrowlandforlife',
		templateUrl: 'structure/editor/paper-tfl.html',
		controller: 'PaperCtrl',
		resolve:
			paperWidth: () -> 2835
			paperHeight: () -> 693
			paperType: () -> 'tfl'
	.when '/upload/tomorrowlandforlife',
		templateUrl: 'structure/editor/upload-tfl.html',
		controller: 'UploadTflCtrl',
	.when '/confirmforlife',
		templateUrl: 'structure/editor/confirm-tfl.html',
		controller: 'ConfirmCtrl',
	.otherwise
		redirectTo: '/tomorrowlandforlife'
]