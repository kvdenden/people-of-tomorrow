$(document).ready( function(){
	var hi404 = false;
	var ratio_video = 268/609;
	var ratio_images = 167/252;
	if( $(".homepage_v3").length > 0 ){
		$(window).resize(function(){
			$("#full_trailer").css("width", $("#full_trailer").parent().width()  ).css('height', Math.floor(ratio_video * $("#full_trailer").parent().width()) );

			if( Math.round(ratio_images * $(".cta").width()) === 154){
				$(".cta").css('height', 156 );
			}else{
				$(".cta").css('height', Math.round(ratio_images * $(".cta").width()) );
			}
		});
		$("#full_trailer").css("width", $("#full_trailer").parent().width()  ).css('height', Math.floor(ratio_video * $("#full_trailer").parent().width()) );

		if( Math.round(ratio_images * $(".cta").width()) === 154){
			$(".cta").css('height', 156 );
		}else{
			$(".cta").css('height', Math.round(ratio_images * $(".cta").width()) );
		}

		$('body').addClass('colormeplenty');
		$('header').addClass('fullscreen').addClass('white');
	}

	if( $("header.learnmore").length > 0 ){
		$('body').addClass('colormeplenty');
		$('header').addClass('fullscreen').addClass('white');
	}

/**** scroll new header ****/
	if( ($("header.normal").length > 0) || ($("header.learnmore").length > 0)){
		$('header').data('size','big');
		var speed = 200;

		$(window).scroll(function(){
			var $nav = $('header');
			if( $(window).width() >= 753){
				if($("body,html").scrollTop() > 40 ){
					if ($nav.data('size') === 'big'){
						$nav.data('size','small').stop().animate({ height:'52px'}, speed);
						$('.col-logo').stop().animate({ paddingTop: '6px', paddingBottom: '6px' }, speed);
						$('#logo a img').stop().animate({width: '71px' }, speed);
						$('.col-gift .menuitem').stop().animate({ marginTop: '14px' }, speed);
						$('.col-cta .menuitem').stop().animate({ marginTop: '14px' }, speed);
						$('.col-menu .menuitem').stop().animate({ marginTop: '14px' }, speed);
						$('.col-menu .social').stop().animate({ marginTop: '14px' }, speed);
					}
				} else {
					if ($nav.data('size') === 'small'){
						$nav.data('size','big').stop().animate({ height:'97px' }, speed);
						$('.col-logo').stop().animate({ paddingTop: '12px', paddingBottom: '12px' }, speed);
						$('#logo a img').stop().animate({ width: '130px' }, speed);
						$('.col-gift .menuitem').stop().animate({ marginTop: '66px' }, speed);
						$('.col-cta .menuitem').stop().animate({ marginTop: '66px' }, speed);
						$('.col-menu .menuitem').stop().animate({ marginTop: '66px' }, speed);
						$('.col-menu .social').stop().animate({ marginTop: '60px' }, speed);
					}
				}
			}else{
				$nav.data('size','big').css('height', 'auto');
			}
		});

		$(window).resize(function(){
			if( $(window).width() < 753 ){
				$('header').css('height', 'auto');
				$('.col-gift .menuitem').css('margin-top', '0px');
				$('.col-cta .menuitem').css('margin-top', '0px');
				$('.col-menu .menuitem').css('margin-top', '0px');
			}else{
				if( ($("header.normal").length > 0) || ($("header.learnmore").length > 0) ){
					if( $('body').scrollTop() > 50 ){
						$('header').data('size','small').stop().animate({ height:'52px'}, speed);
						$('.col-logo').stop().animate({ paddingTop: '6px', paddingBottom: '6px' }, speed);
						$('#logo a img').stop().animate({width: '71px' }, speed);
						$('.col-gift .menuitem').stop().animate({ marginTop: '14px' }, speed);
						$('.col-cta .menuitem').stop().animate({ marginTop: '14px' }, speed);
						$('.col-menu .menuitem').stop().animate({ marginTop: '14px' }, speed);
						$('.col-menu .social').stop().animate({ marginTop: '14px' }, speed);
					}else{
						$('header').data('size','big').stop().animate({ height:'97px' }, speed);
						$('.col-logo').stop().animate({ paddingTop: '12px', paddingBottom: '12px' }, speed);
						$('#logo a img').stop().animate({ width: '130px' }, speed);
						$('.col-gift .menuitem').stop().animate({ marginTop: '66px' }, speed);
						$('.col-cta .menuitem').stop().animate({ marginTop: '66px' }, speed);
						$('.col-menu .menuitem').stop().animate({ marginTop: '66px' }, speed);
						$('.col-menu .social').stop().animate({ marginTop: '60px' }, speed);
					}
				}
			}
		});
	}


	if( $('.productselectv2').length > 0 ){
		$("#slider").responsiveSlides({
			auto: true,
			pager: true,
			nav: false
		});

		$("footer").css('margin-top', 0);

		if( $(window).width() < 753 ){
			$.each( $(".productCol"), function(){
				$(this).children('.content').hide();
			});
		}

		$(".productCol").click(function(){
			//event.preventDefault();
			if( $(window).width() < 753 ){
				var selectedItem = $(this);
				if( $(this).hasClass('open')){
					$(this).removeClass('open');
					$(this).children('.content').stop().slideUp(200);
				}else{
					$.each( $(".productCol"), function(){
						// close all
						if( $(this) !== $(selectedItem)){
							$(this).removeClass('open');
							$(this).children('.content').stop().slideUp(200);
						}else{

						}
					});

					$(this).children('.content').stop().slideDown(200);
					$(this).addClass('open');
				}
			}else{
				$(this).children('.content').show();

				// select a type
				$.each( $(".productCol"), function(){
					$(this).removeClass('selected');
				});

				$(this).addClass('selected');
				$('input#product').val( $(this).attr('data-value') );
				$('input#product_price').val( $(this).attr('data-price') );
				$(".col-product span.help-block").remove();
				if( $(this).attr('data-value') === "large_custom"){
					$(".fullscreen-container.blue").addClass('custom');
				}else{
					$(".fullscreen-container.blue").removeClass('custom');
				}

				/*
				if( $(this).attr('data-value') === "small_square" ){
					$(".editor-link").attr('href', "/en/participate/create#/square");
				}else if( $(this).attr('data-value') === "small_rectangle" ){
					$(".editor-link").attr('href', "/en/participate/create#/rectangle");
				}else if( $(this).attr('data-value') === "large" ){
					$(".editor-link").attr('href', "/en/participate/create#/large");
				}else if( $(this).attr('data-value') === "large_custom" ){
					$(".editor-link").attr('href', "/en/participate/create#/upload");
				}
				*/

				if($(".editor-link").hasClass('hasError')){
					$('.warning-block').slideUp(200);
					$('.thankyou-block').slideDown(200).delay(3000).slideUp(500);
				}
			}
		});


		if($('.normal-flow').length > 0){
			$(".editor-link").click(function(){
				//event.preventDefault();
				if($(this).attr('href') === "#"){
					event.preventDefault();
					$('.warning-block').slideDown(200);
					$(this).addClass('hasError');
				}
			});
		}

	}


/**** fix disclaimer modal window bug ****/
	if( $('#disclaimer_modal').length > 0){
		$('#disclaimer_modal').css('display','none');
	}
/**** set 404 page options ****/
	if( typeof hi404 !== 'undefined' ){
		if( hi404 === true ){
			$("#main-navigation").remove();
			$("footer").remove();
		}
	}

/**** productselect enhancements ****/
	if( $('.productselect').length > 0 ){
		$("#product_1").change(function(){ addremoveproductselection($(this).val(), $(this)); });
		$("#product_2").change(function(){ addremoveproductselection($(this).val(), $(this)); });
		$("#product_3").change(function(){ addremoveproductselection($(this).val(), $(this)); });
	}

	function addremoveproductselection(newval, subject){
		if( newval > 0 ){
			$(subject).parent().parent().children('.wrapper').addClass('selected');
		}else{
			$(subject).parent().parent().children('.wrapper').removeClass('selected');
		}
	}
/**** background video ****/
	//if( (Modernizr.video.h264 !== "probably")&&(Modernizr.video.ogg !== "probably")&&(Modernizr.video.webm !== 'probably') ){
	//	$("body").find('video').remove();
	//}
	/*
	if(navigator.userAgent.match(/(iPod|iPhone|iPad)/i) ){
		var vidArr = $("body").find('.video__source');
		$(vidArr).each(function(){
			$(this).remove();
		});
	}
	*/

	//if( $("body").find('video').length > 0 ){
	//	if( $(document).find('video').canPlayType ){
	//	}
	//}
/**** ****/

/**** resize YoutubeVideo ****/
	function resizevideo(){
		var videoW = Number($('.youtube_video').parent().css('width').split('px')[0]) - 60;
		var videoH = Math.round((videoW*ratioH)/ratioW);
		// set video size
		$('.youtube_video').attr('width', videoW).attr('height', videoH);
	}

	if( $('.youtube_video').length > 0 ){
		var ratioW = 16;
		var ratioH = 9;

		$(window).resize(function(){
			resizevideo();
		});

		resizevideo();
	}
/**** ****/

/**** Scroll to anchor ****/
	function scrollToAnchor(aid){
		var aTag = $("a[name='"+ aid +"']");
		$('html,body').animate({scrollTop: aTag.offset().top}, 400);
	}

	$(".scrollto").click( function(event){
		event.preventDefault();
		scrollToAnchor( $(this).attr('id') );
	});


	$("a.scroll-link").click(function(event) {
		event.preventDefault();

		var links = $(this).attr('href');
		$('html, body').animate({
		scrollTop: $(links).offset().top
		}, 2000);
	});


/*** vertical center with margin***/
	function setToMiddle(){
		//if( $(window).width() > 768 ){
		$('.overlay-centered').each( function(){
			var wrapper_height = Number($(this).parent().parent().parent().parent().css('height').split('px')[0]);
			var wrapper_margin = Number($(this).parent().parent().parent().css('margin-top').split('px')[0]);
			var center_margin = ( Number(wrapper_height) - Number(wrapper_margin) - Number($(this).height()) )/2;

			if( ($(this).hasClass('smallfix')) && ($(window).width() <= 768) ){
				$(this).css('margin-top', 30);
			}else{

				$(this).css('margin-top', center_margin);
			}
		});
	}

	if( $('.overlay-centered').length > 0){
		$(window).resize(function(){
			setToMiddle();
		});

		setToMiddle();
	}


/*** hack when OW logo is in header ***/
	if( $('.ow_logo_wrapper').length > 0 ){
		$('.navbar-header').css('margin-top', "-134px");
	}


/*** jquery validate - bootstrap override ***/
	$.validator.setDefaults({
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error').removeClass('has-success');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			if(element.parent('.input-group').length) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		}
	});




/*** jquery validation for basket form ***/
	function enableBasketFormValidation(){
		var form = $("#basket_form");
		var email_element = $("#email");
		var emailagain = $("#confirm_email");
		var terms_element = $("#terms");

		form.validate({
			rules:{
				email:{ required: true },
				confirm_email:{
					required: true,
					equalTo: email_element
				},
				terms:{ required: true }
			},
			messages:{
				email:{
					required: email_element.attr("data-validation-required-message")
				},
				confirm_email:{
					required: email_element.attr("data-validation-required-message"),
					emailConfirm: emailagain.attr("data-validation-matches-message")
				},
				terms:{
					required: terms_element.attr("data-validation-required-message")
				}
			}
		});

		form.removeAttr("novalidate");
	}

	if( $("#basket_form").length > 0){
		enableBasketFormValidation();
	}


	function enableGiftFormValidation(){
		var form = $("#gift_form");
		var email_element = $("#email");
		var emailagain = $("#confirm_email");
		var terms_element = $("#terms");
		var email_element_rec = $("#email_rec");
		var emailagain_rec = $("#confirm_email_rec");
		var product_element = $("#product");

		form.validate({
			ignore: [],
			rules:{
				email:{ required: true },
				confirm_email:{
					required: true,
					equalTo: email_element
				},

				email_rec: { required: true },
				confirm_email_rec:{
					required: true,
					equalTo: email_element_rec
				},

				terms:{ required: true },
				product:{ required:true },
			},
			messages:{
				email:{
					required: email_element.attr("data-validation-required-message")
				},
				confirm_email:{
					required: email_element.attr("data-validation-required-message"),
					emailConfirm: emailagain.attr("data-validation-matches-message")
				},

				email_rec:{
					required: email_element_rec.attr("data-validation-required-message")
				},
				confirm_email_rec:{
					required: email_element_rec.attr("data-validation-required-message"),
					emailConfirm: emailagain_rec.attr("data-validation-matches-message")
				},

				terms:{
					required: terms_element.attr("data-validation-required-message")
				},
				product:{
					required: product_element.attr("data-validation-required-message")
				}
			}
		});

		form.removeAttr("novalidate");
	}

	if( $("#gift_form").length > 0){
		enableGiftFormValidation();
	}

/*** jquery validtaion for newsletter form ***/
	function enableNewsletterFormValidation(){
		var form = $("#community_form");
		var name_element = $("#form_name");
		var email_element = $("#form_email");


		form.validate({
			rules:{
				form_name:{ required: true },
				form_email:{ required: true }
			},
			messages:{
				form_name:{ required: name_element.attr("data-validation-required-message") },
				form_email:{ required: email_element.attr("data-validation-required-message") }
			}
		});

		form.removeAttr("novalidate");
	}

	if( $("#community_form").length > 0){
		enableNewsletterFormValidation();
	}

/*** jquery validation for neighbours form ***/
	function enableNeighbourFormValidation(){
		var nbform = $("#neighbour_form");

		var email = $("#form_email");
		var emailagain = $("#form_emailagain");
		var name = $("#form_name");
		var surname = $("#form_surname");
		var street = $("#form_street");
		var number = $("#form_number");
		var city = $("#form_city");
		var zip = $("#form_zip");
		var country = $("#form_country");

		nbform.validate({
			rules:{
				form_email:{ required: true },
				form_emailagain:{
					required: true,
					equalTo: email,
				},

				form_name:{ required: true },
				form_surname:{ required: true },

				form_street:{ required: true },
				form_number:{ required: true, number: false },

				form_city:{ required: true },
				form_zip:{ required: true, number: true },

				form_country:{ required: true }
			},
			messages:{
				form_email:{ required: email.attr("data-validation-email-message") },
				form_emailagain:{
					required: email.attr("data-validation-email-message"),
					emailConfirm: emailagain.attr("data-validation-matches-message"),
				},

				form_name:{ required: name.attr("data-validation-required-message") },
				form_surname:{ required: surname.attr("data-validation-required-message") },

				form_street:{ required: street.attr("data-validation-required-message") },
				form_number:{ required: number.attr("data-validation-required-message") },

				form_city:{ required: city.attr("data-validation-required-message") },
				form_zip:{ required: zip.attr("data-validation-required-message") },

				form_country:{ required: country.attr("data-validation-required-message") }
			},
			submitHandler: function(form) {
				$(form).ajaxSubmit({
					dataType: 'json',
					success: function(result) {
						if('token' in result) {
							//var token = result.token;
							var lang = $('html').attr('lang');

							var querystring = "?t=" + result.token + "&e=" + result.email + "&fn=" + result.firstname + "&ls=" + result.lastname;

							if (lang === 'nl') {
								window.location.href = '/neighbours/nl/bevestiging' + querystring;
							} else {
								window.location.href = '/neighbours/en/confirmation'+ querystring;
							}

						}
					},
					error: function() {
						var orig = $(".error-message").first();
						var clone = $(".error-message").clone();
						$(orig).after($(clone)).show();
					}
				});
			}
		});
	}

	if( $("#neighbour_form").length > 0 ){
		enableNeighbourFormValidation();
	}

/*** ***/
/*
	if( typeof doTransactionCall !== 'undefined' ){
		if(doTransactionCall === true ){
			ga('ecommerce:clear');

			var product_arr = [];

			if( typeof productArr.items.product_1 !== 'undefined'){
				productArr.items.product_1.name = name_arr.product_1;
				product_arr.push(productArr.items.product_1 );
			}

			if( typeof productArr.items.product_2 !== 'undefined'){
				productArr.items.product_2.name = name_arr.product_2;
				product_arr.push(productArr.items.product_2 );
			}

			if( typeof productArr.items.product_3 !== 'undefined'){
				productArr.items.product_3.name = name_arr.product_3;
				product_arr.push(productArr.items.product_3 );
			}
			// **** //

			$(product_arr).each(function(index){
				var product = product_arr[index];

				ga('ecommerce:addItem', {
					'id': orderid,
					'name': product.name,
					'price': product.price,
					'quantity': product.qty,
					'currency': currency // local currency code.
				});

				totalprice += parseInt(product.price, 10);
			});

			ga('ecommerce:addTransaction',{
				'id': orderid,
				'affiliation': 'One world',
				'revenue': totalprice,
				'currency': currency
			});

			ga('ecommerce:send');
		}
	}
	*/

	if($('.homepage_v2').length > 0){
		$("#slider-arne").responsiveSlides({
			auto: false,
			pager: true,
			nav: true
		});

		$("#slider-tomorrowland").responsiveSlides({
			auto: false,
			pager: true,
			nav: true
		});

		$("#slider-trailer").responsiveSlides({
			auto: false,
			pager: true,
			nav: true
		});
	}

	$('#connections').anythingSlider({
		buildArrows         : true,
		buildStartStop      : true,
		hashTags            : false
	});

	$("#legends").anythingSlider({
		buildArrows         : true,
		buildStartStop      : true,
		hashTags            : false
	});

	$('.legendary-click').click(function(event){
		event.preventDefault();
		$('#legends').anythingSlider(2);
	});

	if( $('#full_trailer').length > 0){
		$('.watch_video').click(function(event){
			event.preventDefault();
			$('#connections').anythingSlider(2);
		});
	}

	/*** HOME ***/
	if($(".homepage_v2").length >0){
		//
		// trailer allways resizes execept if larger than 1248
		if($(window).width() < 768){
			$("#full_trailer").css("width", $(window).width() ).css('height',350);
		}else if($(window).width() <  980 ){
			$("#full_trailer").css("width", $(window).width() ).css('height',450);
		}else{
			$("#full_trailer").css("width", 1248).css('height', 546);
		}
		//

		$(window).resize(function(){
			// trailer allways resizes execept if larger than 1248
			if($(window).width() < 768){
				$("#full_trailer").css("width", $(window).width() ).css('height',350);
			}else if($(window).width() <  980 ){
				$("#full_trailer").css("width", $(window).width() ).css('height',450);
			}else{
				$("#full_trailer").css("width", 1248).css('height', 546);
			}
		});
	}

	/**** Google analytics integration ****/
	// collaboration tracking
	$(".collaboration .tml").on('click', function(){
		ga('send', 'external-link-click', 'Tomrrowland link', 'click', 'Tomorrowland link');
	});

	$(".collaboration .arne").on('click', function(){
		ga('send', 'external-link-click', 'Arne Quinze link', 'click', 'Arne Quinze link');
	});

	$(".collaboration .grouped .schorre").on('click', function(){
		ga('send', 'external-link-click', 'De Schorre link', 'click', 'De Schorre link');
	});

	$(".collaboration .grouped .provant").on('click', function(){
		ga('send', 'external-link-click', 'Provant link', 'click', 'Provant link');
	});

	// social network clicks
	$(".social-footer .facebook").on('click', function(){
		ga('send', 'external-social-click', 'Facebook', 'click', 'Facebook');
	});

	$(".social-footer .twitter").on('click', function(){
		ga('send', 'external-social-click', 'Twitter', 'click', 'Twitter');
	});

	$(".social-footer .instagram").on('click', function(){
		ga('send', 'external-social-click', 'Instagram', 'click', 'Instagram');
	});

	$(".social .facebook").on('click', function(){
		ga('send', 'external-social-click', 'Facebook', 'click', 'Facebook');
	});

	$(".social .twitter").on('click', function(){
		ga('send', 'external-social-click', 'Twitter', 'click', 'Twitter');
	});

	$(".social .instagram").on('click', function(){
		ga('send', 'external-social-click', 'Instagram', 'click', 'Instagram');
	});

	/* background image resizer */
	function background_resizer(control_element, bg_w, bg_h, footer, size_element){
		//console.log(control_element);
		if( $(control_element).length > 0 ){
			var ratio = bg_w/bg_h;
			var alt_ratio = bg_h/bg_w;

			$(window).resize( function(){
				var window_w = $(window).width();
				var window_h = $(document).height();
				var footer_h = (footer)? $('footer').height() : 0;
				var new_h = window_w * alt_ratio;
				var new_w = window_w;

				if( new_h < (window_h - footer_h) ){
					new_w = ratio * window_h;
					new_h = window_h;
				}

				$(size_element).css('background-size', new_w + "px " + new_h + "px");
			});

			var window_w = $(window).width();
			var window_h = $(document).height();
			var footer_h = 0;
			var new_h = window_w * alt_ratio;
			var new_w = window_w;

			if( new_h < (window_h - footer_h) ){
				new_w = ratio * window_h;
				new_h = window_h;
			}

			$(size_element).css('background-size', new_w + "px " + new_h + "px");
			//console.log(new_w + " x " + new_h);
		}
	}

	/** TML tickets homepage - background image sizer **/
	background_resizer($(".tmlfts"), 620, 413, true, $('.fts_bg'));
	/** Versuz homepage - background image sizer **/
	background_resizer($(".versuz"), 1920, 1200, false, $('.versuz'));
	background_resizer($(".versuz"), 1920, 1200, false, $('.shade'));
	//background_resizer($(".tfl_bg"), 2560, 1500, false, $('.tfl_bg'));
});
