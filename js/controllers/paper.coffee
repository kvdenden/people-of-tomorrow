'use strict'


# helper functions

class Helper
  constructor: (path) ->
    @path = path

  saveImage: (data, element, callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/save-image.php"
      dataType: "json"
      data:
        base64data: data
      cache: false
    .done (result) =>
      element.attr("src", "/preview/#{result.filename}")
      callback(result) if callback?

  validate: (token, type, callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/validate-token.php"
      dataType: "json"
      data:
        token: token
        type: type
      cache: false
    .done (result) =>
      callback(result) if callback?

  save: (token, type, data, width, height, filename, custom, success, failure) ->
    @validate token, type, (result) =>
      if 'id' of result
        $.ajax
          type: "POST"
          url: "#{@path}/save-design.php"
          dataType: "json"
          data:
            token: token
            credit_id: result.id
            type: result.type
            data: '' # data
            width: width
            height: height
            filename: filename
            custom: custom
          cache: false
        .done (result) =>
          # console.log(result)
          success(result) if success?
      else
        failure(result) if failure?

  # save design without credit
  save2: (type, data, width, height, filename, custom, success) ->
    $.ajax
      type: "POST"
      url: "#{@path}/save-design.php"
      dataType: "json"
      data:
        type: type
        data: '' # data
        width: width
        height: height
        filename: filename
        custom: custom
      cache: false
    .done (result) =>
      success(result) if success?

  sessionCredit: (callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/session-credit.php"
      dataType: "json"
      cache: false
    .done (result) =>
      callback(result) if callback?

  ftsCredit: (callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/fts-credit.php"
      dataType: "json"
      cache: false
    .done (result) =>
      callback(result) if callback?

  versuzCredit: (callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/versuz-credit.php"
      dataType: "json"
      cache: false
    .done (result) =>
      callback(result) if callback?

  openCredit: (callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/open-credit.php"
      dataType: "json"
      cache: false
    .done (result) =>
      callback(result) if callback?

  # get url path by id and language

  getPath: (id, language, callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/geturl.php"
      dataType: "json"
      data:
        id: id
        language: language
      cache: false
    .done (result) =>
      callback(result) if callback?

  sendMail: (title, email, name, image, language, template, tag , callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/send-email.php"
      dataType: "json"
      data:
        title: title
        email: email
        name: name
        image: image
        language: language
        template: template
        tag: tag
      cache: false
    .done (result) ->
      # console.log(result)
      callback(result) if callback?

  specialOrder: (token, callback) ->
    $.ajax
      type: "GET"
      url: "#{@path}/special-order.php"
      dataType: "json"
      data:
        token: token
      cache: false
    .done (result) ->
      callback(result) if callback?

  contactInfo: (token, name, email, callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/contact-info.php"
      dataType: "json"
      data:
        token: token
        name: name
        email: email
      cache: false
    .done (result) ->
      callback(result) if callback?

  createOrder: (name, email, credit_type, width, height, filename, callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/create-order.php"
      dataType: "json"
      data:
        name: name
        email: email
        type: credit_type
        width: width
        height: height
        filename: filename
      cache: false
    .done (result) ->
      callback(result) if callback?

  createOpenOrder: (name, email, credit_type, width, height, filename, callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/create-open-order.php"
      dataType: "json"
      data:
        name: name
        email: email
        type: credit_type
        width: width
        height: height
        filename: filename
      cache: false
    .done (result) ->
      callback(result) if callback?

  createTicketsOrder: (name, email, credit_type, width, height, filename, flow, callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/create-tickets-order.php"
      dataType: "json"
      data:
        name: name
        email: email
        type: credit_type
        width: width
        height: height
        filename: filename
        flow: flow
      cache: false
    .done (result) ->
      callback(result) if callback?

  createTflOrder: (name, email, credit_type, width, height, filename, callback) ->
    $.ajax
      type: "POST"
      url: "#{@path}/create-tfl-order.php"
      dataType: "json"
      data:
        name: name
        email: email
        type: credit_type
        width: width
        height: height
        filename: filename
      cache: false
    .done (result) ->
      callback(result) if callback?


# models

class Paper
  constructor: (id, width, height, type, scope) ->
    @scope = scope
    @canvasheight = 713
    @canvaswidth = 1450
    if type is 'large'
      @canvaswidth = 2900
    @id = id
    @paper = Raphael document.getElementById(id), '100%', '100%'
    @paper.canvas.setAttribute('preserveAspectRatio', 'xMidYMid meet')
    # @paper.canvas.setAttributeNS("http://www.w3.org/XML/1998/namespace", "xml:space","preserve");
    @paper.setViewBox 0, 0, @canvaswidth, @canvasheight, true
    @setSize(width, height)

    @selected = null

    # @fonts = [ "Arial", "Comic Sans MS", "Georgia", "Impact", "Times" ]

    @number_of_fonts = 100
    @fonts = ['Octin Spraypaint', 'Peach Milk', 'Tattoo Ink', 'Another Typewriter', 'Awesome', 'Breakdown', 'Mari David', 'Mrs Monster']

    # webfonts = [ "Lobster", "Changa One", "Special Elite", "Bigelow Rules", "Ubuntu" ]
    webfonts = ["Alfa Slab One", "Averia Sans Libre", "Crafty Girls", "Permanent Marker", "Arvo", "New Rocker", "Lily Script One", "Ubuntu", "Lobster", "Sniglet", "Finger Paint", "Luckiest Guy", "Chelsea Market", "Audiowide", "Lemon", "Indie Flower", "Rock Salt", "Oswald", "Raleway", "Fenix"]

    WebFont.load
      google:
        families: webfonts
    @fonts.push(font) for font in webfonts

    @fonts = _.sample(@fonts, @number_of_fonts)

    @dingbat_fonts =
      'PoT Icons': 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./:;<=>?@[\]^_`{|}~Œ^‘’‚“”„…‹€◼¡¢£¤¥¦§¨©ª«¬®¯œ˜–—•€™'
      # 'Atman Dings': 'ABCDEFGHJKLMNOPQRSUVWXYabcdfgijklmnpqstuvwxy'
      # 'From This Moment': 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
      # 'Mailart Graphics': 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
      # 'Marcelle Swashes' 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1​2​3​4​5​6​7​8​9​0​&​.​,​?​!​@​(​)​#​$​%​*​+​-​=​:​;'

    # @number_of_dingbats = 28
    @number_of_dingbats = 1000
    @dingbats = []
    for font, string of @dingbat_fonts
      characters = string.split('')
      for char in characters
        @dingbats.push
          font: font
          text: char

    @dingbats = _.sample(@dingbats, @number_of_dingbats)





    @number_of_stamps = 100
    @stamps = [
    #   title: 'background 1'
    #   filename: 'v1'
    #   svg:
    #     square: ''
    #     rectangle: ''
    #     large: ''
    # ,
    #   title: 'background 2'
    #   filename: 'v2'
    #   svg:
    #     square: ''
    #     rectangle: ''
    #     large: ''
    # ,
      title: 'background 3'
      filename: 'v3'
      svg:
        square: ''
        rectangle: ''
        large: ''
    ,
    #   title: 'footsteps'
    #   filename: 'footsteps'
    #   svg:
    #     square: ''
    #     rectangle: ''
    #     large: ''
    # ,
      title: 'cloud'
      filename: 'cloud'
      svg:
        square: ''
        rectangle: ''
        large: ''
    # ,
    #   title: 'new 1'
    #   filename: 'bg-new-1'
    #   svg:
    #     square: ''
    #     rectangle: ''
    #     large: ''
    # ,
    #   title: 'test'
    #   filename: 'test'
    #   svg:
    #     square: ''
    #     rectangle: ''
    #     large: ''
    # ,
    #   title: 'scissors'
    #   filename: 'scissors'
    #   svg:
    #     square: ''
    #     rectangle: ''
    #     large: ''
    ]

    if type is 'large'
      for i in [1, 2, 4, 6, 7, 8, 9, 10, 11, 12, 14, 18, 20, 21, 22]
        el =
          title: "design #{i}"
          filename: "bg-#{i}"
          svg:
            square: ''
            rectangle: ''
            large: ''
        @stamps.push el
    else
      for i in [1, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 18, 20, 21, 22]
        el =
          title: "design #{i}"
          filename: "bg-#{i}"
          svg:
            square: ''
            rectangle: ''
            large: ''
        @stamps.push el

    for filename in _.sample (_.map @stamps, (s) -> s.filename), @number_of_stamps
      types = if type is 'large' then ['large'] else ['square', 'rectangle']
      for type in types
        @loadBackground(filename, type)

    # @stamps = _.sample(@stamps, @number_of_stamps)

  loadBackground: (filename, type) ->
    $.ajax
      type: "GET"
      dataType: 'json'
      url: "/content/_global/backgrounds/#{filename}-#{type}.txt"
      cache: false
    .done (result) =>
      # console.log("loaded bg for #{filename} #{type}")
      s = _.findWhere @stamps, filename: filename
      if s
        s.svg[type] = result
        # console.log(result)
        if result is "" and type is 'large'
          @stamps.splice @stamps.indexOf(s), 1
          @scope.$apply() unless @scope.$$phase
    .error () =>
      console.log("error loading bg for #{filename} #{type}")
      if type is 'large'
        s = _.findWhere @stamps, filename: filename
        if s
          @stamps.splice @stamps.indexOf(s), 1
          @scope.$apply() unless @scope.$$phase

  text: (text, x, y) -> @paper.text(x, y, text)

  path: (str) -> @paper.path(str)

  background: (key, type) ->
    # console.log("Change background to #{key} #{type}")
    # console.log(@stamps)
    @stamp.remove() if @stamp?
    if key?
      s = _.findWhere @stamps, title: key
      # console.log(s)
      @stamp = @paper.add(s.svg[type])
      @stamp.transform("T#{@offsetX()},#{@offsetY()}")
    else
      @stamp = null

  transform: (element) ->
    @paper.freeTransform(element, {animate: false, keepRatio: ['axisX', 'axisY', 'bboxSides', 'bboxCorners'], scale: ['axisX', 'axisY'], boundary: false, draw: 'circle', distance: 1.3, range: {scale: [1, 99909]}, attrs: {'stroke': '#fff', 'stroke-width': 3}})

  setSize: (width, height) ->
    [@width, @height] = [width, height]
    if @viewport?
      @viewport.attr
        x: (@canvaswidth - @width) / 2
        y: (@canvasheight - @height) / 2
        width: @width
        height: @height
    else
      @viewport = @paper.rect((@canvaswidth - @width) / 2, (@canvasheight - @height) / 2, @width, @height)
      @viewport.attr
        "stroke": "#fff"
        "stroke-dasharray": "--"
        "stroke-width": 3
    # @paper.setViewBox 0, 0, @width, @height, true
    @resize()

  offsetX: () ->
    (@canvaswidth - @width) / 2
  offsetY: () ->
    (@canvasheight - @height) / 2

  resize: () ->
    $el = $(document.getElementById(@id))
    $el.height $el.width() * (@canvasheight / @canvaswidth)

  select: (el) ->
    if @selected isnt el
      @selected.deselect() if @selected?
      el.select()
      @selected = el

  deselect: () ->
    @selected.deselect() if @selected?
    @selected = null

  toJSON: () ->
    @deselect()
    @viewport.hide() if @viewport?
    json = @paper.toJSON()
    @viewport.show() if @viewport?
    return json

  load: (json) ->
    @paper.fromJSON(json)

  # convert mouse (page) coordinates to paper coordinates
  coordinate: (x, y) ->
    $el = $(document.getElementById(@id))
    offset = $el.offset()
    return {
      x: (x - offset.left) * @canvaswidth / $el.width(),
      y: (y - offset.top) * @canvasheight / $el.height()
    }

  toSVG: () ->
    # hide viewport and and set viewbox
    @viewport.hide() if @viewport?
    @paper.setViewBox (@canvaswidth - @width) / 2, (@canvasheight - @height) / 2, @width, @height
    # @paper.setSize @width, @height
    # get svg
    # svg = @paper.toSVG()
    svg = document.getElementById("paper-svg").innerHTML
    # show viewport and reset viewbox
    # @paper.setSize "100%", "100%"
    @viewport.show() if @viewport?
    @paper.setViewBox 0, 0, @canvaswidth, @canvasheight
    return svg

class Element
  constructor: (paper, x, y) ->
    [@paper, @x, @y] = [paper, x, y]
    @element = null

  update: () ->
    @transform.unplug() if @transform?
    @transform = @paper.transform(@element)
    window.element = @element
    window.transform = @transform
    @transform.apply()

  select: () -> @transform.showHandles() if @transform?
  deselect: () -> @transform.hideHandles { undrag: false } if @transform?

  delete: () ->
    @transform.unplug() if @transform?
    @element.remove()

class DingbatElement extends Element
  constructor: (paper, dingbat) ->
    super(paper, paper.canvaswidth / 2, paper.canvasheight / 2)

    @element = @paper.text(dingbat.text, @x, @y)
    @element.attr('font-family', dingbat.font)
    @element.attr('font-size', 72)
    @update()

class TextElement extends Element
  constructor: (paper, x, y, options) ->
    super(paper, x, y)

    @dingbat = options.dingbat

    @text = options.text
    @font = {}

    @element = @paper.text(@_text(), x, y)

    @setFontSize(options.fontsize)
    @setWrap(options.wrap)
    @setAlign(options.align)
    @setFont(options.font)
    @setBold(options.bold)
    @setItalic(options.italic)
    @update()

  setText: (text) ->
    @text = text
    @element.attr('text', @_text())
    @update()

  setWrap: (wrap) ->
    if wrap isnt @wrap
      @wrap = wrap
      @element.attr('text', @_text())
      @update()

  setAlign: (align) ->
    if align isnt @align
      @align = align
      @element.attr('text-anchor', @align) if @align in ['start', 'middle', 'end']
      @update()

  setFont: (font) ->
    if font isnt @font.family
      @font.family = font
      @element.attr('font-family', @font.family)
      @update()

  setBold: (bold) ->
    if bold isnt @font.bold
      @font.bold = bold
      if bold
        @element.attr('font-weight', 'bold')
      else
        @element.attr('font-weight', 'normal')
      @update()

  setItalic: (italic) ->
    if italic isnt @font.italic
      @font.italic = italic
      if italic
        @element.attr('font-style', 'italic')
      else
        @element.attr('font-style', 'normal')
      @update()

  setFontSize: (fontsize) ->
    if fontsize isnt @font.size
      @font.size = fontsize
      @element.attr('font-size', @font.size)
      @update()

  # wrapped text
  _text: () ->
    if @wrap?
      wordwrap(@text, @wrap, "\n", true).split("\n").map((el) -> el.trim()).join("\n")
    else
      @text

class PathElement extends Element
  constructor: (paper, options) ->
    super(paper, 0, 0)
    {@brushSize} = options
    @points = []
    @element = @paper.path(@pathString())
    @element.attr {
      "stroke-width": @brushSize or 1,
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }
    @update(false)

  addPoint: (x, y) ->
    if @points.length > 0
      firstPoint = @points[0]
      dx = x - firstPoint.x
      dy = y - firstPoint.y
      if @points.length > 1
        lastPoint = @points[@points.length-1]
        if dx - lastPoint.x != 0 or dy - lastPoint.y != 0
          @points.push {x: dx, y: dy}
      else
        @points.push {x: dx, y: dy}
    else
      # first point
      @transform.attrs.translate.x = x
      @transform.attrs.translate.y = y
      @transform.apply()
      @points.push {x: x, y: y}
    @update(false)

  setBrushSize: (brushSize) ->
    if brushSize isnt @brushSize
      @brushSize = brushSize
      @element.attr('stroke-width', @brushSize)
      @update(true)

  simplify: (tolerance) ->
    simplePoints = simplify(@points, tolerance)
    @points = simplePoints if simplePoints.length > 2

  finish: () ->
    @simplify(1)
    @update(true)


  pathString: () ->
    str = ""
    if @points.length > 0
      str += "M 0 0 "
      if @points.length is 2
        str += "L #{@points[1].x} #{@points[1].y}" if @points[2]?
      else
        str += "R "
        for p in @points[1..]
          if p?
            str += "#{p.x} #{p.y} "
    return str

  update: (select) ->
    select ?= true
    @element.attr('path', @pathString())
    super()
    if select then @select() else @deselect()


# modal controller
class ModalCtrl
  constructor: ($scope, $modalInstance, options) ->
    # console.log(options)
    $scope.options = options

class UploadSmallCtrl
  constructor: ($scope, $translate, $modal, $location, Design) ->
    Design.setType 'normal'
    $location.path('/upload')



class UploadCtrl
  constructor: ($scope, $translate, $modal, $location, Design) ->

    $scope.upload = {}
    $scope.alerts = []
    $scope.token_alerts = []
    $scope.preview = false

    lang = $('html').attr('lang')
    if lang?
      $translate.uses lang

    util = new Helper("structure/_private")

    console.log("Design type is #{Design.getType()}")

    Design.setType('large') unless Design.getType()?

    $scope.small = Design.getType() is 'normal'

    # $location.path('/choose') unless Design.getType(  )?

    $scope.breadcrumbs = [
      title: $translate('BREADCRUMB_CHOOSE_TYPE')
      path: '/choose'
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_CHOOSE_UPLOAD')
      path: '/choose/upload'
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_UPLOAD')
      path: null
      class: 'active'
    ,
      title: $translate('BREADCRUMB_CONFIRM')
      path: null
      class: ''
    ]
    util.sessionCredit (result) ->
      if result.length > 0 and 'token' of result
        $scope.breadcrumbs = $scope.breadcrumbs[1..]

    $scope.design =
      type: Design.getType()

    $scope.goto = (path) ->
      $location.path(path) if path?

    $scope.upload_start = (formData, jqForm, options) ->
      $scope.upload = {}
      $('#progress').hide().fadeIn();

    $scope.upload_success = (responseText, statusText, xhr, $form) ->
      # console.log(responseText)
      $scope.upload =
        progress: 100

      if 'result' of responseText
        $scope.upload.width = responseText.result.width
        $scope.upload.height = responseText.result.height
        $scope.upload.filename = responseText.result.filename
        $scope.preview = true
        $scope.filename = window.location.protocol + '//' + window.location.host + '/preview/' + responseText.result.filename

        $scope.alerts = []

      else
        $scope.preview = false
        # something went wrong
        if 'error' of responseText
          $scope.alert($translate('ERROR_FILESIZE'), 'danger') if responseText.error is 'filesize'
          $scope.alert($translate('ERROR_DIMENSIONS'), 'danger') if responseText.error is 'dimensions'
          $scope.alert($translate('ERROR_FILETYPE'), 'danger') if responseText.error is 'filetype'
        else
          $scope.alert($translate('ERROR'), 'danger')
      $scope.$apply() unless $scope.$$phase

    $scope.upload_failure = () ->
      $scope.preview = false
      $('#progress').hide();
      $scope.alert($translate('UPLOAD_ERROR'), 'danger')

    $scope.upload_progress = (event, position, total, percentComplete) ->
      $("#progress .progress-bar").css("width", percentComplete + "%");
      $("#progress .progress-bar").text(percentComplete + "% Complete");

    $scope.uploadDesign = () ->
      $("#submit").click()

    $scope.alert = (message, type, delay) ->
      al =
        message: message
        type: type or 'info'
      $scope.alerts.push al
      if delay > 0
        setTimeout ( ->
          idx = $scope.alerts.indexOf(al)
          $scope.alerts.splice(idx, 1)
          $scope.$apply() unless $scope.$$phase
        ), delay
      $scope.$apply() unless $scope.$$phase

    $scope.closeAlert = (alert) ->
      idx = $scope.alerts.indexOf(alert)
      $scope.alerts.splice(idx, 1)
      $scope.$apply() unless $scope.$$phase

    $scope.tokenAlert = (message, type, delay) ->
      al =
        message: message
        type: type or 'info'
      $scope.token_alerts.push al
      if delay > 0
        setTimeout ( ->
          idx = $scope.token_alerts.indexOf(al)
          $scope.token_alerts.splice(idx, 1)
          $scope.$apply() unless $scope.$$phase
        ), delay
      $scope.$apply() unless $scope.$$phase

    $scope.closeTokenAlert = (alert) ->
      idx = $scope.token_alerts.indexOf(alert)
      $scope.token_alerts.splice(idx, 1)
      $scope.$apply() unless $scope.$$phase

    $scope.validate = () ->
      Design.setFilename $scope.upload.filename #$("#upload-preview img").attr('src').replace(/^.*[\\\/]/, '')

      success = (result) ->
        Design.setToken $scope.token
        $scope.token_alerts = []
        if 'email' of result and result.email and result.email isnt ""
          filename = window.location.protocol + '//' + window.location.host + '/preview/' + Design.getFilename()
          util.sendMail("Thank you for your submission", result.name, result.email, "<img src='#{filename}' />", lang, "designed.html", "designed")
        $location.path('/confirm')
        $scope.$apply() unless $scope.$$phase
      failure = (result) ->
        $scope.tokenAlert($translate(result.error), 'warning', 0)
        # $.scrollTo("#messages", 500)
        $scope.$apply() unless $scope.$$phase
      util.save $scope.token, Design.getType(), null, $scope.upload.width, $scope.upload.height, $scope.upload.filename, true, success, failure

    $scope.share = () ->
      # filename = window.location.protocol + '//' + window.location.host + $("#preview #image-wrapper img").attr('src')
      filename = $("#upload-preview img").attr('src')
      file_id = filename.substr(filename.lastIndexOf('/')+1, 32)
      share_url = window.location.protocol + '//' + window.location.host + "/en/preview?c=#{file_id}"
      # $.ajax
      #   type: "GET"
      #   url: "https://www.facebook.com/sharer/sharer.php?u=#{filename}"
      #   cache: false
      # .done (result) =>
      FB.ui
        method: "feed"
        name: 'People of Tomorrow - My message'
        link: share_url
        caption: "My message to the world"
        description: 'Let\'s build a bridge with YOUR MESSAGE on the holy grounds of Tomorrowland, Design by Arne Quinze'
        picture: "http://www.peopleoftomorrow.com/images/facebook_thumb.jpg"

    # $scope.validate = () ->
    #   success = (result) ->
    #     Design.setWidth $scope.upload.width
    #     Design.setHeight $scope.upload.height
    #     Design.setFilename $scope.upload.filename

    #     $location.path('/confirm')
    #     $scope.$apply() unless $scope.$$phase
    #   failure = () ->
    #     $scope.alert($translate('SAVE_FAILURE'), 'warning', 0)
    #     $scope.$apply() unless $scope.$$phase
    #   util.save $scope.token, Design.getType(), null, $scope.upload.width, $scope.upload.height, $scope.upload.filename, true, success, failure

    $scope.buyCredit = () ->
      Design.setFilename $scope.upload.filename
      # save design with empty credit_id
      util.save2 Design.getType(), '', Design.getWidth(), Design.getHeight(), Design.getFilename(), true, (result) ->
        if 'result' of result
          id = result.result
          # store design id in cookie

          if id?
            $.cookie('design', null)
            $.cookie('design_type', null)
            $.cookie('design', id, { expires: 1, path: '/' })
            $.cookie('design_type', "#{Design.getType()} custom", { expires: 1, path: '/' })
          # redirect to payment...

          util.getPath 41, lang, (result) ->
            window.location.href = result

    $scope.edit = () ->
      type = Design.getType()
      if type is 'normal'
        $location.path('/small')
      else if type is 'large'
        $location.path('/large')
      else if type is 'group'
        $location.path('/group')

    $("#upload-form input").not("[type=submit]").jqBootstrapValidation
      submitSuccess: ($form, event) ->
        $form.ajaxSubmit
          beforeSubmit: $scope.upload_start
          success: $scope.upload_success
          error: $scope.upload_failure
          uploadProgress: $scope.upload_progress
          dataType: 'json'

        event.preventDefault()

    window.scope = $scope

class UploadCreativeCtrl
  constructor: ($scope, $translate, $modal, $location, Design) ->
    lang = $('html').attr('lang')
    if lang?
      $translate.uses lang

    util = new Helper("structure/_private")

    Design.setType('large') unless Design.getType()?

    $scope.design =
      type: Design.getType()

    $scope.user =
      name: ''
      email: ''

    $scope.alerts = []

    $scope.upload_start = (formData, jqForm, options) ->
      $scope.upload = {}
      $('#progress').hide().fadeIn();

    $scope.upload_success = (responseText, statusText, xhr, $form) ->
      $scope.$apply() unless $scope.$$phase
      # console.log(responseText)
      $scope.upload =
        progress: 100

      if 'result' of responseText
        $scope.upload.width = responseText.result.width
        $scope.upload.height = responseText.result.height
        $scope.upload.filename = responseText.result.filename
        $scope.preview = true
        $scope.filename = window.location.protocol + '//' + window.location.host + '/preview/' + responseText.result.filename

        util.createOrder $scope.user.name, $scope.user.email, $scope.design.type, $scope.upload.width, $scope.upload.height, $scope.upload.filename, (result) ->
          # console.log(result)
          if('error' of result)
            $scope.alerts = []
            $scope.alert($translate(result.error))
          else

            Design.setToken(result.token)
            Design.setFilename($scope.upload.filename)
            Design.setWidth($scope.upload.width)
            Design.setHeight($scope.upload.height)

            util.sendMail("Thank you for your submission", $scope.user.name, $scope.user.email, "<img src='#{$scope.filename}' />", lang, "designed.html", "designed")

            # redirect to confirmation page
            $location.path('/confirm')

          $scope.$apply() unless $scope.$$phase


      else
        $scope.preview = false
        # something went wrong
        if 'error' of responseText
          $scope.alert($translate('ERROR_FILESIZE'), 'danger') if responseText.error is 'filesize'
          $scope.alert($translate('ERROR_DIMENSIONS'), 'danger') if responseText.error is 'dimensions'
          $scope.alert($translate('ERROR_FILETYPE'), 'danger') if responseText.error is 'filetype'
        else
          $scope.alert($translate('ERROR'), 'danger')
      $scope.$apply() unless $scope.$$phase

    $scope.upload_failure = () ->
      $scope.preview = false
      $('#progress').hide();
      $scope.alert($translate('UPLOAD_ERROR'), 'danger')

    $scope.upload_progress = (event, position, total, percentComplete) ->
      $("#progress .progress-bar").css("width", percentComplete + "%");
      $("#progress .progress-bar").text(percentComplete + "% Complete");

    $scope.uploadDesign = () ->
      $("#submit").click()

    $scope.alert = (message, type, delay) ->
      al =
        message: message
        type: type or 'info'
      $scope.alerts.push al
      if delay > 0
        setTimeout ( ->
          idx = $scope.alerts.indexOf(al)
          $scope.alerts.splice(idx, 1)
          $scope.$apply() unless $scope.$$phase
        ), delay
      $scope.$apply() unless $scope.$$phase

    $scope.closeAlert = (alert) ->
      idx = $scope.alerts.indexOf(alert)
      $scope.alerts.splice(idx, 1)
      $scope.$apply() unless $scope.$$phase

    $("#upload-form input").not("[type=submit]").jqBootstrapValidation
      submitSuccess: ($form, event) ->
        $form.ajaxSubmit
          beforeSubmit: $scope.upload_start
          success: $scope.upload_success
          error: $scope.upload_failure
          uploadProgress: $scope.upload_progress
          dataType: 'json'

        event.preventDefault()

    $scope.gotoEditor = () ->
      $location.path('/creative')
      $scope.$apply() unless $scope.$$phase

class UploadTicketsCtrl
  constructor: ($scope, $translate, $modal, $location, Design) ->
    lang = $('html').attr('lang')
    if lang?
      $translate.uses lang

    util = new Helper("structure/_private")

    Design.setType('large') unless Design.getType()?

    $scope.design =
      type: Design.getType()

    $scope.user =
      name: ''
      email: ''

    $scope.alerts = []

    $scope.upload_start = (formData, jqForm, options) ->
      $scope.upload = {}
      $('#progress').hide().fadeIn();

    $scope.upload_success = (responseText, statusText, xhr, $form) ->
      # console.log(responseText)
      $scope.upload =
        progress: 100

      if 'result' of responseText
        $scope.upload.width = responseText.result.width
        $scope.upload.height = responseText.result.height
        $scope.upload.filename = responseText.result.filename
        $scope.preview = true
        $scope.filename = window.location.protocol + '//' + window.location.host + '/preview/' + responseText.result.filename

        util.createTicketsOrder $scope.user.name, $scope.user.email, $scope.design.type, $scope.upload.width, $scope.upload.height, $scope.upload.filename, '', (result) ->
          # console.log(result)
          if('error' of result)
            $scope.alerts = []
            $scope.alert($translate(result.error))
          else

            Design.setToken(result.token)
            Design.setFilename($scope.upload.filename)
            Design.setWidth($scope.upload.width)
            Design.setHeight($scope.upload.height)

            util.sendMail("Thank you for your submission", $scope.user.name, $scope.user.email, "<img src='#{$scope.filename}' />", lang, "designed.html", "designed")

            # redirect to confirmation page
            $location.path('/confirm')

          $scope.$apply() unless $scope.$$phase


      else
        $scope.preview = false
        # something went wrong
        if 'error' of responseText
          $scope.alert($translate('ERROR_FILESIZE'), 'danger') if responseText.error is 'filesize'
          $scope.alert($translate('ERROR_DIMENSIONS'), 'danger') if responseText.error is 'dimensions'
          $scope.alert($translate('ERROR_FILETYPE'), 'danger') if responseText.error is 'filetype'
        else
          $scope.alert($translate('ERROR'), 'danger')
      $scope.$apply() unless $scope.$$phase

    $scope.upload_failure = () ->
      $scope.preview = false
      $('#progress').hide();
      $scope.alert($translate('UPLOAD_ERROR'), 'danger')

    $scope.upload_progress = (event, position, total, percentComplete) ->
      $("#progress .progress-bar").css("width", percentComplete + "%");
      $("#progress .progress-bar").text(percentComplete + "% Complete");

    $scope.uploadDesign = () ->
      $("#submit").click()

    $scope.alert = (message, type, delay) ->
      al =
        message: message
        type: type or 'info'
      $scope.alerts.push al
      if delay > 0
        setTimeout ( ->
          idx = $scope.alerts.indexOf(al)
          $scope.alerts.splice(idx, 1)
          $scope.$apply() unless $scope.$$phase
        ), delay
      $scope.$apply() unless $scope.$$phase

    $scope.closeAlert = (alert) ->
      idx = $scope.alerts.indexOf(alert)
      $scope.alerts.splice(idx, 1)
      $scope.$apply() unless $scope.$$phase

    $("#upload-form input").not("[type=submit]").jqBootstrapValidation
      submitSuccess: ($form, event) ->
        $form.ajaxSubmit
          beforeSubmit: $scope.upload_start
          success: $scope.upload_success
          error: $scope.upload_failure
          uploadProgress: $scope.upload_progress
          dataType: 'json'

        event.preventDefault()

class UploadTflCtrl
  constructor: ($scope, $translate, $modal, $location, Design) ->
    lang = $('html').attr('lang')
    if lang?
      $translate.uses lang

    util = new Helper("structure/_private")

    Design.setType('large') unless Design.getType()?

    $scope.design =
      type: Design.getType()

    $scope.user =
      name: ''
      email: ''

    $scope.alerts = []

    $scope.upload_start = (formData, jqForm, options) ->
      $scope.upload = {}
      $('#progress').hide().fadeIn();

    $scope.upload_success = (responseText, statusText, xhr, $form) ->
      #console.log(responseText)
      $scope.upload =
        progress: 100

      if 'result' of responseText
        $scope.upload.width = responseText.result.width
        $scope.upload.height = responseText.result.height
        $scope.upload.filename = responseText.result.filename
        $scope.preview = true
        $scope.filename = window.location.protocol + '//' + window.location.host + '/preview/' + responseText.result.filename

        util.createTflOrder $scope.user.name, $scope.user.email, $scope.design.type, $scope.upload.width, $scope.upload.height, $scope.upload.filename, (result) ->
          #console.log(result)
          if('error' of result)
            $scope.alerts = []
            $scope.alert($translate(result.error))
          else

            Design.setToken(result.token)
            Design.setFilename($scope.upload.filename)
            Design.setWidth($scope.upload.width)
            Design.setHeight($scope.upload.height)

            util.sendMail("Thank you for your submission", $scope.user.name, $scope.user.email, "<img src='#{$scope.filename}' />", lang, "tfl-confirmation.html", "designed")

            # redirect to confirmation page
            $location.path('/confirmforlife')

          $scope.$apply() unless $scope.$$phase


      else
        $scope.preview = false
        # something went wrong
        if 'error' of responseText
          $scope.alert($translate('ERROR_FILESIZE'), 'danger') if responseText.error is 'filesize'
          $scope.alert($translate('ERROR_DIMENSIONS'), 'danger') if responseText.error is 'dimensions'
          $scope.alert($translate('ERROR_FILETYPE'), 'danger') if responseText.error is 'filetype'
        else
          $scope.alert($translate('ERROR'), 'danger')
      $scope.$apply() unless $scope.$$phase

    $scope.upload_failure = () ->
      $scope.preview = false
      $('#progress').hide();
      $scope.alert($translate('UPLOAD_ERROR'), 'danger')

    $scope.upload_progress = (event, position, total, percentComplete) ->
      $("#progress .progress-bar").css("width", percentComplete + "%");
      $("#progress .progress-bar").text(percentComplete + "% Complete");

    $scope.uploadDesign = () ->
      $("#submit").click()

    $scope.alert = (message, type, delay) ->
      al =
        message: message
        type: type or 'info'
      $scope.alerts.push al
      if delay > 0
        setTimeout ( ->
          idx = $scope.alerts.indexOf(al)
          $scope.alerts.splice(idx, 1)
          $scope.$apply() unless $scope.$$phase
        ), delay
      $scope.$apply() unless $scope.$$phase

    $scope.closeAlert = (alert) ->
      idx = $scope.alerts.indexOf(alert)
      $scope.alerts.splice(idx, 1)
      $scope.$apply() unless $scope.$$phase

    $("#upload-form input").not("[type=submit]").jqBootstrapValidation
      submitSuccess: ($form, event) ->
        $form.ajaxSubmit
          beforeSubmit: $scope.upload_start
          success: $scope.upload_success
          error: $scope.upload_failure
          uploadProgress: $scope.upload_progress
          dataType: 'json'

        event.preventDefault()

    $scope.gotoEditor = () ->
      $location.path('/tomorrowlandforlife')
      $scope.$apply() unless $scope.$$phase



class SubmitCtrl
  constructor: ($scope, $translate, $modal, $location, Design) ->
    $scope.group = Design.getType() is 'group'
    # $scope.design = Design
    # window.scope = $scope
    lang = $('html').attr('lang')
    if lang?
      $translate.uses lang

    util = new Helper("structure/_private")
    if Design.getFilename()?
      $scope.filename = window.location.protocol + '//' + window.location.host + '/preview/' + Design.getFilename()
      $scope.square = (Design.getWidth() == 693 && Design.getHeight() == 693)
    else
      $location.path('/')

    window.util = util

    $scope.alerts = []

    $scope.breadcrumbs = [
      title: $translate('BREADCRUMB_CHOOSE_TYPE')
      path: '/choose'
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_CHOOSE_UPLOAD')
      path: '/choose/upload'
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_EDITOR')
      path: null
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_SUBMIT')
      path: null
      class: 'active'
    ,
      title: $translate('BREADCRUMB_CONFIRM')
      path: null
      class: ''
    ]

    util.sessionCredit (result) ->
      if result.length > 0 and 'token' of result
        $scope.breadcrumbs = $scope.breadcrumbs[1..]
        $scope.token = result['token']
        $scope.$apply() unless $scope.$$phase

    $scope.goto = (path) ->
      $location.path(path) if path?

    $scope.share = () ->

      filename = $scope.filename
      file_id = filename.substr(filename.lastIndexOf('/'), 32)
      share_url = window.location.protocol + '//' + window.location.host + "/en/preview?c=#{file_id}"
      # $("#preview #image-wrapper img").first
      # $.ajax
      #   type: "GET"
      #   url: "https://www.facebook.com/sharer/sharer.php?u=#{$scope.filename}"
      #   cache: false
      # .done (result) =>
      FB.ui
        method: "feed"
        name: 'People of Tomorrow - My message'
        link: share_url
        caption: "My message to the world"
        description: 'Let\'s build a bridge with YOUR MESSAGE on the holy grounds of Tomorrowland, Design by Arne Quinze'
        picture: "http://www.peopleoftomorrow.com/images/facebook_thumb.jpg"


    $scope.closeAlert = (alert) ->
      idx = $scope.alerts.indexOf(alert)
      $scope.alerts.splice(idx, 1)
      $scope.$apply() unless $scope.$$phase

    $scope.alert = (message, type, delay) ->
      al =
        message: message
        type: type or 'info'
      $scope.alerts.push al
      if delay > 0
        setTimeout ( ->
          idx = $scope.alerts.indexOf(al)
          $scope.alerts.splice(idx, 1)
          $scope.$apply() unless $scope.$$phase
        ), delay
      $scope.$apply() unless $scope.$$phase

    $scope.buyCredit = () ->
      # save design with empty credit_id
      util.save2 Design.getType(), '', Design.getWidth(), Design.getHeight(), Design.getFilename(), false, (result) ->
        if 'result' of result
          id = result.result
          # store design id in cookie

          if id?
            $.cookie('design', null)
            $.cookie('design', id, { expires: 1, path: '/' })
          # redirect to payment...

          util.getPath 41, lang, (result) ->
            window.location.href = result

    $scope.validate = () ->
      success = (result) ->
        Design.setToken $scope.token
        if 'email' of result and result.email and result.email isnt ""
          filename = window.location.protocol + '//' + window.location.host + '/preview/' + Design.getFilename()
          util.sendMail("Thank you for your submission", result.name, result.email, "<img src='#{filename}' />", lang, "designed.html", "designed")
        $location.path('/confirm')
        $scope.$apply() unless $scope.$$phase
      failure = (result) ->
        $scope.alert($translate(result.error), 'warning', 0)
        $.scrollTo("#messages", 500)
        $scope.$apply() unless $scope.$$phase
      util.save $scope.token, Design.getType(), Design.getData(), Design.getWidth(), Design.getHeight(), Design.getFilename(), false, success, failure

    $scope.design = Design
    window.scope = $scope

class ConfirmCtrl
  constructor: ($scope, $translate, $modal, $location, Design) ->
    lang = $('html').attr('lang')
    if lang?
      $translate.uses lang

    util = new Helper("structure/_private")

    $scope.breadcrumbs = [
      title: $translate('BREADCRUMB_CHOOSE_TYPE')
      path: '/choose'
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_CHOOSE_UPLOAD')
      path: '/choose/upload'
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_EDITOR')
      path: null
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_SUBMIT')
      path: null
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_CONFIRM')
      path: null
      class: 'active'
    ]

    $scope.special_order = false

    $scope.special =
      token: Design.getToken()
      name: ''
      email: ''

    util.specialOrder Design.getToken(), (result) ->
      $scope.special_order = result.special
      $scope.$apply() unless $scope.$$phase

    $scope.snippets =
      community: $("#snippets #community").html()
      fb: $("#snippets #facebook").html()

    util.sessionCredit (result) ->
      if result.length > 0 and 'token' of result
        $scope.breadcrumbs = $scope.breadcrumbs[1..]

    if Design.getFilename()?
      $scope.filename = window.location.protocol + '//' + window.location.host + '/preview/' + Design.getFilename()
      $scope.square = (Design.getWidth() == 693 && Design.getHeight() == 693)
    else
      $location.path('/')

    $scope.goto = (path) ->
      $location.path(path)

    $scope.share = () ->
      filename = $scope.filename
      file_id = filename.substr(filename.lastIndexOf('/')+1, 32)
      share_url = window.location.protocol + '//' + window.location.host + "/en/preview?c=#{file_id}"
      FB.ui
        method: "feed"
        name: 'People of Tomorrow - My message'
        link: share_url
        caption: "My message to the world"
        description: 'Let\'s build a bridge with YOUR MESSAGE on the holy grounds of Tomorrowland, Design by Arne Quinze'
        picture: "http://www.peopleoftomorrow.com/images/facebook_thumb.jpg"

    $scope.submitContactInfo = () ->
      console.log("submitting contact info")
      util.contactInfo $scope.special.token, $scope.special.name, $scope.special.email, (result) ->
        $scope.special_order = false;
        $scope.$apply() unless $scope.$$phase

    $scope.design = Design
    window.scope = $scope

class ChooseCtrl
  constructor: ($scope, $translate, $modal, $location, Design) ->
    lang = $('html').attr('lang')
    if lang?
      $translate.uses lang

    util = new Helper("structure/_private")
    util.sessionCredit (result) ->
      if result.length > 0 and 'type' of result
        type = result['type']
        $scope.choose(type)
        $scope.$apply() unless $scope.$$phase

    $scope.breadcrumbs = [
      title: $translate('BREADCRUMB_CHOOSE_TYPE')
      path: '/choose'
      class: 'active'
    ,
      title: $translate('BREADCRUMB_CHOOSE_UPLOAD')
      path: null
      class: ''
    ,
      title: $translate('BREADCRUMB_EDITOR')
      path: null
      class: ''
    ,
      title: $translate('BREADCRUMB_SUBMIT')
      path: null
      class: ''
    ,
      title: $translate('BREADCRUMB_CONFIRM')
      path: null
      class: ''
    ]

    util.sessionCredit (result) ->
      if result.length > 0 and 'token' of result
        $scope.breadcrumbs = $scope.breadcrumbs[1..]

    $scope.selected = 'square';

    $scope.next = () ->
      if $scope.selected is 'creative'
        Design.setType 'large'
        @goto('/upload')
      else
        @goto("/#{$scope.selected}")


    $scope.choose = (type) ->
      $scope.selected = type
      # Design.setType type
      # @goto('/choose/upload')

    $scope.goto = (path) ->
      $location.path(path) if path?

    window.scope = $scope

class ChooseUploadCtrl
  constructor: ($scope, $translate, $modal, $location, Design) ->
    lang = $('html').attr('lang')
    if lang?
      $translate.uses lang

    util = new Helper("structure/_private")

    $location.path('/') unless Design.getType()?

    $scope.breadcrumbs = [
      title: $translate('BREADCRUMB_CHOOSE_TYPE')
      path: '/choose'
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_CHOOSE_UPLOAD')
      path: '/choose/upload'
      class: 'active'
    ,
      title: $translate('BREADCRUMB_EDITOR')
      path: null
      class: ''
    ,
      title: $translate('BREADCRUMB_SUBMIT')
      path: null
      class: ''
    ,
      title: $translate('BREADCRUMB_CONFIRM')
      path: null
      class: ''
    ]

    util.sessionCredit (result) ->
      if result.length > 0 and 'token' of result
        $scope.breadcrumbs = $scope.breadcrumbs[1..]

    # hax by Ben
    console.log(Design.getType());
    if Design.getType() == 'group'
      $('.minor').hide()

    $scope.upload = () -> $location.path('/upload')
    $scope.edit = () ->
      type = Design.getType()
      if type is 'normal'
        $scope.goto('/small')
      else if type is 'large'
        $scope.goto('/large')
      else if type is 'group'
        $scope.goto('/group')

    $scope.goto = (path) ->
      $location.path(path) if path?



# paper controller (main)
class PaperCtrl

  constructor: ($scope, $translate, $modal, $location, Design, paperWidth, paperHeight, paperType) ->
    lang = $('html').attr('lang')
    if lang?
      $translate.uses lang

    $scope.breadcrumbs = [
      title: $translate('BREADCRUMB_CHOOSE_TYPE')
      path: '/choose'
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_CHOOSE_UPLOAD')
      path: '/choose/upload'
      class: 'active-trail'
    ,
      title: $translate('BREADCRUMB_EDITOR')
      path: null
      class: 'active'
    ,
      title: $translate('BREADCRUMB_SUBMIT')
      path: null
      class: ''
    ,
      title: $translate('BREADCRUMB_CONFIRM')
      path: null
      class: ''
    ]

    util = new Helper("structure/_private")
    window.util = util

    util.sessionCredit (result) ->
      if result.length > 0 and 'token' of result
        $scope.breadcrumbs = $scope.breadcrumbs[1..]

    $scope.fts_token = false

    util.ftsCredit (result) ->
      $scope.ftsCredit = result
      if 'token' of result
        $scope.fts_token = true
        $scope.token = result.token

    $scope.creative = false
    if paperType is "creative"
      paperType = "large"
      $scope.creative = true

    $scope.open = false
    if paperType is "open-square"
      paperType = "square"
      $scope.open = true

    if paperType is "open-rectangle"
      paperType = "rectangle"
      $scope.open = true

    $scope.tickets = false
    if paperType is "tickets-square"
      paperType = "square"
      $scope.tickets = true

    if paperType is "tickets-rectangle"
      paperType = "rectangle"
      $scope.tickets = true

    if paperType is "tickets-large"
      paperType = "large"
      $scope.tickets = true

    $scope.flow = null
    if paperType is "provant-large"
      paperType = "large"
      $scope.tickets = true
      $scope.flow = 'p'

    $scope.tfl = false
    if paperType is "tfl"
      paperType = "large"
      $scope.tfl = true

    Design.setType paperType
    Design.setType "normal" if paperType is "square" or paperType is "rectangle"
    Design.setWidth paperWidth
    Design.setHeight paperHeight

    $scope.group = paperType is 'group'
    $scope.tryout = false
    if paperType is 'tryout'
      paperType = 'normal'
      $scope.tryout = true

    $scope.backgroundType = paperType
    $scope.backgroundType = "square" if paperType is "group"

    if paperType is "square" or paperType is "rectangle"
      paperType = "normal"

    $scope.paper = new Paper('paper-svg', paperWidth, paperHeight, paperType, $scope)
    window.onresize = () =>
      $scope.paper.resize()

    $scope.versuz_token = false
    util.versuzCredit (result) ->
      $scope.$apply () ->
        $scope.versuzCredit = result
        if 'token' of result
          $scope.fts_token = true
          $scope.versuz_token = true
          $scope.token = result.token
          # $scope.paper.dingbats.unshift
          #   font: "Versuz"
          #   text: 'A'

    $scope.open_token = false
    util.openCredit (result) ->
      $scope.openCredit = result
      if 'token' of result
        $scope.fts_token = true
        $scope.open_token = true
        $scope.token = result.token

    $svg = $(document.getElementById($scope.paper.id)).children('svg')

    $svg.click (e) =>
      if $(e.target).is 'svg'
        $scope.$apply () ->
          $scope.deselect()

    $svg.dblclick (e) =>
      if $scope.mode.text and $(e.target).is 'svg'
        $scope.$apply () ->
          pt = $scope.paper.coordinate(e.pageX, e.pageY)
          $scope.newTextModal(pt.x, pt.y)

    $scope.mousedown = false
    $svg.mousedown (e) ->
      if e.which is 1
        $scope.mousedown = true
        if $scope.mode.paintbrush? and $(e.target).is 'svg'
          $scope.paper.deselect()
          el = new PathElement $scope.paper,
            brushSize: $scope.attributes.brushsize
          el.element.click () ->
            $scope.$apply () ->
              $scope.select(el)
          $scope.currentPath = el

    $svg.mousemove (e) ->
      if $scope.mousedown and $scope.mode.paintbrush?
        pt = $scope.paper.coordinate(e.pageX, e.pageY)
        $scope.currentPath.addPoint(pt.x, pt.y)

    $(document).mouseup (e) ->
      if e.which is 1
        $scope.mousedown = false
        if $scope.mode.paintbrush? and $scope.currentPath?
          $scope.currentPath.finish()
          $scope.paper.select($scope.currentPath)
          $scope.paper.deselect()
          $scope.currentPath = null

    # $scope.chooseShapes = paperType is 'normal'
    $scope.chooseShapes = false

    $scope.defaultWrap = 75
    $scope.mode =
      text: true
    $scope.background =
      title: null
    $scope.viewport = $scope.backgroundType
    $scope.viewport = 'large' if paperType is 'large'
    $scope.alerts = []
    $scope.show_preview = false

    $scope.user =
      name: ''
      email: ''

    $scope.submitting = false

    $scope.share = () ->
      filename = window.location.protocol + '//' + window.location.host + $("#preview #image-wrapper img").attr('src')
      file_id = filename.substr(filename.lastIndexOf('/')+1, 32)
      share_url = window.location.protocol + '//' + window.location.host + "/en/preview?c=#{file_id}"

      # $.ajax
      #   type: "GET"
      #   url: "https://www.facebook.com/sharer/sharer.php?u=#{filename}"
      #   cache: false
      # .done (result) =>
      FB.ui
        method: "feed"
        name: 'People of Tomorrow - My message'
        link: share_url
        caption: "My message to the world"
        description: 'Let\'s build a bridge with YOUR MESSAGE on the holy grounds of Tomorrowland, Design by Arne Quinze'
        picture: "http://www.peopleoftomorrow.com/images/facebook_thumb.jpg"

    $scope.goto = (path) ->
      $location.path(path) if path?


    $scope.newTextModal = (x, y) ->
      x ?= $scope.paper.width/2
      y ?= $scope.paper.height/2
      modal = $modal.open
        templateUrl: 'structure/editor/modal.html'
        controller: ModalCtrl
        resolve:
          options: () ->
            title: $translate('NEW_MODAL_TITLE')
            result:
              text: ''
              wrap:
                checked: true
                size: $scope.defaultWrap

      modal.result.then ((result) ->
        options =
          text: result.text
          wrap: if result.wrap.checked then result.wrap.size else false
          align: $scope.attributes.align
          font: $scope.attributes.font.family
          bold: $scope.attributes.font.bold
          italic: $scope.attributes.font.italic
          fontsize: $scope.attributes.font.size
        $scope.newText(x, y, options)), () -> ''

    $scope.editTextModal = (element) ->
      modal = $modal.open
        templateUrl: 'structure/editor/modal.html'
        controller: ModalCtrl
        resolve:
          options: () ->
            title: $translate('EDIT_MODAL_TITLE')
            result:
              text: element.text
              wrap:
                checked: if element.wrap then true else false
                size: if element.wrap then element.wrap else $scope.defaultWrap

      modal.result.then ((result) ->
        element.setText(result.text)
        element.setWrap(if result.wrap.checked then result.wrap.size else false)), () -> ''

    $scope.newText = (x, y, options) ->
      options.dingbat = false
      el = new TextElement($scope.paper, x, y, options)
      el.element.click () ->
        $scope.$apply () ->
          $scope.select(el)
      el.element.dblclick () ->
        $scope.$apply () ->
          $scope.edit(el)
      $scope.select(el)
      return el

    $scope.addDingbat = (dingbat) ->
      # el = new DingbatElement($scope.paper, dingbat);
      options =
        text: dingbat.text
        fontsize: 128
        wrap: false
        align: 'middle'
        font: dingbat.font
        bold: false
        italic: false
        dingbat: true

      el = new TextElement($scope.paper, $scope.paper.canvaswidth/2, $scope.paper.canvasheight/2, options)
      el.element.click () ->
        $scope.$apply () ->
          $scope.select(el)
      $scope.select(el)
      return el

    $scope.setMode = (mode, val) ->
      $scope.mode = {}
      $scope.mode[mode] = true if val

    $scope.select = (el) ->
      $scope.paper.select(el)
      $scope.$emit('selection.changed', $scope.paper.selected)

    $scope.deselect = () ->
      $scope.paper.deselect()
      $scope.$emit('selection.changed', $scope.paper.selected)

    $scope.edit = (el) ->
      el ?= $scope.paper.selected
      $scope.editTextModal(el)

    $scope.delete = () ->
      $scope.paper.selected.delete()
      $scope.deselect()

    $scope.data = () -> $scope.paper.toJSON()

    $scope.preview = (callback) ->
      $("#preview #image-wrapper").addClass("preview")
      $("#preview #image-wrapper img").remove()
      $scope.deselect()
      svg = $scope.paper.toSVG()
      # svg = document.getElementById("paper-svg").innerHTML
      $("#canvas-wrapper").show().css
        width: $scope.paper.width,
        height: $scope.paper.height
      canvas = document.getElementById("canvas")
      canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height)
      canvg "canvas", svg,
        ignoreMouse: true
        ignoreAnimation: true
        # offsetX: -$scope.paper.offsetX()
        # offsetY: -$scope.paper.offsetY()
      image = new Image()
      image.src = canvas.toDataURL('image/png')
      $("#preview #image-wrapper").append(image)
      $("#canvas-wrapper").hide()

      util.saveImage canvas.toDataURL(), $("#preview #image-wrapper img").first(), (result) ->
        $scope.$apply () ->
          $scope.show_preview = true
        if callback?
          callback(result)
        else
          () -> $.scrollTo("#preview", 1000)

    $scope.buyCredit = () ->
      $scope.preview (result) ->
        # Design.setData $scope.data()
        Design.setFilename result.filename
        Design.setWidth $scope.paper.width
        Design.setHeight $scope.paper.height
        Design.setType paperType
        # save design with empty credit_id
        util.save2 Design.getType(), '', Design.getWidth(), Design.getHeight(), Design.getFilename(), false, (result) ->
          if 'result' of result
            id = result.result
            # store design id in cookie

            if id?
              $.cookie('design', null)
              $.cookie('design', id, { expires: 1, path: '/' })
            # redirect to payment...

            util.getPath 41, lang, (result) ->
              window.location.href = result

    $scope.submit = (callback) ->
      unless $scope.submitting
        $scope.submitting = true
        $scope.preview (result) ->
          # Design.setData $scope.data()
          Design.setFilename result.filename
          Design.setWidth $scope.paper.width
          Design.setHeight $scope.paper.height
          Design.setType paperType
          $scope.$apply () ->
            $location.path('/submit')

    $scope.submitForm = () ->
      $("#submit").click()

    $scope.submitTflForm = () ->
      $("#tfl-submit").click()

    $scope.creativeSubmit = () ->
      unless $scope.submitting
        $scope.submitting = true
        $scope.preview (result) ->
          # Design.setData $scope.data()
          Design.setFilename result.filename
          Design.setWidth $scope.paper.width
          Design.setHeight $scope.paper.height
          Design.setType paperType

          if $scope.open
            util.createOpenOrder $scope.user.name, $scope.user.email, paperType, $scope.paper.width, $scope.paper.height, result.filename, (result) ->
              #console.log(result)
              if('error' of result)
                $scope.alerts = []
                $scope.alert($translate(result.error))
              else
                Design.setToken(result.token)
                console.log(Design)

                # redirect to confirmation page
                $location.path('/confirm')

              $scope.$apply() unless $scope.$$phase
          else if $scope.tickets
            util.createTicketsOrder $scope.user.name, $scope.user.email, paperType, $scope.paper.width, $scope.paper.height, result.filename, $scope.flow, (result) ->
              #console.log(result)
              if('error' of result)
                $scope.alerts = []
                $scope.alert($translate(result.error))
              else
                Design.setToken(result.token)
                console.log(Design)

                # redirect to confirmation page
                $location.path('/confirm')

              $scope.$apply() unless $scope.$$phase
          else
            util.createOrder $scope.user.name, $scope.user.email, paperType, $scope.paper.width, $scope.paper.height, result.filename, (result) ->
              #console.log(result)
              if('error' of result)
                $scope.alerts = []
                $scope.alert($translate(result.error))
              else
                Design.setToken(result.token)
                console.log(Design)

                # redirect to confirmation page
                $location.path('/confirm')

              $scope.$apply() unless $scope.$$phase

    $("#creative-submit-form input").not("[type=submit]").jqBootstrapValidation
      submitSuccess: ($form, event) ->
        $scope.creativeSubmit()

    $scope.tflSubmit = () ->
      unless $scope.submitting
        $scope.submitting = true
        $scope.preview (result) ->
          # Design.setData $scope.data()
          Design.setFilename result.filename
          Design.setWidth $scope.paper.width
          Design.setHeight $scope.paper.height
          Design.setType paperType


          util.createTflOrder $scope.user.name, $scope.user.email, paperType, $scope.paper.width, $scope.paper.height, result.filename, (result) ->
            #console.log(result)
            if('error' of result)
              $scope.alerts = []
              $scope.alert($translate(result.error))
            else
              Design.setToken(result.token)
              console.log(Design)

              # redirect to confirmation page
              $location.path('/confirmforlife')

            $scope.$apply() unless $scope.$$phase

    $("#tfl-submit-form input").not("[type=submit]").jqBootstrapValidation
      submitSuccess: ($form, event) ->
        $scope.tflSubmit()


    # $scope.save = () ->
    #   success = () ->
    #     $scope.alert($translate('SAVE_SUCCESS'), 'info', 0)
    #   failure = () ->
    #     $scope.alert($translate('SAVE_FAIL'), 'warning', 0)
    #   util.save $scope.token, paperType, '', $scope.data(), success, failure

    $scope.validate = () ->
      Design.setFilename $("#preview #image-wrapper img").attr('src').replace(/^.*[\\\/]/, '')

      success = (result) ->
        Design.setToken $scope.token
        if 'email' of result and result.email and result.email isnt ""
          filename = window.location.protocol + '//' + window.location.host + '/preview/' + Design.getFilename()
          util.sendMail("Thank you for your submission", result.name, result.email, "<img src='#{filename}' />", lang, "designed.html", "designed")
        $location.path('/confirm')
        $scope.$apply() unless $scope.$$phase
      failure = (result) ->
        $scope.alert($translate(result.error), 'warning', 0)
        # $.scrollTo("#messages", 500)
        $scope.$apply() unless $scope.$$phase
      util.save $scope.token, Design.getType(), Design.getData(), Design.getWidth(), Design.getHeight(), Design.getFilename(), false, success, failure

    $scope.help = () ->
      $('body').chardinJs('start');

    $scope.closeAlert = (alert) ->
      idx = $scope.alerts.indexOf(alert)
      $scope.alerts.splice(idx, 1)
      $scope.$apply() unless $scope.$$phase

    $scope.alert = (message, type, delay) ->
      al =
        message: message
        type: type or 'info'
      $scope.alerts.push al
      if delay > 0
        setTimeout ( ->
          idx = $scope.alerts.indexOf(al)
          $scope.alerts.splice(idx, 1)
          $scope.$apply() unless $scope.$$phase
        ), delay
      $scope.$apply() unless $scope.$$phase


    $(document).keydown (e) ->
      if $scope.paper.selected? and not $("body").hasClass("modal-open") and (e.keyCode is 27 or e.keyCode is 46 or e.keyCode is 8)
        $focussed = $(":focus")
        unless $focussed.is("input, textarea")
          $scope.delete()
          e.preventDefault()

    selectionChanged = () ->
      $scope.show_edit = false
      if $scope.paper.selected?
        if $scope.paper.selected instanceof TextElement and not $scope.paper.selected.dingbat
          $scope.show_edit = true
          $scope.attributes.align = $scope.paper.selected.align
          $scope.attributes.font.family = $scope.paper.selected.font.family
          $scope.attributes.font.bold = $scope.paper.selected.font.bold
          $scope.attributes.font.italic = $scope.paper.selected.font.italic
          $scope.attributes.font.size = $scope.paper.selected.font.size
        else if $scope.paper.selected instanceof PathElement
          $scope.attributes.brushsize = $scope.paper.selected.brushSize
      else
        $scope.attributes.align = 'middle'
        $scope.attributes.font.bold = false
        $scope.attributes.font.italic = false
        $scope.attributes.font.size = 60
      $scope.$apply() unless $scope.$$phase

    $scope.$on 'selection.changed', selectionChanged

    attributesChanged = () ->
      if $scope.paper.selected?
        if $scope.paper.selected instanceof TextElement and not $scope.paper.selected.dingbat
          $scope.paper.selected.setAlign($scope.attributes.align)
          $scope.paper.selected.setFont($scope.attributes.font.family)
          $scope.paper.selected.setBold($scope.attributes.font.bold)
          $scope.paper.selected.setItalic($scope.attributes.font.italic)
          $scope.paper.selected.setFontSize($scope.attributes.font.size)
        else if $scope.paper.selected instanceof PathElement
          $scope.paper.selected.setBrushSize($scope.attributes.brushsize)
      $scope.$apply() unless $scope.$$phase

    $scope.$watch 'attributes', attributesChanged, true

    backgroundChanged = () ->
      $scope.paper.background($scope.background.title, $scope.viewport)
    $scope.$watch 'background.title', backgroundChanged, true

    viewportChanged = () ->
      if $scope.viewport is 'square'
        $scope.paper.setSize 693, 693
      else if $scope.viewport is 'rectangle'
        $scope.paper.setSize 1417, 331
      backgroundChanged()

    if $scope.chooseShapes
      $scope.$watch 'viewport', viewportChanged, true

    $scope.attributes =
      align: 'middle',
      font:
        family: $scope.paper.fonts[0],
        bold: false,
        italic: false,
        size: 60
      brushsize: 10
      background: null

    $scope.show_edit = false

    $scope.newText $scope.paper.canvaswidth/2, $scope.paper.canvasheight/2,
      text: 'Double-click me',
      wrap: false,
      align: 'middle',
      font: $scope.attributes.font.family,
      bold: $scope.attributes.font.bold,
      italic: $scope.attributes.font.italic
      fontsize: $scope.attributes.font.size

    $scope.$on '$viewContentLoaded', () ->
      setTimeout ( ->
         # hack
        $('.fonts .font .btn[title="'+$("select[ng-model='attributes.font.family'] option[selected='selected']").text()+'"]').addClass('active')
        $('.backgrounds .background .btn[title="'+$("select[ng-model='background.title'] option[selected='selected']").text()+'"]').addClass('active')
        # $scope.help()
      ), 500

    $scope.design = Design
    window.scope = $scope


angular.module('sculpture')
  .controller('PaperCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', 'paperWidth', 'paperHeight', 'paperType', PaperCtrl])
  .controller('SubmitCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', SubmitCtrl])
  .controller('ConfirmCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', ConfirmCtrl])
  .controller('UploadCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', UploadCtrl])
  .controller('UploadSmallCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', UploadSmallCtrl])
  .controller('UploadCreativeCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', UploadCreativeCtrl])
  .controller('UploadTicketsCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', UploadTicketsCtrl])
  .controller('ChooseCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', ChooseCtrl])
  .controller('ChooseUploadCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', ChooseUploadCtrl])

angular.module('tfl')
  .controller('PaperCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', 'paperWidth', 'paperHeight', 'paperType', PaperCtrl])
  .controller('ConfirmCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', ConfirmCtrl])
  .controller('UploadTflCtrl', ['$scope', '$translate', '$modal', '$location', 'Design', UploadTflCtrl])
