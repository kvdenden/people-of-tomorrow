$(document).ready(function() {

	$.ajax({
		type: "POST",
		url: "../neighbours/structure/_private/credits-remaining.php",
		dataType: "json",
		cache: false
	}).done(function(result) {
		if (parseInt(result) > 0) {
			$("#neighbour_form").show();
			//console.log(result + ' credits remaining...');
		} else {
			$("#neighbour_form").remove();
			$("#no-credits").show();
			//console.log('no more credits....');
		}
	});
});